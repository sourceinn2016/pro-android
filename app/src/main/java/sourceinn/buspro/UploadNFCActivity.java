package sourceinn.buspro;

import android.app.PendingIntent;
import android.content.Intent;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import sourceinn.buspro.reponse.BaseResponse;
import sourceinn.buspro.reponse.LoginResponse;
import sourceinn.buspro.reponse.RouteResponse;

/**
 * Created by sourceinn on 2017/3/27.
 */

public class UploadNFCActivity extends BaseActivity implements View.OnClickListener {
    private NfcAdapter mNfcAdapter;
    private PendingIntent mPendingIntent;
    boolean isUploading = false;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_nfc);
        ImageView back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(this);
        mNfcAdapter = mNfcAdapter.getDefaultAdapter(this);
        if (mNfcAdapter == null) {
            Common.showMsgDialog(getSupportFragmentManager(), getString(R.string.noSupportNFC), new MsgDialogFragment.MsgDialogLister() {
                @Override
                public void sureEvent() {
                    finish();
                }
            }, "noCancel");
            return;
        }
        if (!mNfcAdapter.isEnabled()) {
            Common.showMsgDialog(getSupportFragmentManager(), getString(R.string.plsOpenNFCInSystem), new MsgDialogFragment.MsgDialogLister() {
                @Override
                public void sureEvent() {
                    finish();
                }
            }, "noCancel");
            return;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            mPendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()), PendingIntent.FLAG_IMMUTABLE|0);
        }else{
            mPendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()), 0);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        if(!isUploading){
            isUploading = true;
            final Tag tag = intent.getParcelableExtra(mNfcAdapter.EXTRA_TAG);
            Log.v("QQQ", Common.bytesToHexString(tag.getId()));
            //Toast.makeText(this, Common.bytesToHexString(tag.getId()), Toast.LENGTH_SHORT).show();

            Common.showLoading(getSupportFragmentManager(), islive);
            NFCCardCreate(Common.bytesToHexString(tag.getId())).subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Action1<BaseResponse>() {
                        @Override
                        public void call(BaseResponse o) {
                            String msg = Common.bytesToHexString(tag.getId());
                            if(o.getResult().equals("Success")){
                                msg += " "+getString(R.string.addSuccess);
                            }else{
                                msg += " "+getString(R.string.addFail);
                            }
                            Common.showMsgDialog(getSupportFragmentManager(), msg, null, "noCancel");
                        }
                    }, new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            isUploading = false;
                            Common.hideLoading(getSupportFragmentManager(), islive);
                        }
                    }, new Action0() {
                        @Override
                        public void call() {
                            isUploading = false;
                            Common.hideLoading(getSupportFragmentManager(), islive);
                        }
                    });
        }

    }

    private Observable<BaseResponse> NFCCardCreate(final String CardID){
        return Observable.create(new Observable.OnSubscribe<BaseResponse>() {
            @Override
            public void call(Subscriber<? super BaseResponse> subscriber) {
                HashMap<String, String> para = new HashMap<String, String>();
                para.put("Token", LoginResponse.getLastest(UploadNFCActivity.this).getToken());
                para.put("CardID", CardID);
                try {
                    String resp = OkHttpUtil.execute(OkHttpUtil.postRequest(getString(R.string.base_url)+"NFCCardCreate", para)).body().string();
                    Log.v("QQQ", resp);
                    Gson gson = new Gson();
                    BaseResponse<List<RouteResponse>> cc = gson.fromJson(resp, new TypeToken<BaseResponse<List<RouteResponse>>>() {}.getType());
                    subscriber.onNext(cc);
                } catch (IOException e) {
                    subscriber.onError(e);
                }
                subscriber.onCompleted();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mNfcAdapter != null && mNfcAdapter.isEnabled()) {
            mNfcAdapter.enableForegroundDispatch(this, mPendingIntent, null, null);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mNfcAdapter != null) {
            mNfcAdapter.disableForegroundDispatch(this);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.back:
                finish();
                break;
        }
    }
}
