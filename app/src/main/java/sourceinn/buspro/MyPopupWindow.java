package sourceinn.buspro;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;

import java.util.List;

/**
 * Created by sourceinn on 2016/8/23.
 */
public class MyPopupWindow extends PopupWindow implements AdapterView.OnItemClickListener {
    ListView listView;
    Context mContext;
    PopAdapter mAdapter;
    List<String> str;
    private PopListener popListener;
    public interface PopListener{
        void popClick(String str);
    }
    public void setPopListener(PopListener l){
        popListener = l;
    }



    public MyPopupWindow(Context mContext, List<String> str) {
        super(mContext);
        this.mContext = mContext;
        this.str = str;
        init();
    }

    private void init() {
        View view = LayoutInflater.from(mContext).inflate(R.layout.mypopup, null);
        setContentView(view);
        setWidth((int) (150* Common.getDensity(mContext)));
        setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
        setFocusable(true);
        setBackgroundDrawable(new BitmapDrawable(mContext.getResources(), (Bitmap) null));

        listView = (ListView) view.findViewById(R.id.listView);
        mAdapter = new PopAdapter(mContext, str);
        listView.setAdapter(mAdapter);
        listView.setOnItemClickListener(this);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        dismiss();
        if(popListener!=null)
            popListener.popClick(str.get(position));
    }

    public void refreshData(List<String> newstr){
        str.clear();
        for(int i=0;i<newstr.size();i++){
            str.add(newstr.get(i));
        }
        mAdapter.notifyDataSetChanged();
    }
}
