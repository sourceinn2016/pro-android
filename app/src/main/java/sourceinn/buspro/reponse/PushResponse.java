package sourceinn.buspro.reponse;

/**
 * Created by sourceinn on 2016/9/30.
 */
public class PushResponse {
    private String name;
    private String id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
