package sourceinn.buspro.reponse;

import java.io.Serializable;

/**
 * Created by sourceinn on 2016/9/19.
 */
public class RouteResponse implements Serializable{
    private String ID;
    private String Name;
    private String Status;
    private String Station;

    public String getStation() {
        return Station;
    }

    public void setStation(String station) {
        Station = station;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }
}
