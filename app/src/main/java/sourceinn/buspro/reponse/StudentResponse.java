package sourceinn.buspro.reponse;

import java.io.Serializable;
import java.util.List;

/**
 * Created by sourceinn on 2016/10/26.
 */
public class StudentResponse implements Serializable{
    private String RouteID;
    private String StartLogTimeAM;
    private String StopLogTimeAM;
    private String StartLogTimePM;
    private String StopLogTimePM;
    private List<StationResponse> List;

    public String getRouteID() {
        return RouteID;
    }

    public void setRouteID(String routeID) {
        RouteID = routeID;
    }

    public String getStartLogTimeAM() {
        return StartLogTimeAM;
    }

    public void setStartLogTimeAM(String startLogTimeAM) {
        StartLogTimeAM = startLogTimeAM;
    }

    public String getStopLogTimeAM() {
        return StopLogTimeAM;
    }

    public void setStopLogTimeAM(String stopLogTimeAM) {
        StopLogTimeAM = stopLogTimeAM;
    }

    public String getStartLogTimePM() {
        return StartLogTimePM;
    }

    public void setStartLogTimePM(String startLogTimePM) {
        StartLogTimePM = startLogTimePM;
    }

    public String getStopLogTimePM() {
        return StopLogTimePM;
    }

    public void setStopLogTimePM(String stopLogTimePM) {
        StopLogTimePM = stopLogTimePM;
    }

    public java.util.List<StationResponse> getList() {
        return List;
    }

    public void setList(java.util.List<StationResponse> list) {
        List = list;
    }
}
