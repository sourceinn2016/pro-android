package sourceinn.buspro.reponse;

import android.content.Context;

import java.io.Serializable;
import java.util.List;

import sourceinn.buspro.Common;

/**
 * Created by sourceinn on 2016/9/14.
 */
public class LoginResponse implements Serializable{
    private String Message;
    private String Result;
    private String Token;
    private List<RouteResponse> Data;
    private boolean IsConductor;
    private boolean IsRollCall;

    private String Mobile;
    private String Password;

    public String getMobile() {
        return Mobile;
    }

    public void setMobile(String mobile) {
        Mobile = mobile;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getResult() {
        return Result;
    }

    public void setResult(String result) {
        Result = result;
    }

    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        Token = token;
    }

    public List<RouteResponse> getData() {
        return Data;
    }

    public void setData(List<RouteResponse> data) {
        Data = data;
    }

    public boolean isConductor() {
        return IsConductor;
    }

    public void setIsConductor(boolean isConductor) {
        IsConductor = isConductor;
    }

    public boolean isRollCall() {
        return IsRollCall;
    }

    public void setRollCall(boolean rollCall) {
        IsRollCall = rollCall;
    }

    public static LoginResponse getLastest(Context context){
        return Common.getLoginInfo(context);
    }
}
