package sourceinn.buspro.reponse;

/**
 * Created by sourceinn on 2016/9/29.
 */
public class AbnormalResponse {
    private String Name;
    private String Route;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getRoute() {
        return Route;
    }

    public void setRoute(String route) {
        Route = route;
    }
}
