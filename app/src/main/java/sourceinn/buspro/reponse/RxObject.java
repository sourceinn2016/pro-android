package sourceinn.buspro.reponse;

/**
 * Created by sourceinn on 2016/9/20.
 */
public class RxObject {
    private String tag;
    private Object object;

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }
}
