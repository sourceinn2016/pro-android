package sourceinn.buspro.reponse;

import java.io.Serializable;

public class FlagData implements Serializable {
    public static final int IN = 0;
    public static final int OUT = 1;
    public static final int NOTIFY = 2;
    private int id;
    private String StaionID_Date_AMPM;

    private int type;
    private String RouteID;
    private String Account;

    public String getRouteID() {
        return RouteID;
    }

    public void setRouteID(String routeID) {
        RouteID = routeID;
    }

    public String getAccount() {
        return Account;
    }

    public void setAccount(String account) {
        Account = account;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStaionID_Date_AMPM() {
        return StaionID_Date_AMPM;
    }

    public void setStaionID_Date_AMPM(String staionID_Date_AMPM) {
        StaionID_Date_AMPM = staionID_Date_AMPM;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
