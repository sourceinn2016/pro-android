package sourceinn.buspro.reponse;

/**
 * Created by sourceinn on 2016/7/25.
 */
public class BaseResponse<T> {
    private String Result;
    private String Message;
    private T Data;

    public String getResult() {
        return Result;
    }

    public void setResult(String result) {
        Result = result;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public T getData() {
        return Data;
    }

    public void setData(T data) {
        Data = data;
    }
}
