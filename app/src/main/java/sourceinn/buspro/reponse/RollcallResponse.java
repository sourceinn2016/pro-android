package sourceinn.buspro.reponse;

import java.util.List;

/**
 * Created by sourceinn on 2016/9/23.
 */
public class RollcallResponse {
    private String ID;
    private String Name;
    private List<String> ParentsPhone;
    private boolean Status;
    private boolean isSign;
    private String StationID;
    private String CardID;

    public String getStationID() {
        return StationID;
    }

    public void setStationID(String stationID) {
        StationID = stationID;
    }

    public String getCardID() {
        return CardID;
    }

    public void setCardID(String cardID) {
        CardID = cardID;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public List<String> getParentsPhone() {
        return ParentsPhone;
    }

    public void setParentsPhone(List<String> parentsPhone) {
        ParentsPhone = parentsPhone;
    }

    public boolean isStatus() {
        return Status;
    }

    public void setStatus(boolean status) {
        Status = status;
    }

    public boolean isSign() {
        return isSign;
    }

    public void setSign(boolean sign) {
        isSign = sign;
    }
}
