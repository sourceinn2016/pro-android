package sourceinn.buspro.reponse;

import java.io.Serializable;

public class DistanceRp implements Serializable {
    private int In;
    private int Out;
    private int Notify;
    private boolean Departure;

    public int getIn() {
        return In;
    }

    public void setIn(int in) {
        In = in;
    }

    public int getOut() {
        return Out;
    }

    public void setOut(int out) {
        Out = out;
    }

    public int getNotify() {
        return Notify;
    }

    public void setNotify(int notify) {
        Notify = notify;
    }

    public boolean isDeparture() {
        return Departure;
    }

    public void setDeparture(boolean departure) {
        Departure = departure;
    }
}
