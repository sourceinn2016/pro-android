package sourceinn.buspro.reponse;

import java.io.Serializable;
import java.util.List;

/**
 * Created by sourceinn on 2016/10/26.
 */
public class GetMemberResponse implements Serializable{
    private String ID;
    private boolean IsConductor;
    private boolean IsRollCall;
    private String Name;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public boolean isConductor() {
        return IsConductor;
    }

    public void setConductor(boolean conductor) {
        IsConductor = conductor;
    }

    public boolean isRollCall() {
        return IsRollCall;
    }

    public void setRollCall(boolean rollCall) {
        IsRollCall = rollCall;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }
}
