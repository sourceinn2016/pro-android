package sourceinn.buspro.reponse;

import java.io.Serializable;

/**
 * Created by sourceinn on 2016/9/19.
 */
public class StationResponse implements Serializable{
    private String ID;
    private String Sequence;
    private String Name;
    private String StopTime;
    private String OutTime;
    private String Status;
    private String Longitude;
    private String Latitude;
    private String DefaultTime;
    private boolean showOutTime = false;

    private boolean showDefaultTime = true;

    public boolean isShowOutTime() {
        return showOutTime;
    }

    public void setShowOutTime(boolean showOutTime) {
        this.showOutTime = showOutTime;
    }

    public String getOutTime() {
        return OutTime;
    }

    public void setOutTime(String outTime) {
        OutTime = outTime;
    }

    public boolean isShowDefaultTime() {
        return showDefaultTime;
    }

    public void setShowDefaultTime(boolean showDefaultTime) {
        this.showDefaultTime = showDefaultTime;
    }

    public String getDefaultTime() {
        return DefaultTime;
    }

    public void setDefaultTime(String defaultTime) {
        DefaultTime = defaultTime;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getSequence() {
        return Sequence;
    }

    public void setSequence(String sequence) {
        Sequence = sequence;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getStopTime() {
        return StopTime;
    }

    public void setStopTime(String stopTime) {
        StopTime = stopTime;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }
}
