package sourceinn.buspro;

import android.Manifest;
import android.app.Dialog;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

/**
 * Created by sourceinn on 2016/4/15.
 */
public class DiscloseureDia extends DialogFragment {
    public final static String TAG="DiscloseureDia";
    public static final int REQUEST_LOCATION_CODE = 1198;
    CB cb;
    RxBus rxBus = RxBus.getDefault();
    public interface CB{
        void onPermissionGet();
    }
    public void setCb(CB cb){
        this.cb = cb;
    }
    public static DiscloseureDia showMsgDialog(FragmentManager fm, CB cb ){
        FragmentTransaction ft =fm.beginTransaction();
        Fragment prev = fm.findFragmentByTag(TAG);
        if (prev != null) {
            ft.remove(prev);
        }
        final DiscloseureDia newFragment = DiscloseureDia.newInstance();
        newFragment.setCb(cb);
        ft.add(newFragment, TAG);
        ft.commitAllowingStateLoss();
        return newFragment;
    }


    public static DiscloseureDia newInstance(){
        Bundle args = new Bundle();
        DiscloseureDia fragment = new DiscloseureDia();
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return new Dialog(getActivity(), getTheme()){
            @Override
            public void onBackPressed() {

            }
        };
    }

    @Override
    public void onStart() {
        super.onStart();

        //填滿並去背
        getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        //去遮罩
        Window window = getDialog().getWindow();
        WindowManager.LayoutParams windowParams = window.getAttributes();
        windowParams.dimAmount = 0.5f;
        window.setAttributes(windowParams);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        View v = inflater.inflate(R.layout.disclosure, null);
        return v;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TextView change = view.findViewById(R.id.change);
        change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        //請求權限
                        requestPermissions(
                                new String[]{   Manifest.permission.ACCESS_COARSE_LOCATION,
                                        Manifest.permission.ACCESS_FINE_LOCATION},
                                REQUEST_LOCATION_CODE);
                    }else{

                    }
                }
            }
        });
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode==REQUEST_LOCATION_CODE) {
            if (Common.checkGrantResult(grantResults)) {
                cb.onPermissionGet();
                dismiss();
            }else{
                Common.showMsgDialog(getChildFragmentManager(),  getString(R.string.permission_rationale), null, "noCancel");
            }
        }
    }
}