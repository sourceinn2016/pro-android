package sourceinn.buspro;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import sourceinn.buspro.reponse.AbnormalResponse;


public class AbnormalAdapter extends RecyclerView.Adapter {
    protected List<AbnormalResponse> mDatas;
    RxBus rxBus = RxBus.getDefault();
    protected Context mContext;
    String time;
    public AbnormalAdapter(Context context, List<AbnormalResponse> mDatas){
        mContext = context;
        this.mDatas = mDatas;
        Date today = Calendar.getInstance().getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("HH");
        String nowHH = formatter.format(today);
        if(Integer.parseInt(nowHH)<12){
            time = "AM";
        }else{
            time = "PM";
        }
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.abnormal_item, parent, false);
        return new MyViewHolder(view);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        ((MyViewHolder)holder).name.setText(mDatas.get(position).getName());
        ((MyViewHolder)holder).route.setText(mDatas.get(position).getRoute());
        ((MyViewHolder)holder).time.setText(time);


        if(position%2==0){
            ((MyViewHolder)holder).itemView.setBackgroundResource(R.color.white);
        }else{
            ((MyViewHolder)holder).itemView.setBackgroundResource(R.color.btngray);
        }

    }


    @Override
    public int getItemCount()
    {
        return mDatas==null?0:mDatas.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView name;
        TextView route;
        TextView time;



        public MyViewHolder(View view)
        {
            super(view);
            name = (TextView)view.findViewById(R.id.name);
            route = (TextView)view.findViewById(R.id.route);
            time = (TextView)view.findViewById(R.id.time);

        }
    }

}
