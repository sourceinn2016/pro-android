package sourceinn.buspro;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SwitchCompat;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import sourceinn.buspro.reponse.BaseResponse;
import sourceinn.buspro.reponse.GetMemberResponse;
import sourceinn.buspro.reponse.LoginResponse;
import sourceinn.buspro.reponse.RxObject;


public class GetMemberAdapter extends RecyclerView.Adapter {
    protected List<GetMemberResponse> mDatas;
    RxBus rxBus = RxBus.getDefault();
    protected Context mContext;
    public GetMemberAdapter(Context context, List<GetMemberResponse> mDatas){
        mContext = context;
        this.mDatas = mDatas;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.dispatch_listitem, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        ((MyViewHolder)holder).name.setText(mDatas.get(position).getName());
        ((MyViewHolder)holder).IsConductor.setChecked(mDatas.get(position).isConductor()?true:false);
        ((MyViewHolder)holder).IsRollCall.setChecked(mDatas.get(position).isRollCall()?true:false);
        ((MyViewHolder)holder).FramIsConductor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RxObject rxObject = new RxObject();
                rxObject.setTag("showloading");
                rxBus.post(rxObject);
                SetMember(mDatas.get(position).getID(), "Conductor", String.valueOf(!((MyViewHolder)holder).IsConductor.isChecked())).subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Action1<String>() {
                            @Override
                            public void call(String s) {
                                RxObject rxObject = new RxObject();
                                rxObject.setTag("hideloading");
                                rxBus.post(rxObject);
                                if(s.equals("Success")){
                                    ((MyViewHolder)holder).IsConductor.setChecked(!((MyViewHolder)holder).IsConductor.isChecked());
                                }
                            }
                        });

            }
        });
        ((MyViewHolder)holder).FramIsRollCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RxObject rxObject = new RxObject();
                rxObject.setTag("showloading");
                rxBus.post(rxObject);
                SetMember(mDatas.get(position).getID(), "RollCall", String.valueOf(!((MyViewHolder)holder).IsRollCall.isChecked())).subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Action1<String>() {
                            @Override
                            public void call(String s) {
                                RxObject rxObject = new RxObject();
                                rxObject.setTag("hideloading");
                                rxBus.post(rxObject);
                                if(s.equals("Success")){
                                    ((MyViewHolder)holder).IsRollCall.setChecked(!((MyViewHolder)holder).IsRollCall.isChecked());
                                }
                            }
                        });
            }
        });
        if(position==0){
            ((MyViewHolder)holder).line.setVisibility(View.GONE);
        }else{
            ((MyViewHolder)holder).line.setVisibility(View.VISIBLE);
        }
    }

    public Observable<String> SetMember(final String memID, final String type, final String status){
        return Observable.create(new Observable.OnSubscribe<String>() {
            @Override
            public void call(Subscriber<? super String> subscriber) {
                try {
                    HashMap<String, String> para = new HashMap<String, String>();
                    para.put("Token", LoginResponse.getLastest(mContext).getToken());
                    para.put("MemberID", memID);
                    para.put("Type", type);
                    para.put("Status", status);
                    String  resp = OkHttpUtil.execute(OkHttpUtil.postRequest(mContext.getString(R.string.base_url) + "SetMember", para)).body().string();
                    Gson gson = new Gson();
                    BaseResponse<Boolean> cc = gson.fromJson(resp, new TypeToken<BaseResponse<Boolean>>() {}.getType());
                    if(cc.getMessage().equals("Token 失效！")){
                        RxObject rxObject = new RxObject();
                        rxObject.setTag("Token失效");
                        rxBus.post(rxObject);
                    }else {
                        subscriber.onNext(cc.getResult());
                    }
                } catch (IOException e) {
                    subscriber.onError(e);
                }
                subscriber.onCompleted();
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return mDatas==null?0:mDatas.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView name;
        View line;
        SwitchCompat IsConductor;
        SwitchCompat IsRollCall;
        FrameLayout FramIsConductor;
        FrameLayout FramIsRollCall;

        public MyViewHolder(View view)
        {
            super(view);
            IsConductor = (SwitchCompat) view.findViewById(R.id.IsConductor);
            IsRollCall = (SwitchCompat) view.findViewById(R.id.IsRollCall);
            FramIsConductor = (FrameLayout) view.findViewById(R.id.FramIsConductor);
            FramIsRollCall = (FrameLayout) view.findViewById(R.id.FramIsRollCall);
            name = (TextView)view.findViewById(R.id.name);
            line = view.findViewById(R.id.line);
        }
    }

}
