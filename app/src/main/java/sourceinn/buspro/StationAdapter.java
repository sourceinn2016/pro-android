package sourceinn.buspro;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import sourceinn.buspro.reponse.StationResponse;


public class StationAdapter extends RecyclerView.Adapter {
    protected List<StationResponse> mDatas;
    protected Context mContext;




    public StationAdapter(Context context, List<StationResponse> mDatas){
        mContext = context;
        this.mDatas = mDatas;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.station_item, parent, false);
        return new MyViewHolder(view);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        ((MyViewHolder)holder).name.setText(mDatas.get(position).getName());
        ((MyViewHolder)holder).time.setText(mDatas.get(position).getStopTime());
        ((MyViewHolder)holder).defaultTime.setText(mDatas.get(position).getDefaultTime());
        ((MyViewHolder)holder).outTime.setText(mDatas.get(position).getOutTime());
        if(mDatas.get(position).isShowOutTime()) {
            ((MyViewHolder) holder).outTime.setVisibility(View.VISIBLE);
        }else{
            ((MyViewHolder) holder).outTime.setVisibility(View.GONE);
        }
        if(mDatas.get(position).isShowDefaultTime()){
            ((MyViewHolder)holder).defaultTime.setVisibility(View.VISIBLE);
        }else{
            ((MyViewHolder)holder).defaultTime.setVisibility(View.GONE);
        }

        if(mDatas.get(position).getStatus().length()>0) {
            ((MyViewHolder) holder).bus_status.setImageBitmap(Common.readBitMap(mContext, R.drawable.bus_blue, ((MyViewHolder) holder).bus_status.getWidth(), ((MyViewHolder) holder).bus_status.getHeight()));
            if (mDatas.get(position).getStatus().equals("normal")) {
                ((MyViewHolder)holder).time.setTextColor(mContext.getResources().getColor(R.color.black));
            } else {
                ((MyViewHolder)holder).time.setTextColor(mContext.getResources().getColor(R.color.red));
            }
        }else{
            ((MyViewHolder)holder).time.setTextColor(mContext.getResources().getColor(R.color.black));
            ((MyViewHolder) holder).bus_status.setImageBitmap(Common.readBitMap(mContext, R.drawable.bus_gary, ((MyViewHolder) holder).bus_status.getWidth(), ((MyViewHolder) holder).bus_status.getHeight()));
        }
        if(position==0){
            ((MyViewHolder)holder).lineleft.setVisibility(View.GONE);
            ((MyViewHolder)holder).lineright.setVisibility(View.GONE);
        }else{
            ((MyViewHolder)holder).lineleft.setVisibility(View.VISIBLE);
            ((MyViewHolder)holder).lineright.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public int getItemCount()
    {
        return mDatas.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        ImageView bus_status;
        TextView name;
        TextView time, defaultTime, outTime;
        View lineleft;
        View lineright;


        public MyViewHolder(View view)
        {
            super(view);
            bus_status = (ImageView)view.findViewById(R.id.bus_status);
            name = (TextView)view.findViewById(R.id.name);
            time = (TextView)view.findViewById(R.id.time);
            lineleft = view.findViewById(R.id.lineleft);
            lineright = view.findViewById(R.id.lineright);
            defaultTime = (TextView) view.findViewById(R.id.defaultTime);
            outTime = (TextView) view.findViewById(R.id.outTime);

        }
    }

}
