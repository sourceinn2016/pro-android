package sourceinn.buspro;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.schedulers.TimeInterval;
import sourceinn.buspro.reponse.AbnormalResponse;
import sourceinn.buspro.reponse.BaseResponse;
import sourceinn.buspro.reponse.LoginResponse;
import sourceinn.buspro.reponse.RxObject;

/**
 * Created by sourceinn on 2016/9/14.
 */
public class AbnormalFragment extends BaseFragment implements View.OnClickListener {
    RxBus rxBus = RxBus.getDefault();
    RecyclerView recyclerView;
    protected SwipeRefreshLayout laySwipe;
    AbnormalAdapter adapter;
    List<AbnormalResponse> mData = new ArrayList<>();
    Subscription timeSub;
    Subscription mSubscription;

    public static AbnormalFragment newInstance(){
        return new AbnormalFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.abnormal, null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ImageView bg = (ImageView)view.findViewById(R.id.bg);
        bg.setImageBitmap(Common.readBitMap(mContext, R.drawable.list_background, bg.getWidth(), bg.getHeight()));
        ImageView back = (ImageView)view.findViewById(R.id.back);
        back.setOnClickListener(this);

        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        adapter = new AbnormalAdapter(mContext, mData);
        recyclerView.setAdapter(adapter);
        laySwipe = (SwipeRefreshLayout)view.findViewById(R.id.laySwipe);

        SwipeRefreshLayout.OnRefreshListener onSwipeToRefresh = new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                laySwipe.setRefreshing(true);
                getData();
            }
        };
        laySwipe.setOnRefreshListener(onSwipeToRefresh);

        getData();
    }

    @Override
    public void onPause() {
        super.onPause();
        if(timeSub!=null && !timeSub.isUnsubscribed()){
            timeSub.unsubscribe();
        }
        if(mSubscription!=null && !mSubscription.isUnsubscribed()){
            mSubscription.unsubscribe();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        timeSub = Observable.interval(10000L, TimeUnit.MILLISECONDS).timeInterval()
        .subscribe(new Action1<TimeInterval<Long>>() {
            @Override
            public void call(TimeInterval<Long> longTimeInterval) {
                getData();
            }
        });
    }

    protected void getData(){
        laySwipe.post(new Runnable() {
            @Override
            public void run() {
                laySwipe.setRefreshing(true);
            }
        });
        if(mSubscription!=null && !mSubscription.isUnsubscribed()){
            mSubscription.unsubscribe();
        }
        mSubscription = abnormal().subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        new Action1<List<AbnormalResponse>>() {
                            @Override
                            public void call(List<AbnormalResponse> routeResponses) {
                                adapter.notifyDataSetChanged();
                                laySwipe.setRefreshing(false);
                            }
                        },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable e) {
                                e.printStackTrace();
                                adapter.notifyDataSetChanged();
                                laySwipe.setRefreshing(false);
                            }
                        }
                );

    }

    private Observable<List<AbnormalResponse>> abnormal(){
        return Observable.create(new Observable.OnSubscribe<List<AbnormalResponse>>() {
            @Override
            public void call(Subscriber<? super List<AbnormalResponse>> subscriber) {
                try {

                    String resp = OkHttpUtil.execute(OkHttpUtil.getRequest(getString(R.string.base_url) + "Notification?Token=" +
                            LoginResponse.getLastest(getContext()).getToken())).body().string();
                    Gson gson = new Gson();
                    BaseResponse<List<AbnormalResponse>> cc = gson.fromJson(resp, new TypeToken<BaseResponse<List<AbnormalResponse>>>() {}.getType());


                    mData.clear();
                    if(cc.getData()!=null) {
                        mData.addAll(cc.getData());
                    }
                    subscriber.onNext(cc.getData());
                    //subscriber.onNext(null);
                } catch (Exception e) {
                    subscriber.onError(e);
                }
                subscriber.onCompleted();
            }
        });
    }


    @Override
    public void onClick(View v) {
        RxObject rxObject = new RxObject();;
        switch (v.getId()){
            case R.id.back:
                rxObject.setTag("back");
                rxBus.post(rxObject);
                break;
                
        }
    }
}
