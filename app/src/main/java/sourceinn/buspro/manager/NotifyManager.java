package sourceinn.buspro.manager;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;

import androidx.core.app.NotificationCompat;

import java.util.concurrent.atomic.AtomicInteger;

import sourceinn.buspro.MyService;
import sourceinn.buspro.R;

public class NotifyManager {
    static NotifyManager instance;
    private final static AtomicInteger c = new AtomicInteger(200);
    public final static int foregoundId = 1;
    public final static int gpsReminddId = 2;
    public static NotifyManager getInstance(){
        if(instance==null){
            instance = new NotifyManager();
        }
        return instance;
    }

    public void createNotificationChannel(Context context) {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "channel_name";
            String description = "description";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            String CHANNEL_ID = "sourceinn.buspro";
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    public static int getNextID() {
        return c.incrementAndGet();
    }

    public NotificationCompat.Builder createNormalMsg(Context context, String msg){
        createNotificationChannel(context);
        // Build the notification and add the action.
        return  new NotificationCompat.Builder(context, "sourceinn.buspro")
                .setSmallIcon(R.mipmap.pro_icon)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.pro_icon))
                .setContentTitle("Smartbuz pro")
                .setContentText(msg)
                .setAutoCancel(true)
                .setDefaults(NotificationCompat.DEFAULT_ALL)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
                ;
    }

    public NotificationCompat.Builder createForgroundMsg(Context context, String msg){
        createNotificationChannel(context);
        Intent cancelIntent = new Intent(context, MyService.class);
        cancelIntent.putExtra(MyService.EXTRA_CANCEL_LOCATION_TRACKING_FROM_NOTIFICATION, true);
        PendingIntent cancelPIntent = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S){
            cancelPIntent = PendingIntent.getService(context, 0, cancelIntent,
                    PendingIntent.FLAG_IMMUTABLE |PendingIntent.FLAG_UPDATE_CURRENT);
        }else{
            cancelPIntent = PendingIntent.getService(context, 0, cancelIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        }
        // Build the notification and add the action.
        return new NotificationCompat.Builder(context, "sourceinn.buspro")
                .setSmallIcon(R.mipmap.pro_icon)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.pro_icon))
                .setContentTitle("Smartbuz pro")
                .setContentText(msg)
                .setOngoing(true)
                .setDefaults(NotificationCompat.DEFAULT_ALL)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
                .addAction(R.mipmap.ic_launcher, "強制關閉",cancelPIntent)
                //.setSound(defaultSoundUri)
                //.setContentIntent(pendingIntent)
                ;
    }
}
