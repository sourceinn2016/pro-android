package sourceinn.buspro;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import sourceinn.buspro.reponse.BaseResponse;
import sourceinn.buspro.reponse.LoginResponse;
import sourceinn.buspro.reponse.RollcallResponse;
import sourceinn.buspro.reponse.RouteResponse;
import sourceinn.buspro.reponse.RxObject;

/**
 * Created by sourceinn on 2016/9/14.
 */
public class PushFragment extends BaseFragment implements View.OnClickListener {
    RxBus rxBus = RxBus.getDefault();
    RecyclerView recyclerView;
    PushRouteAdapter pushRouteAdapter;
    PushStudentAdapter pushStudentAdapter;
    List<RouteResponse> spinnerSrc = new ArrayList<>();
    List<RouteResponse> routes = new ArrayList<>();
    List<RollcallResponse> students = new ArrayList<>();
    Subscription mSubscription;
    Spinner routeSpinner;
    SpinnerBaseAdapter spinnerBaseAdapter;
    Spinner canSpinner;
    List<String> tins = new ArrayList<>();
    Button all;
    Button send;
    EditText msg;
    public static PushFragment newInstance(){
        return new PushFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.push, null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        msg = (EditText) view.findViewById(R.id.msg);
        routeSpinner = (Spinner)view.findViewById(R.id.routeSpinner);
        canSpinner = (Spinner)view.findViewById(R.id.can);
        pushStudentAdapter = new PushStudentAdapter(mContext, students);

        canSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if(!tins.get(position).equals(getString(R.string.selectMsg))){
                    msg.setText(tins.get(position));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        routeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                students.clear();
                if(spinnerSrc.get(position).getName().equals(getString(R.string.overRoutePush))){
                    all.setText(getString(R.string.all));
                    all.setVisibility(View.VISIBLE);
                    pushRouteAdapter.notifyChange(routes);
                    recyclerView.setAdapter(pushRouteAdapter);
                }else {
                    Common.showLoading(getChildFragmentManager(), islive);
                    all.setText(getString(R.string.all));
                    all.setVisibility(View.VISIBLE);
                    student(spinnerSrc.get(position).getID()).subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(
                                    new Action1<List<RollcallResponse>>() {
                                        @Override
                                        public void call(List<RollcallResponse> routeResponses) {
                                            pushStudentAdapter.notifyChange(students);
                                            recyclerView.setAdapter(pushStudentAdapter);
                                            Common.hideLoading(getChildFragmentManager(), islive);
                                        }
                                    },
                                    new Action1<Throwable>() {
                                        @Override
                                        public void call(Throwable e) {
                                            e.printStackTrace();
                                            Common.hideLoading(getChildFragmentManager(), islive);
                                        }
                                    }
                            );
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        all = (Button)view.findViewById(R.id.all);
        all.setVisibility(View.GONE);

        all.setOnClickListener(this);

        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new GridLayoutManager(mContext, 3));
        //adapter = new PushRouteAdapter(mContext, routes);
        //recyclerView.setAdapter(adapter);
        recyclerView.setNestedScrollingEnabled(false);
        ImageView back = (ImageView)view.findViewById(R.id.back);
        send = (Button)view.findViewById(R.id.send);
        back.setOnClickListener(this);
        send.setOnClickListener(this);
        getRoute();
        Tins().subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<List<String>>() {
                    @Override
                    public void call(List<String> strings) {
                        tins = strings;
                        tins.add(0, getString(R.string.selectMsg));
                        ArrayAdapter canAdapter = new ArrayAdapter<String>(mContext, R.layout.route_spinner_item, R.id.name, tins);
                        canSpinner.setAdapter(canAdapter);
                    }
                });

    }

    @Override
    public void onPause() {
        super.onPause();
        if(mSubscription!=null && !mSubscription.isUnsubscribed()){
            mSubscription.unsubscribe();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    protected void getRoute(){
        Common.showLoading(getChildFragmentManager(), islive);
        mSubscription = route().subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        new Action1<List<RouteResponse>>() {
                            @Override
                            public void call(List<RouteResponse> routeResponses) {
                                routeSpinner.setAdapter(spinnerBaseAdapter);
                                //recyclerView.setAdapter(adapter);
                                //all.setText("全選");
                                //all.setVisibility(View.VISIBLE);
                                Common.hideLoading(getChildFragmentManager(), islive);
                            }
                        },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable e) {
                                e.printStackTrace();
                                Common.hideLoading(getChildFragmentManager(), islive);
                            }
                        }
                );

    }


    private Observable<List<RouteResponse>> route(){
        return Observable.create(new Observable.OnSubscribe<List<RouteResponse>>() {
            @Override
            public void call(Subscriber<? super List<RouteResponse>> subscriber) {
                try {

                    String resp = OkHttpUtil.execute(OkHttpUtil.getRequest(getString(R.string.base_url) + "Route?Token=" +
                            LoginResponse.getLastest(getContext()).getToken())).body().string();
                    Gson gson = new Gson();
                    BaseResponse<List<RouteResponse>> cc = gson.fromJson(resp, new TypeToken<BaseResponse<List<RouteResponse>>>() {}.getType());
                    RouteResponse op = new RouteResponse();
                    op.setName(getString(R.string.overRoutePush));
                    spinnerSrc.add(op);
                    if(cc.getData()!=null) {
                        routes.addAll(cc.getData());
                        spinnerSrc.addAll(cc.getData());
                    }
                    spinnerBaseAdapter = new SpinnerBaseAdapter(mContext, spinnerSrc);
                    pushRouteAdapter = new PushRouteAdapter(mContext, routes);
                    subscriber.onNext(cc.getData());
                } catch (IOException e) {
                    subscriber.onError(e);
                }
                subscriber.onCompleted();
            }
        });
    }

    private Observable<List<RollcallResponse>> student(final String routeID){

        return Observable.create(new Observable.OnSubscribe<List<RollcallResponse>>() {
            @Override
            public void call(Subscriber<? super List<RollcallResponse>> subscriber) {
                try {
                    String  resp = OkHttpUtil.execute(OkHttpUtil.getRequest(getString(R.string.base_url) + "Rollcall?RouteID=" +routeID+"&Token="+LoginResponse.getLastest(getContext()).getToken())).body().string();
                    //Log.v("QQQ", resp);
                    Gson gson = new Gson();
                    BaseResponse<List<RollcallResponse>> cc = gson.fromJson(resp, new TypeToken<BaseResponse<List<RollcallResponse>>>() {}.getType());

                    students.clear();
                    if(cc.getData()!=null) {
                        students.addAll(cc.getData());
                    }

                    subscriber.onNext(cc.getData());
                } catch (IOException e) {
                    subscriber.onError(e);
                }
                subscriber.onCompleted();
            }
        });

    }
    private Observable<List<String>> Tins(){
        return Observable.create(new Observable.OnSubscribe<List<String>>() {
            @Override
            public void call(Subscriber<? super List<String>> subscriber) {
                try {
                    String  resp = OkHttpUtil.execute(OkHttpUtil.getRequest(getString(R.string.base_url) + "Tins?Token="+LoginResponse.getLastest(getContext()).getToken())).body().string();
                    Gson gson = new Gson();
                    BaseResponse<List<String>> cc = gson.fromJson(resp, new TypeToken<BaseResponse<List<String>>>() {}.getType());
                    //Log.v("QQQ", resp);
                    subscriber.onNext(cc.getData());
                } catch (IOException e) {
                    subscriber.onError(e);
                }
                subscriber.onCompleted();
            }
        });
    }

    private Observable<BaseResponse> sendmsg(final HashMap<String, String> para){

        return Observable.create(new Observable.OnSubscribe<BaseResponse>() {
            @Override
            public void call(Subscriber<? super BaseResponse> subscriber) {
                try {
                    /*for(String key:para.keySet()){
                        Log.v("QQQ", key+" "+para.get(key));
                    }*/
                    String  resp = OkHttpUtil.execute(OkHttpUtil.postRequest(getString(R.string.base_url) + "Broadcast", para)).body().string();
                    Log.v("QQQ", resp);
                    Gson gson = new Gson();
                    BaseResponse cc = gson.fromJson(resp, new TypeToken<BaseResponse>() {}.getType());

                    subscriber.onNext(cc);
                } catch (IOException e) {
                    subscriber.onError(e);
                }
                subscriber.onCompleted();
            }
        });

    }

    @Override
    public void onClick(View v) {
        RxObject rxObject = new RxObject();;
        switch (v.getId()){
            case R.id.back:
                rxObject.setTag("back");
                rxBus.post(rxObject);
                break;
            case R.id.all:
                if(all.getText().toString().equals(getString(R.string.all))){
                    all.setText(getString(R.string.cancelAll));
                    Log.v("QQQ", spinnerSrc.get(routeSpinner.getSelectedItemPosition()).getName());
                    if(spinnerSrc.get(routeSpinner.getSelectedItemPosition()).getName().equals(getString(R.string.overRoutePush)))  {
                        pushRouteAdapter.selectAll(true);
                    }else{
                        pushStudentAdapter.selectAll(true);
                    }
                }else{
                    all.setText(getString(R.string.all));
                    if(spinnerSrc.get(routeSpinner.getSelectedItemPosition()).getName().equals(getString(R.string.overRoutePush)))  {
                        pushRouteAdapter.selectAll(false);
                    }else{
                        pushStudentAdapter.selectAll(false);
                    }
                }
                break;
            case R.id.send:
                HashMap<Integer, Boolean> ss;
                if(spinnerSrc.get(routeSpinner.getSelectedItemPosition()).getName().equals(getString(R.string.overRoutePush)))  {
                    ss = pushRouteAdapter.getSelectList();
                }else{
                    ss = pushStudentAdapter.getSelectList();
                }
                HashMap<String, String> para = new HashMap<String, String>();
                para.put("Token", LoginResponse.getLastest(getContext()).getToken());
                para.put("Message", msg.getText().toString());
                int j=0;
                for (int i=0;i<ss.size();i++){
                    if(ss.get(i)){

                        if(spinnerSrc.get(routeSpinner.getSelectedItemPosition()).getName().equals(getString(R.string.overRoutePush)))  {
                            para.put("RouteID["+j+"]", routes.get(i).getID());
                        }else{
                            para.put("MemberID["+j+"]", students.get(i).getID());
                        }
                        j++;
                    }
                }
                for (Object key : para.keySet()) {
                  Log.v("QQQ", key + " =" + para.get(key));
                }
                if(j>0){
                    Common.showMsgDialog(getChildFragmentManager(), getString(R.string.sendMsgSuccess), null, "noCancel");
                    sendmsg(para).subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Action1<BaseResponse>() {
                                @Override
                                public void call(BaseResponse baseResponse) {
                                    if (baseResponse == null) {

                                    }
                                }
                            }, new Action1<Throwable>() {
                                @Override
                                public void call(Throwable throwable) {

                                }
                            });
                }else{
                    Toast.makeText(mContext, getString(R.string.plsSelectSendWho), Toast.LENGTH_SHORT).show();
                }
                break;

        }
    }
}
