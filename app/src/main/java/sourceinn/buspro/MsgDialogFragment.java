package sourceinn.buspro;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by sourceinn on 2016/4/15.
 */
public class MsgDialogFragment extends DialogFragment {

    Context context;
    public interface MsgDialogLister{void sureEvent();}
    MsgDialogLister msgDialogLister;
    public void setMsgDialogLister(MsgDialogLister l){
        msgDialogLister = l;
    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.context = activity;
    }

    public static MsgDialogFragment newInstance(String msg, String type){
        //Log.v("QQQ", "newInstance");
        Bundle args = new Bundle();
        args.putString("msg", msg);
        args.putString("type", type);
        MsgDialogFragment fragment = new MsgDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        View v = inflater.inflate(R.layout.msgfragment, null);
        Button sure = (Button)v.findViewById(R.id.sure);
        sure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if(msgDialogLister!=null) {
                    msgDialogLister.sureEvent();
                }
            }
        });
        Button cancel = (Button)v.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        if(getArguments().getString("type").equals("noCancel")){
            cancel.setVisibility(View.GONE);
        }
        TextView msg = (TextView)v.findViewById(R.id.msg);
        msg.setText(getArguments().getString("msg"));
        return v;
    }



    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(context, getTheme()) {
            /*@Override
            public void onBackPressed() {
                //do your stuff
                //mCallback.onbackClick( getArguments().getString("type") );
            }*/
        };

        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {

            dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            dialog.setCancelable(false);
            //dialog.setCanceledOnTouchOutside(false);
        }
    }
}