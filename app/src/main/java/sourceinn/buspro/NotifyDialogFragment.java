package sourceinn.buspro;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.HashMap;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import sourceinn.buspro.reponse.BaseResponse;
import sourceinn.buspro.reponse.LoginResponse;

/**
 * Created by sourceinn on 2016/4/15.
 */
public class NotifyDialogFragment extends DialogFragment {
    Context context;
    EditText msg;
    Subscription subscription;

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(subscription!=null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.context = activity;
    }

    public static NotifyDialogFragment newInstance(String id, String msg){
        //Log.v("QQQ", "newInstance");
        Bundle args = new Bundle();
        args.putString("id", id);
        args.putString("msg", msg);
        NotifyDialogFragment fragment = new NotifyDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        View v = inflater.inflate(R.layout.notify_dialog, null);
        Button sure = (Button)v.findViewById(R.id.sure);

        sure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (msg.getText().toString().length() > 0) {
                    Common.showMsgDialog(getChildFragmentManager(), getString(R.string.notifySuccess), new MsgDialogFragment.MsgDialogLister() {
                        @Override
                        public void sureEvent() {
                            dismiss();
                        }
                    }, "noCancel");
                    Sendnotify(getArguments().getString("id"), msg.getText().toString())
                            .subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Action1<BaseResponse>() {
                                @Override
                                public void call(BaseResponse baseResponse) {
                                    //Common.hideLoading(getChildFragmentManager(), ((BaseActivity) context).islive);
                                    if (baseResponse.getResult()!=null && baseResponse.getResult().equals("Success")) {
                                        //dismiss();
                                    } else {
                                        //Common.showMsgDialog(getChildFragmentManager(), baseResponse.getMessage(), null, "noCancel");
                                    }
                                }
                            }, new Action1<Throwable>() {
                                @Override
                                public void call(Throwable throwable) {
                                    //Common.hideLoading(getChildFragmentManager(), ((BaseActivity) context).islive);
                                    throwable.printStackTrace();
                                }
                            });

                } else {
                    Toast.makeText(context, getString(R.string.plsFillContent), Toast.LENGTH_SHORT).show();
                }
            }


        });
        Button cancel = (Button)v.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        msg = (EditText)v.findViewById(R.id.msg);
        msg.setText(getArguments().getString("msg"));
        return v;
    }

    public Observable<BaseResponse> Sendnotify(final String Member_id, final String message){
        return Observable.create(new Observable.OnSubscribe<BaseResponse>() {
            @Override
            public void call(Subscriber<? super BaseResponse> subscriber) {
                try {
                    HashMap<String, String> para = new HashMap<String, String>();
                    para.put("Token", URLDecoder.decode(LoginResponse.getLastest(getContext()).getToken()));
                    para.put("StudentID", Member_id);
                    para.put("Message", message);
                    /*for(String key: para.keySet()){
                        Log.v("QQQ", key+" "+para.get(key));
                    }*/
                    String resp = OkHttpUtil.execute(OkHttpUtil.postRequest(context.getString(R.string.base_url) + "Notify", para)).body().string();
                    Gson gson = new Gson();
                    BaseResponse cc = gson.fromJson(resp, new TypeToken<BaseResponse>() {}.getType());
                    subscriber.onNext(cc);
                } catch (IOException e) {
                    subscriber.onError(e);
                }
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {

            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            dialog.setCancelable(false);
            //dialog.setCanceledOnTouchOutside(false);
        }
    }
}