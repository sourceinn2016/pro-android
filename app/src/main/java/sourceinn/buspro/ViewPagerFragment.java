package sourceinn.buspro;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import sourceinn.buspro.reponse.RouteResponse;
import sourceinn.buspro.reponse.RxObject;

/**
 * Created by sourceinn on 2016/11/30.
 */

public class ViewPagerFragment extends BaseFragment {
    Subscription rxHandle;
    RxBus rxBus = RxBus.getDefault();
    ImageView map;
    public ViewPager viewpager;
    public static ViewPagerFragment newInstance(RouteResponse routeResponse, String type){
        Bundle args = new Bundle();
        args.putSerializable("RouteResponse", routeResponse);
        args.putString("type", type);
        ViewPagerFragment fragment = new ViewPagerFragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.viewpager, null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        viewpager = (ViewPager) view.findViewById(R.id.viewpager);
        ViewPagerAdapter adapter;
        if (getArguments().getString("type")==null){//student
            adapter = new ViewPagerAdapter(getChildFragmentManager(), getContext().getApplicationContext(), null, null);
        }else{
            adapter = new ViewPagerAdapter(getChildFragmentManager(), getContext().getApplicationContext(), (RouteResponse) getArguments().getSerializable("RouteResponse"), getArguments().getString("type"));
        }
        viewpager.setAdapter(adapter);
        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if(position==1){
                    map.setVisibility(View.GONE);
                }else{
                    map.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        map = view.findViewById(R.id.imageview_map);
        map.setOnClickListener(v->{
            viewpager.setCurrentItem(1, true);
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        rxHandle = rxBus.toObservable(RxObject.class)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<RxObject>() {
                    @Override
                    public void call(RxObject rxObject) {
                        if(rxObject.getTag().equals("backToStation")){
                            viewpager.setCurrentItem(0, true);
                        }
                    }
                });
    }

    @Override
    public void onPause() {
        super.onPause();
        if(rxHandle!=null && !rxHandle.isUnsubscribed()){
            rxHandle.unsubscribe();
        }
    }
}
