package sourceinn.buspro.ui.map.presenter;

import android.content.Context;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rx.Observable;
import rx.Single;
import sourceinn.buspro.model.DataManager;
import sourceinn.buspro.reponse.BaseResponse;
import sourceinn.buspro.reponse.LoginResponse;
import sourceinn.buspro.reponse.RouteResponse;
import sourceinn.buspro.reponse.StationResponse;
import sourceinn.buspro.reponse.StudentResponse;
import sourceinn.buspro.ui.map.entity.TrackRp;

public class MapPresenter {
    DataManager dataManager = new DataManager();
    public Single<List<StationResponse>> getStationForAdminParent(String route_id){
        HashMap para = new HashMap();
        para.put("RouteID", route_id);
        return dataManager.getStationListForAdminParent(para)
                .toSingle();
    }

    public Single<StationResponse> getLocation(Context context, String route_id){
        HashMap<String, String> para = new HashMap<String, String>();
        para.put("RouteID", route_id);
        para.put("Token", LoginResponse.getLastest(context).getToken());
        return dataManager.RouteLocation(para);
    }

    public Single<StudentResponse> getStationListForStudentDriver(Context context){
        HashMap para = new HashMap();
        para.put("Token", LoginResponse.getLastest(context).getToken());
        return dataManager.getStationListForStudentDriver(para)
                .toSingle()
                .map(response->response.getData());
    }

    public Single<List<TrackRp>> getTrack(String route_id, String category){
        HashMap<String, String> para = new HashMap<String, String>();
        para.put("routeId", route_id);
        para.put("category", category);
        return dataManager.GetDefaultTrack(para);
    }
}
