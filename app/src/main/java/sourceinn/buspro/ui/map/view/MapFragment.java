package sourceinn.buspro.ui.map.view;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.schedulers.TimeInterval;
import sourceinn.buspro.BaseFragment;
import sourceinn.buspro.Common;
import sourceinn.buspro.OkHttpUtil;
import sourceinn.buspro.R;
import sourceinn.buspro.RxBus;
import sourceinn.buspro.databinding.FgMapBinding;
import sourceinn.buspro.reponse.BaseResponse;
import sourceinn.buspro.reponse.LoginResponse;
import sourceinn.buspro.reponse.RouteResponse;
import sourceinn.buspro.reponse.RxObject;
import sourceinn.buspro.reponse.StationResponse;
import sourceinn.buspro.reponse.StudentResponse;
import sourceinn.buspro.ui.map.presenter.MapPresenter;
import sourceinn.buspro.util.DateTimeUtil;

/**
 * Created by sourceinn on 2016/12/5.
 */

public class MapFragment extends BaseFragment {
    RxBus rxBus = RxBus.getDefault();
    GoogleMap mMap;
    FgMapBinding binding;

    String RouteID;
    Subscription timeSub;
    Marker currenMarker;
    boolean hasIni = false;

    MapPresenter presenter;

    boolean selectIsAm = true;
    Polyline polyline;
    //Bitmap cartBitmap;
    //student
    public static MapFragment newInstance() {
        MapFragment fragment = new MapFragment();
        return fragment;
    }

    //admin or parent
    public static MapFragment newInstance(RouteResponse routeResponse) {
        Bundle args = new Bundle();
        args.putSerializable("RouteResponse", routeResponse);
        MapFragment fragment = new MapFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FgMapBinding.inflate(inflater, null, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter = new MapPresenter();
        binding.findcart.setVisibility(View.GONE);
        binding.findcart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currenMarker != null) {
                    LatLng sydney = currenMarker.getPosition();
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
                    currenMarker.showInfoWindow();
                }
            }
        });
        binding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RxObject rxObject = new RxObject();
                rxObject.setTag("backToStation");
                rxBus.post(rxObject);
            }
        });
        binding.mapView.onCreate(savedInstanceState);
        binding.mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mMap = googleMap;
                //Log.v("QQQ", "getMapAsync");
                // Add a marker in Sydney and move the camera
                //if(checkPermission()) {
                //mMap.setMyLocationEnabled(false); // 右上角的定位功能；這行會出現紅色底線，不過仍可正常編譯執行
                //}
                /*}else{
                    requestPermissions( new String[]{
                                    Manifest.permission.ACCESS_FINE_LOCATION,
                                    Manifest.permission.ACCESS_COARSE_LOCATION
                            },
                            1234);
                }*/
                mMap.getUiSettings().setZoomControlsEnabled(true);  // 右下角的放大縮小功能
                mMap.getUiSettings().setCompassEnabled(true);       // 左上角的指南針，要兩指旋轉才會出現
                mMap.getUiSettings().setMapToolbarEnabled(false);    // 右下角的導覽及開啟 Google Map功能

                selectIsAm = Integer.parseInt(DateTimeUtil.getTimeString(System.currentTimeMillis(), "HH"))<12;
                binding.textviewAmpm.setText(selectIsAm?"AM":"PM");
                if (LoginResponse.getLastest(getContext()).getResult().equals("student") ||
                        LoginResponse.getLastest(getContext()).getResult().equals("driver")) {
                    //student()
                            /*.flatMap(new Func1<BaseResponse<StudentResponse>, Observable<List<StationResponse>>>() {
                        @Override
                        public Observable<List<StationResponse>> call(BaseResponse<StudentResponse> studentResponseBaseResponse) {
                            return route(studentResponseBaseResponse.getData().getRouteID());
                        }
                    })*/
                    presenter.getStationListForStudentDriver(requireContext())
                            .subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(stationResponse -> {
                                setStation(stationResponse.getList());
                                RouteID = stationResponse.getRouteID();
                                getLasteLocation();//此時有路限ID  就可以找車輛位置
                                getTrack(RouteID, selectIsAm);
                            },
                                    e -> e.printStackTrace());
                } else {
                    RouteResponse routeResponse = (RouteResponse) getArguments().getSerializable("RouteResponse");
                    RouteID = routeResponse.getID();
                    getLasteLocation();
                    getTrack(RouteID, selectIsAm);
                    presenter.getStationForAdminParent(RouteID)
                        .subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(
                                    new Action1<List<StationResponse>>() {
                                        @Override
                                        public void call(List<StationResponse> stationResponses) {
                                            setStation(stationResponses);
                                        }
                                    },
                                    new Action1<Throwable>() {
                                        @Override
                                        public void call(Throwable e) {
                                            e.printStackTrace();
                                        }
                                    }
                            );
                }
            }
        });

        binding.textviewAmpm.setOnClickListener(view1 ->{
            selectIsAm = !selectIsAm;
            binding.textviewAmpm.setText(selectIsAm?"AM":"PM");
            getTrack(RouteID, selectIsAm);
        });

    }

    public boolean checkPermission() {
        return ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    /*@Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode==1234) {
            if (Common.checkGrantResult(grantResults)) {
                mMap.setMyLocationEnabled(true);
            }else{

            }
        }
    }*/

    private void setStation(List<StationResponse> list) {
        //Log.v("QQQ", "setStation");
        LatLng sydney = null;
        for (int i = 0; i < list.size(); i++) {
            sydney = new LatLng(Double.parseDouble(list.get(i).getLatitude()), Double.parseDouble(list.get(i).getLongitude()));
            Marker ma = mMap.addMarker(new MarkerOptions().position(sydney).title(list.get(i).getName())
                    //.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET))
            );
            //ma.showInfoWindow();
            if (i == 0) {
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 13.5f));
            }
        }
        //sydney = new LatLng(25.016689, 121.262478);
        //mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 14));

    }

    //取得車輛未置
    private void getLasteLocation() {
        if(RouteID!=null) {
            onResumeCompositeSubscription.add(
                    presenter.getLocation(requireContext(), RouteID)
                            .subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(data -> {
                                updateCurrentLocation(data);
                            }, error -> {
                                Log.v("QQQ", "getLocation error");
                                error.printStackTrace();
                            })
            );
        }
    }

    private void getTrack(String routeId, boolean selectIsAm) {
        if(RouteID!=null) {
            if(polyline!=null) {
                polyline.remove();
            }
            onResumeCompositeSubscription.add(
                    presenter.getTrack(routeId, selectIsAm?"0":"1")
                            .subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(data -> {
                                PolylineOptions polylineOptions = new PolylineOptions();
                                polylineOptions.color(Color.parseColor("#008000"));
                                for (int i=0; i<data.size(); i++){
                                    polylineOptions.add(new LatLng(data.get(i).getLatitude(), data.get(i).getLongitude()));
                                }
                                polyline = mMap.addPolyline(polylineOptions);
                            }, error -> {
                                Log.v("QQQ", "getTrack error");
                                error.printStackTrace();
                                //Toast.makeText(requireContext(), "路線軌跡尚未設置", Toast.LENGTH_SHORT).show();
                            })
            );
        }
    }

    //更新車輛位置
    private void updateCurrentLocation(StationResponse staionData) {
        /*int dd = (int) Common.getDensity(mContext)*40;
        Bitmap.Config conf = Bitmap.Config.ARGB_8888;
        Bitmap bmp = Bitmap.createBitmap(dd, dd, conf);


        Bitmap   cartBitmap = Common.readBitMap(mContext, R.drawable.bus_blue, dd, dd);


        Canvas canvas = new Canvas(bmp);
        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);

            Canvas canvas1 = new Canvas(bmp);
            // paint defines the text color, stroke width and size
            Paint color = new Paint();
            color.setTextSize(35);
            color.setColor(Color.BLACK);

            // modify canvas
            canvas1.drawBitmap(cartBitmap, 0,0, color);
            canvas1.drawText("User Name!", 30, 40, color);*/
        //Log.v("QQQ", "updateCurrentLocation");


        if (currenMarker != null) {
            currenMarker.remove();
        }
        if (staionData.getLatitude() == null || staionData.getLongitude() == null || staionData.getLatitude().length() == 0 || staionData.getLongitude().length() == 0) {
            binding.findcart.setVisibility(View.GONE);
            return;
        }
        binding.findcart.setVisibility(View.VISIBLE);
        LatLng sydney = new LatLng(Double.parseDouble(staionData.getLatitude()), Double.parseDouble(staionData.getLongitude()));
        int size = (int) (Common.getDensity(mContext) * 25);
        currenMarker = mMap.addMarker(new MarkerOptions().position(sydney).title(getString(R.string.carPosition))
                        .icon(BitmapDescriptorFactory.fromBitmap(Common.readBitMap(mContext, R.drawable.yellowbusicon, size, size)))
                //.icon(BitmapDescriptorFactory.fromBitmap(bmp))
                //.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
        );

        /*if(!hasIni){
            hasIni = true;
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 14));
            currenMarker.showInfoWindow();
        }*/
    }

    @Override
    public void onResume() {
        super.onResume();
        binding.mapView.onResume();
        timeSub = Observable.interval(10000L, TimeUnit.MILLISECONDS).timeInterval()
                .subscribe(new Action1<TimeInterval<Long>>() {
                    @Override
                    public void call(TimeInterval<Long> longTimeInterval) {
                        getLasteLocation();
                    }
                });
    }

    @Override
    public void onPause() {
        super.onPause();
        binding.mapView.onPause();
        if (timeSub != null && !timeSub.isUnsubscribed()) {
            timeSub.unsubscribe();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding.mapView.onDestroy();
    }
}
