package sourceinn.buspro.ui.login.presenter;

import java.util.Map;

import rx.Single;
import sourceinn.buspro.model.DataManager;
import sourceinn.buspro.reponse.LoginResponse;

public class LoginPresenter {
    DataManager dataManager = new DataManager();
    public Single<LoginResponse> login(Map map){
        return dataManager.login(map);
    }

    public Single<LoginResponse> changeRoute(Map map){
        return dataManager.changeRoute(map);
    }
}
