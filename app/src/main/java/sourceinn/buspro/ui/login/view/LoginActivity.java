package sourceinn.buspro.ui.login.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;


import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;


import java.io.IOException;
import java.util.HashMap;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

import sourceinn.buspro.BaseActivity;
import sourceinn.buspro.Common;
import sourceinn.buspro.DBHelper;
import sourceinn.buspro.MainActivity;
import sourceinn.buspro.MsgDialogFragment;
import sourceinn.buspro.OkHttpUtil;
import sourceinn.buspro.R;
import sourceinn.buspro.RouteSelectDialogFragment;
import sourceinn.buspro.fcm.MyFirebaseMessagingService;
import sourceinn.buspro.reponse.BaseResponse;
import sourceinn.buspro.reponse.LoginResponse;
import sourceinn.buspro.reponse.RouteResponse;
import sourceinn.buspro.ui.login.presenter.LoginPresenter;

public class LoginActivity extends BaseActivity implements View.OnClickListener {
    EditText mobile;
    EditText password;
    ImageView logo;
    ImageView loading_logo;
    LinearLayout input_area;
    LoginResponse loginInfo;
    Button login;
    String versionName;
    LoginPresenter presenter;
    boolean isFromNotify = false;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.login);
        presenter = new LoginPresenter();
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        versionName = pInfo.versionName;

        loginInfo = Common.getLoginInfo(this);
        input_area = (LinearLayout)findViewById(R.id.input_area);
        ImageView bg = (ImageView)findViewById(R.id.bg);
        bg.setImageBitmap(Common.readBitMap(this, R.drawable.bg_blue, bg.getWidth(), bg.getHeight()));
        logo = (ImageView)findViewById(R.id.logo);
        logo.setImageBitmap(Common.readBitMap(this, R.drawable.loading_logo, logo.getWidth(), logo.getHeight()));
        loading_logo = (ImageView)findViewById(R.id.loading_logo);
        loading_logo.setImageBitmap(Common.readBitMap(this, R.drawable.loading_logo, loading_logo.getWidth(), loading_logo.getHeight()));
        input_area.setVisibility(View.GONE);
        logo.setVisibility(View.GONE);
        loading_logo.setVisibility(View.VISIBLE);
        if (checkPlayServices()) {
            // Start IntentService to register this application with GCM.
            //Intent intent = new Intent(this, RegistrationIntentService.class);
            //startService(intent);
        }else{
            //Toast.makeText(this, "This device is not supported gcm.", Toast.LENGTH_SHORT).show();
            //finish();
        }

        login = (Button)findViewById(R.id.login_btn);
        //login.setLetterSpacing(1.8f);
        mobile = (EditText)findViewById(R.id.mobile);
        password = (EditText)findViewById(R.id.password);
        login.setOnClickListener(this);

        //測試用
        LinearLayout test_area = (LinearLayout) findViewById(R.id.test_area);
        test_area.setVisibility(View.GONE);
        Button admin = (Button) findViewById(R.id.admin);
        Button student1 = (Button) findViewById(R.id.student1);
        Button student2 = (Button) findViewById(R.id.student2);
        Button parents = (Button) findViewById(R.id.parents);
        admin.setOnClickListener(this);
        student1.setOnClickListener(this);
        student2.setOnClickListener(this);
        parents.setOnClickListener(this);

        isFromNotify = getIntent().getBooleanExtra("openMessage", false);

        Observable.create(new Observable.OnSubscribe<Object>() {
            @Override
            public void call(Subscriber<? super Object> subscriber) {
                try {
                    Thread.sleep(1000);
                    subscriber.onNext(new Object());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Object>() {
                    @Override
                    public void call(Object o) {
                        input_area.setVisibility(View.VISIBLE);
                        logo.setVisibility(View.VISIBLE);
                        Animation animationFadeIn = AnimationUtils.loadAnimation(LoginActivity.this, R.anim.fadein);
                        input_area.startAnimation(animationFadeIn);
                        logo.startAnimation(animationFadeIn);
                        loading_logo.setVisibility(View.GONE);
                        if (loginInfo != null) {
                            /*//確認Token有無效
                            ValidateToken().subscribeOn(Schedulers.newThread())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(new Action1<BaseResponse>() {
                                        @Override
                                        public void call(BaseResponse baseResponse) {
                                            if(baseResponse.getResult().equals("Success")){
                                                Intent intent = new Intent();
                                                intent.setClass(LoginActivity.this, MainActivity.class);
                                                startActivity(intent);
                                                finish();
                                            }else{
                                                mobile.setText(loginInfo.getMobile());
                                                password.setText(loginInfo.getPassword());
                                                login.performClick();
                                            }
                                        }
                                    });*/


                            mobile.setText(loginInfo.getMobile());
                            password.setText(loginInfo.getPassword());
                            if(loginInfo.getPassword().length()>0) {
                                login.performClick();
                            }

                        }
                    }
                });

    }

    public Observable<BaseResponse> ValidateToken(){
        return Observable.create(new Observable.OnSubscribe<BaseResponse>() {
            @Override
            public void call(Subscriber<? super BaseResponse> subscriber) {
                HashMap<String, String> para = new HashMap<String, String>();
                para.put("Token", loginInfo.getToken());
                try {
                    String resp = OkHttpUtil.execute(OkHttpUtil.postRequest(getString(R.string.base_url)+"ValidateToken", para)).body().string();
                    Gson gson = new Gson();
                    BaseResponse cc = gson.fromJson(resp, new TypeToken<BaseResponse>() {}.getType());
                    subscriber.onNext(cc);

                    //Log.v("QQQ", resp);
                } catch (IOException e) {
                    subscriber.onError(e);
                }
                subscriber.onCompleted();
            }
        });
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, 9000, new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        Toast.makeText(LoginActivity.this, getString(R.string.needUpdate), Toast.LENGTH_SHORT).show();
                        finish();
                    }
                })
                        .show();
            } else {
                //Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.login_btn:
                if(!Common.checkIntenet(this)){
                    Common.showMsgDialog(getSupportFragmentManager(), getString(R.string.plsOpenInternet), null, "noCancel");
                }else if (mobile.getText().toString().equals("") || password.getText().toString().equals("")) {
                    Toast.makeText(this, getString(R.string.plsFillPhonePwd), Toast.LENGTH_SHORT).show();
                } else {
                    Common.showLoading(getSupportFragmentManager(), islive);
                    /*login().subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(subscriber);*/
                    if(mSubcription!=null && !mSubcription.isUnsubscribed()){
                        mSubcription.unsubscribe();
                    }
                    HashMap<String, String> para = new HashMap<String, String>();
                    para.put("Mobile", mobile.getText().toString());
                    para.put("Password", password.getText().toString());
                    para.put("RegID", DBHelper.getMapByKey(LoginActivity.this, MyFirebaseMessagingService.FCM_KEY));
                    para.put("DeviceType", "android");
                    para.put("Ver", versionName);
                    mSubcription = presenter.login(para)
                            .subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(
                                new Action1<LoginResponse>() {
                                    @Override
                                    public void call(LoginResponse loginResponse) {
                                        Common.hideLoading(getSupportFragmentManager(), islive);
                                        if(loginResponse.getResult().equals("Error")){
                                            Common.showMsgDialog(getSupportFragmentManager(), loginResponse.getMessage(), null, "noCancel");
                                        }else{//success
                                            loginInfo = loginResponse;
                                            loginInfo.setMobile(mobile.getText().toString());
                                            loginInfo.setPassword(password.getText().toString());
                                            if(loginInfo.getData()==null){//一條路線
                                                Common.setLoginInfo(LoginActivity.this, loginInfo);
                                                Intent intent = new Intent();
                                                intent.putExtra("openMessage", isFromNotify);
                                                intent.setClass(LoginActivity.this, MainActivity.class);
                                                startActivity(intent);
                                                finish();
                                            }else{//多條路線
                                                overOneRoute();
                                            }

                                        }
                                    }
                                },
                                new Action1<Throwable>() {
                                    @Override
                                    public void call(Throwable throwable) {
                                        Common.hideLoading(getSupportFragmentManager(), islive);
                                        throwable.printStackTrace();
                                    }
                                }
                            );


                }
                break;
            //測試
            case R.id.admin:
                mobile.setText("frank.shen.fs@gmail.com");
                password.setText("@978o85817");
                break;
            case R.id.parents:
                mobile.setText("0911123456");
                password.setText("@978o85817");

                break;
            case R.id.student1://車長
                mobile.setText("student1");
                password.setText("@978o85817");
                break;
            case R.id.student2://一般學生
                mobile.setText("student2");
                password.setText("@978o85817");
                break;
        }
    }

    public void overOneRoute(){
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Fragment prev = fm.findFragmentByTag("selectRoute");
        if (prev != null) {
            ft.remove(prev);
        }
        RouteSelectDialogFragment newFragment = RouteSelectDialogFragment.newInstance(loginInfo.getData());
        newFragment.setMsgDialogLister(new RouteSelectDialogFragment.RouteSelectDialogLister() {
            @Override
            public void selected(final RouteResponse s) {
                Common.showLoading(getSupportFragmentManager(), islive);
                ChangeRoute(loginInfo.getToken(), s.getID())
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Action1<LoginResponse>() {
                            @Override
                            public void call(LoginResponse o) {
                                Common.hideLoading(getSupportFragmentManager(), islive);
                                if(o.getResult().equals("Success")){
                                    loginInfo.setToken(o.getToken());
                                    Common.setLoginInfo(LoginActivity.this, loginInfo);
                                    Intent intent = new Intent();
                                    intent.putExtra("openMessage", isFromNotify);
                                    intent.setClass(LoginActivity.this, MainActivity.class);
                                    startActivity(intent);
                                    finish();
                                }else{
                                    Common.showMsgDialog(getSupportFragmentManager(), o.getMessage(), new MsgDialogFragment.MsgDialogLister() {
                                        @Override
                                        public void sureEvent() {

                                        }
                                    }, "noCancel");
                                }
                            }
                        });


            }
        });
        ft.add(newFragment, "selectRoute");
        ft.commit();
    }

    public Observable<LoginResponse> ChangeRoute(final String token, final String route_id){
        return Observable.create(new Observable.OnSubscribe<LoginResponse>() {
            @Override
            public void call(Subscriber<? super LoginResponse> subscriber) {
                HashMap<String, String> para = new HashMap<String, String>();
                para.put("Token", token);
                para.put("RouteID", route_id);
                try {
                    String resp = OkHttpUtil.execute(OkHttpUtil.postRequest(getString(R.string.base_url)+"ChangeRoute", para)).body().string();
                    Gson gson = new Gson();
                    LoginResponse cc = gson.fromJson(resp, new TypeToken<LoginResponse>() {}.getType());
                    subscriber.onNext(cc);

                    //Log.v("QQQ", resp);
                } catch (IOException e) {
                    subscriber.onError(e);
                }
                subscriber.onCompleted();
            }
        });
    }


}
