package sourceinn.buspro.ui.route.presenter;

import java.util.List;
import java.util.Map;

import rx.Single;
import sourceinn.buspro.model.DataManager;
import sourceinn.buspro.reponse.BaseResponse;
import sourceinn.buspro.reponse.LoginResponse;
import sourceinn.buspro.reponse.RouteResponse;

public class RoutePresenter {
    DataManager dataManager = new DataManager();
    public Single<BaseResponse<List<RouteResponse>>> getRoute(Map map){
        return dataManager.getRoute(map);
    }
}
