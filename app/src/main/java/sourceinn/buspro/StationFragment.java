package sourceinn.buspro;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import sourceinn.buspro.databinding.FgStationBinding;
import sourceinn.buspro.presenter.StationPresenter;
import sourceinn.buspro.reponse.BaseResponse;
import sourceinn.buspro.reponse.LoginResponse;
import sourceinn.buspro.reponse.RouteResponse;
import sourceinn.buspro.reponse.RxObject;

/**
 * Created by sourceinn on 2016/9/14.
 */
public class StationFragment extends BaseFragment implements View.OnClickListener, StationPresenter.CB {
    RxBus rxBus = RxBus.getDefault();
    FgStationBinding binding;
    RecyclerView recyclerView;
    protected SwipeRefreshLayout laySwipe;
    TextView title;
    View line;
    StationAdapter adapter;
    //List<StationResponse> mData = new ArrayList<>();
    Button changeRoute;
    RelativeLayout relativeLayout;

    TextView nfcMsg;
    RelativeLayout nfcArea;

    StationPresenter presenter;
    RelativeLayout disclosureArea;

    @Override
    public void onPause() {
        super.onPause();
        presenter.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onResume();
    }



    //student
    public static StationFragment newInstance(){
        StationFragment fragment = new StationFragment();
        return fragment;
    }

    //admin or parent
    public static StationFragment newInstance(RouteResponse routeResponse, String type){
        Bundle args = new Bundle();
        args.putSerializable("RouteResponse", routeResponse);
        args.putString("type", type);//為了只有一條路線 自動跳轉到站點列表  然後隱藏ㄧ些icon
        StationFragment fragment = new StationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FgStationBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        disclosureArea = view.findViewById(R.id.disclosureArea);
        disclosureArea.setVisibility(View.GONE);
        ImageView closeDisclosure = view.findViewById(R.id.closeDisclosure);
        closeDisclosure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                disclosureArea.setVisibility(View.GONE);
            }
        });
        nfcMsg = (TextView) view.findViewById(R.id.nfcMsg);
        nfcArea = (RelativeLayout) view.findViewById(R.id.nfcArea);
        nfcArea.setOnClickListener(this);
        relativeLayout = (RelativeLayout) view.findViewById(R.id.relativeLayout);
        ImageView bg = (ImageView)view.findViewById(R.id.bg);
        bg.setImageBitmap(Common.readBitMap(mContext, R.drawable.list_background, bg.getWidth(), bg.getHeight()));
        line = view.findViewById(R.id.line);
        line.setVisibility(View.GONE);
        title = (TextView)view.findViewById(R.id.title);
        ImageView logout =(ImageView)view.findViewById(R.id.logout);
        ImageView setting = (ImageView)view.findViewById(R.id.setting);
        ImageView dispatch = (ImageView)view.findViewById(R.id.dispatch);
        ImageView back = (ImageView)view.findViewById(R.id.back);
        ImageView rollcall = (ImageView)view.findViewById(R.id.rollcall);
        Button message = view.findViewById(R.id.message);
        message.setOnClickListener(this);
        logout.setOnClickListener(this);
        setting.setOnClickListener(this);
        dispatch.setOnClickListener(this);
        back.setOnClickListener(this);
        rollcall.setOnClickListener(this);
        int dp = (int) Common.getDensity(mContext);
        dispatch.setImageBitmap(Common.readBitMap(mContext, R.drawable.edit_user, 40*dp, 40*dp));
        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));

        laySwipe = (SwipeRefreshLayout)view.findViewById(R.id.laySwipe);
        SwipeRefreshLayout.OnRefreshListener onSwipeToRefresh = new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                laySwipe.setRefreshing(true);
                presenter.requestData();
            }
        };
        laySwipe.setOnRefreshListener(onSwipeToRefresh);
        if(LoginResponse.getLastest(getContext()).isRollCall()){
            nfcArea.setVisibility(View.VISIBLE);
            rollcall.setVisibility(View.VISIBLE);
        }else{
            nfcArea.setVisibility(View.GONE);
            rollcall.setVisibility(View.GONE);
        }


        if(LoginResponse.getLastest(getContext()).getResult().equals("student") || LoginResponse.getLastest(getContext()).getResult().equals("driver")){
            logout.setVisibility(View.VISIBLE);
            setting.setVisibility(View.VISIBLE);
            dispatch.setVisibility(View.INVISIBLE);
            back.setVisibility(View.GONE);
        }else if(LoginResponse.getLastest(getContext()).getResult().equals("admin")){
            logout.setVisibility(View.GONE);
            setting.setVisibility(View.GONE);
            dispatch.setVisibility(View.VISIBLE);
            //dispatch.setVisibility(View.INVISIBLE);
            back.setVisibility(View.VISIBLE);
            //rollcall.setVisibility(View.GONE);
            RouteResponse routeResponse = (RouteResponse) getArguments().getSerializable("RouteResponse");
            title.setText(routeResponse.getName());
        }else if(LoginResponse.getLastest(getContext()).getResult().equals("parents")){
            //rollcall.setVisibility(View.GONE);
            RouteResponse routeResponse = (RouteResponse) getArguments().getSerializable("RouteResponse");
            title.setText(routeResponse.getName());
            dispatch.setVisibility(View.INVISIBLE);
            if(getArguments().getString("type").equals("onlyOne")){
                logout.setVisibility(View.VISIBLE);
                setting.setVisibility(View.VISIBLE);
                back.setVisibility(View.GONE);
            }else {
                logout.setVisibility(View.GONE);
                setting.setVisibility(View.GONE);
                back.setVisibility(View.VISIBLE);
            }
        }


        changeRoute = (Button) view.findViewById(R.id.changeRoute);
        changeRoute.setOnClickListener(this);
        changeRoute.setVisibility(LoginResponse.getLastest(getContext()).isConductor()?View.VISIBLE:View.GONE);


        presenter = new StationPresenter(this);
        adapter = new StationAdapter(mContext, presenter.getStations());
        recyclerView.setAdapter(adapter);
        presenter.onCreate();
    }

    public RouteResponse getRouteRp(){
        if(getArguments()!=null) {
            return (RouteResponse) getArguments().getSerializable("RouteResponse");
        }else{
            return null;
        }
    }


    public Observable<LoginResponse> ChangeRoute(final String token, final String route_id){
        return Observable.create(new Observable.OnSubscribe<LoginResponse>() {
            @Override
            public void call(Subscriber<? super LoginResponse> subscriber) {
                HashMap<String, String> para = new HashMap<String, String>();
                para.put("Token", token);
                para.put("RouteID", route_id);
                try {
                    String resp = OkHttpUtil.execute(OkHttpUtil.postRequest(getString(R.string.base_url)+"ChangeRoute", para)).body().string();
                    Gson gson = new Gson();
                    LoginResponse cc = gson.fromJson(resp, new TypeToken<LoginResponse>() {}.getType());
                    subscriber.onNext(cc);

                    //Log.v("QQQ", resp);
                } catch (IOException e) {
                    subscriber.onError(e);
                }
                subscriber.onCompleted();
            }
        });
    }

    public Observable<BaseResponse<List<RouteResponse>>> CheckRoute(){
        return Observable.create(new Observable.OnSubscribe<BaseResponse<List<RouteResponse>>>() {
            @Override
            public void call(Subscriber<? super BaseResponse<List<RouteResponse>>> subscriber) {
                HashMap<String, String> para = new HashMap<String, String>();
                para.put("Token", LoginResponse.getLastest(getContext()).getToken());
                try {
                    String resp = OkHttpUtil.execute(OkHttpUtil.postRequest(getString(R.string.base_url)+"CheckRoute", para)).body().string();
                    Gson gson = new Gson();
                    BaseResponse<List<RouteResponse>> cc = gson.fromJson(resp, new TypeToken<BaseResponse<List<RouteResponse>>>() {}.getType());
                    subscriber.onNext(cc);

                    //Log.v("QQQ", resp);
                } catch (IOException e) {
                    subscriber.onError(e);
                }
                subscriber.onCompleted();
            }
        });
    }

    @Override
    public void onClick(View v) {
        RxObject rxObject= new RxObject();
        switch (v.getId()){
            case R.id.logout:
                presenter.onLogout();
                break;
            case R.id.setting:
                rxObject.setTag("setting");
                rxBus.post(rxObject);
                break;
            case R.id.dispatch:
                rxObject.setTag("dispatch");
                RouteResponse routeResponse = (RouteResponse) getArguments().getSerializable("RouteResponse");
                rxObject.setObject(routeResponse.getID());
                rxBus.post(rxObject);
                break;
            case R.id.back:
                rxObject.setTag("back");
                rxBus.post(rxObject);
                break;
            case R.id.rollcall:
            case R.id.nfcArea:
                rxObject.setTag("StationToRollcall");
                rxObject.setObject(title.getText().toString());
                rxBus.post(rxObject);
                break;
            case R.id.changeRoute:
                //Common.showLoading(getChildFragmentManager(), islive);
                CheckRoute().subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Action1<BaseResponse<List<RouteResponse>>>() {
                            @Override
                            public void call(BaseResponse<List<RouteResponse>> baseResponse) {
                                //Common.hideLoading(getChildFragmentManager(), islive);
                                showRouteSelect(baseResponse.getData());
                            }
                        });

                break;
            case R.id.message:
                PushMessageDialogFg.showPushMessageDialogFg(getChildFragmentManager());
                break;
        }
    }

    private void showRouteSelect(List<RouteResponse> routeResponses){
        FragmentManager fm = getChildFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Fragment prev = fm.findFragmentByTag("selectRoute");
        if (prev != null) {
            ft.remove(prev);
        }
        RouteSelectDialogFragment newFragment = RouteSelectDialogFragment.newInstance(routeResponses);
        newFragment.setMsgDialogLister(new RouteSelectDialogFragment.RouteSelectDialogLister() {
            @Override
            public void selected(final RouteResponse s) {
                //Common.showLoading(getChildFragmentManager(), islive);
                //title.setText(s.getName());
                ChangeRoute(LoginResponse.getLastest(getContext()).getToken(), s.getID())
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Action1<LoginResponse>() {
                            @Override
                            public void call(LoginResponse o) {
                                //Common.hideLoading(getChildFragmentManager(), islive);
                                if(o.getResult().equals("Success")){
                                    Common.updateToken(mContext, o.getToken());
                                    RxObject rxObject = new RxObject();
                                    rxObject.setTag("ChangeRoute");
                                    rxBus.post(rxObject);
                                }else{
                                    Common.showMsgDialog(getChildFragmentManager(), o.getMessage(), new MsgDialogFragment.MsgDialogLister() {
                                        @Override
                                        public void sureEvent() {

                                        }
                                    }, "noCancel");
                                }
                            }
                        });


            }
        });
        ft.add(newFragment, "selectRoute");
        ft.commit();
    }

    @Override
    public void dataRefreshDone() {
        adapter.notifyDataSetChanged();
    }

    @Override
    public void showLoading() {
        laySwipe.setRefreshing(true);
    }
    @Override
    public void hideLoading() {
        laySwipe.setRefreshing(false);
    }

    @Override
    public void setNfcText(String text) {
        nfcMsg.setText(text);
    }

    @Override
    public void setDiscloseureVisable(boolean flag) {
        if(flag){disclosureArea.setVisibility(View.VISIBLE);}else{
            disclosureArea.setVisibility(View.GONE);
        }
    }

    @Override
    public void setRouteName(String routeName) {
        title.setText(routeName);
    }

    @Override
    public void onDestroyView() {
        presenter.onDestroy();
        super.onDestroyView();
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override
    public void onStop() {
        presenter.onStop();
        super.onStop();
    }
}
