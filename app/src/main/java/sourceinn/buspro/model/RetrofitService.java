package sourceinn.buspro.model;

import java.util.List;
import java.util.Map;

import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;
import rx.Observable;
import rx.Single;
import sourceinn.buspro.reponse.BaseResponse;
import sourceinn.buspro.reponse.DistanceRp;
import sourceinn.buspro.reponse.LoginResponse;
import sourceinn.buspro.reponse.RouteResponse;
import sourceinn.buspro.reponse.StationResponse;
import sourceinn.buspro.reponse.StudentResponse;
import sourceinn.buspro.ui.map.entity.TrackRp;

/**
 * Created by sourceinnW on 2018/4/10.
 */

public interface RetrofitService {

    @FormUrlEncoded
    @POST("Login")
    Single<LoginResponse> login(@FieldMap Map<String, String> para);

    @GET("Student")
    Observable<BaseResponse<StudentResponse>> getStationListForStudentDriver(@QueryMap Map<String, String> para);

    @GET("Station")
    Observable<List<StationResponse>> getStationListForAdminParent(@QueryMap Map<String, String> para);

    @GET("Route")
    Single<BaseResponse<List<RouteResponse>>> getRoute(@QueryMap Map<String, String> para);

    @FormUrlEncoded
    @POST("getDistance")
    Observable<BaseResponse<DistanceRp>> getDistance(@FieldMap Map<String, String> para);

    @FormUrlEncoded
    @POST("InStation")
    Observable<BaseResponse> InStation(@FieldMap Map<String, String> para);

    @FormUrlEncoded
    @POST("OutStation")
    Observable<BaseResponse> OutStation(@FieldMap Map<String, String> para);

    @FormUrlEncoded
    @POST("InStationNotify")
    Observable<BaseResponse> InStationNotify(@FieldMap Map<String, String> para);

    @FormUrlEncoded
    @POST("SetPosition")
    Observable<BaseResponse> SetPosition(@FieldMap Map<String, String> para);

    @FormUrlEncoded
    @POST("ChangeRoute")
    Single<LoginResponse> ChangeRoute(@FieldMap Map<String, String> para);

    @FormUrlEncoded
    @POST("CheckRoute")
    Single<BaseResponse<List<RouteResponse>>> CheckRoute(@FieldMap Map<String, String> para);

    @FormUrlEncoded
    @POST("RouteLocation")
    Single<BaseResponse<StationResponse>> RouteLocation(@FieldMap Map<String, String> para);

    @FormUrlEncoded
    @POST("GetDefaultTrack")
    Single<List<TrackRp>> GetDefaultTrack(@FieldMap Map<String, String> para);

}
