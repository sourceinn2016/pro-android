package sourceinn.buspro.model;

import java.util.List;
import java.util.Map;

import retrofit2.Response;
import rx.Observable;
import rx.Single;
import rx.functions.Func1;
import sourceinn.buspro.reponse.BaseResponse;
import sourceinn.buspro.reponse.DistanceRp;
import sourceinn.buspro.reponse.LoginResponse;
import sourceinn.buspro.reponse.RouteResponse;
import sourceinn.buspro.reponse.StationResponse;
import sourceinn.buspro.reponse.StudentResponse;
import sourceinn.buspro.ui.map.entity.TrackRp;

/**
 * Created by sourceinnW on 2018/4/10.
 */

public class DataManager {
    private RetrofitService mRetrofitService;
    public DataManager(){
        this.mRetrofitService = RetrofitHelper.getInstance().getServer();
    }
    public Single<LoginResponse> login(Map para){
        return mRetrofitService.login(para);
    }

    public Observable<BaseResponse<StudentResponse>> getStationListForStudentDriver(Map para){
        return mRetrofitService.getStationListForStudentDriver(para);
    }
    public Observable<List<StationResponse>> getStationListForAdminParent(Map para){
        return mRetrofitService.getStationListForAdminParent(para);
    }
    public Single<BaseResponse<List<RouteResponse>>> getRoute(Map para){
        return mRetrofitService.getRoute(para);
    }

    public Observable<BaseResponse> InStation(Map para){
        return mRetrofitService.InStation(para);
    }

    public Observable<BaseResponse<DistanceRp>> getDistance(Map para){
        return mRetrofitService.getDistance(para);
    }

    public Observable<BaseResponse> OutStation(Map para){
        return mRetrofitService.OutStation(para);
    }

    public Observable<BaseResponse> InStationNotify(Map para){
        return mRetrofitService.InStationNotify(para);
    }

    public Observable<BaseResponse> SetPosition(Map para){
        return mRetrofitService.SetPosition(para);
    }

    public Single<LoginResponse> changeRoute(Map para){
        return mRetrofitService.ChangeRoute(para);
    }

    public Single<List<RouteResponse>> checkRoute(Map para){
        return mRetrofitService.CheckRoute(para).map(response->((BaseResponse<List<RouteResponse>>)response).getData());
    }

    public Single<StationResponse> RouteLocation(Map para){
        return mRetrofitService.RouteLocation(para).map(response->((BaseResponse<StationResponse>)response).getData());
    }

    public Single<List<TrackRp>> GetDefaultTrack(Map para){
        return mRetrofitService.GetDefaultTrack(para);
    }



}

