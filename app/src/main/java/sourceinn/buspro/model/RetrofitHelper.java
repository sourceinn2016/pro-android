package sourceinn.buspro.model;

import android.util.Log;

import com.google.gson.GsonBuilder;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created by sourceinnW on 2018/4/10.
 */

public class RetrofitHelper {
    OkHttpClient client = new OkHttpClient();
    GsonConverterFactory factory = GsonConverterFactory.create(new GsonBuilder().create());
    private static RetrofitHelper instance = null;
    public static String baseurl = "http://smartbuz.com.tw/ApiV2/";
    private Retrofit mRetrofit = null;
    HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
        @Override
        public void log(String message) {
            Log.i("OKHTTP：", message);
        }
    }).setLevel(HttpLoggingInterceptor.Level.BODY);
    public static RetrofitHelper getInstance(){
        if (instance == null){
            instance = new RetrofitHelper();
        }
        return instance;
    }
    private RetrofitHelper(){
        init();
    }

    private void init() {
        resetApp();
    }

    private void resetApp() {

        client = new OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .addInterceptor(loggingInterceptor)
                .addInterceptor(new Interceptor() {
                    @NotNull
                    @Override
                    public Response intercept(@NotNull Chain chain) throws IOException {
                        Request original = chain.request();
                        HttpUrl originalHttpUrl = original.url();

                        String lang = (Locale.getDefault().getLanguage()+"-"+Locale.getDefault().getCountry()).toLowerCase();
                        if(!lang.equals("zh-tw")){
                            lang = "en-us";
                        }
                        HttpUrl url = originalHttpUrl.newBuilder()
                                .addQueryParameter("lang", lang)
                                .build();

                        // Request customization: add request headers
                        Request.Builder requestBuilder = original.newBuilder()
                                .url(url);


                        Request request = requestBuilder.build();
                        return chain.proceed(request);
                    }
                })
                .build();
        mRetrofit= new Retrofit.Builder()
                .baseUrl(baseurl)
                .client(client)
                .addConverterFactory(factory)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();

    }
    public RetrofitService getServer(){
        return mRetrofit.create(RetrofitService.class);
    }
}
