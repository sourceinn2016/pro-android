package sourceinn.buspro;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by sourceinn on 2015/11/25.
 */
public class DBHelper extends SQLiteOpenHelper {

    public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String create = "CREATE TABLE IF NOT EXISTS login" +
                "( id INTEGER PRIMARY KEY, " +
                " Mobile TEXT," +
                " Password TEXT," +
                " Token TEXT," +
                " Result TEXT," +//Charactor
                " IsConductor INTEGER," +
                " IsRollCall INTEGER)";
        db.execSQL(create);
        create =  "CREATE TABLE IF NOT EXISTS gps" +
                "( id INTEGER PRIMARY KEY, " +
                " Latitude DOUBLE," +
                " Longitude DOUBLE," +
                " isSend INTEGER DEFAULT 0," +
                " CreateTime DATETIME DEFAULT (strftime('%Y-%m-%d %H:%M:%f', 'now', 'localtime')) )";
        db.execSQL(create);

        create =  "CREATE TABLE IF NOT EXISTS signature" +
                "( id INTEGER PRIMARY KEY, " +
                " MemberID TEXT," +
                " CreateTime DATETIME DEFAULT (datetime('now','localtime')))";
        db.execSQL(create);

        create = "CREATE TABLE IF NOT EXISTS MAP" +
                "( " +
                " DATA_KEY TEXT," +
                " DATA_VALUE TEXT" +
                " )";
        db.execSQL(create);

        create =  "CREATE TABLE IF NOT EXISTS FLAG" +
                "( id INTEGER PRIMARY KEY, " +
                " StaionID_Date_AMPM TEXT," +
                " Type INTEGER," +
                " RouteID TEXT," +
                " Account TEXT," +
                " CreateTime DATETIME DEFAULT (strftime('%Y-%m-%d %H:%M:%f', 'now', 'localtime')) )";
        db.execSQL(create);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.v("onUpgrade", "onUpgrade");
        //db.execSQL("DROP TABLE IF EXISTS login");
        db.execSQL("DROP TABLE IF EXISTS FLAG");
        //db.execSQL("DROP TABLE IF EXISTS signature");
        onCreate(db);
    }

    public void truncate(String table){
        SQLiteDatabase db =this.getWritableDatabase();
        db.execSQL("DELETE FROM " + table+"  WHERE 1 = 1");
                db.close();
    }


    public static void insertMap(Context context, String key, String val) {
        DBHelper myDB = new DBHelper(context, context.getResources().getString(R.string.DB_name), null, context.getResources().getInteger(R.integer.DB_version));
        SQLiteDatabase db = myDB.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM MAP WHERE DATA_KEY='"+key+"'", null);
        if (cursor.getCount() > 0) {
            db.execSQL("UPDATE MAP SET DATA_KEY ='"+key+"', DATA_VALUE='"+val+"' WHERE DATA_KEY='"+key+"'");
        }else{
            db.execSQL("INSERT INTO MAP ( DATA_KEY, DATA_VALUE) VALUES ('" + key + "','" + val+ "')");
        }
        db.close();
    }

    public static void removeMapByKey(Context context, String key) {
        DBHelper myDB = new DBHelper(context, context.getResources().getString(R.string.DB_name), null, context.getResources().getInteger(R.integer.DB_version));
        SQLiteDatabase db = myDB.getWritableDatabase();
        db.execSQL("DELETE FROM MAP WHERE DATA_KEY='"+key+"'");
        db.close();
    }

    public static String getMapByKey(Context context, String key) {
        String val = null;
        DBHelper myDB = new DBHelper(context, context.getResources().getString(R.string.DB_name), null, context.getResources().getInteger(R.integer.DB_version));
        SQLiteDatabase db = myDB.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM MAP WHERE DATA_KEY='"+key+"'", null);
        while(cursor.moveToNext()){
            val = cursor.getString(cursor.getColumnIndex("DATA_VALUE"));
        }
        cursor.close();
        db.close();
        return val;
    }
}
