package sourceinn.buspro;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.HashMap;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import sourceinn.buspro.reponse.BaseResponse;
import sourceinn.buspro.reponse.LoginResponse;
import sourceinn.buspro.reponse.RxObject;

/**
 * Created by sourceinn on 2016/9/14.
 */
public class SettingFragment extends BaseFragment implements View.OnClickListener {
    RxBus rxBus = RxBus.getDefault();
    EditText pwd;
    EditText newpwd;
    EditText confirm_newpwd;
    Button sure;
    ImageView back;
    Action1 next = new Action1<BaseResponse<Boolean>>() {

        @Override
        public void call(BaseResponse<Boolean> booleanBaseResponse) {
            Common.hideLoading(getChildFragmentManager(), islive);
            if (booleanBaseResponse.getResult().equals("Success")) {
                Common.showMsgDialog(getChildFragmentManager(), getString(R.string.editSuccess), new MsgDialogFragment.MsgDialogLister() {
                    @Override
                    public void sureEvent() {
                        back.performClick();
                    }
                }, "noCancel");
            } else {
                Common.showMsgDialog(getChildFragmentManager(), getString(R.string.wrongPwd), null, "noCancel");
            }
        }
    };
    Action1 error = new Action1<Throwable>() {
        @Override
        public void call(Throwable e) {
            Common.hideLoading(getChildFragmentManager(), islive);
            e.printStackTrace();
        }
    };

    public static SettingFragment newInstance(){
        SettingFragment fragment = new SettingFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.setting, null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TextView version = (TextView)view.findViewById(R.id.version);
        PackageInfo pInfo = null;
        try {
            pInfo = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String versionName = pInfo.versionName;
        version.setText("Version "+versionName);
        ImageView bg = (ImageView)view.findViewById(R.id.bg);
        bg.setImageBitmap(Common.readBitMap(mContext, R.drawable.list_background, bg.getWidth(), bg.getHeight()));
        back =(ImageView)view.findViewById(R.id.back);
        back.setOnClickListener(this);
        pwd = (EditText)view.findViewById(R.id.pwd);
        newpwd = (EditText)view.findViewById(R.id.newpwd);
        confirm_newpwd = (EditText)view.findViewById(R.id.confirm_newpwd);
        sure = (Button)view.findViewById(R.id.sure);
        sure.setOnClickListener(this);

    }




    private Observable<BaseResponse<Boolean>> change(){
        return Observable.create(new Observable.OnSubscribe<BaseResponse<Boolean>>() {
            @Override
            public void call(Subscriber<? super BaseResponse<Boolean>> subscriber) {
                try {
                    HashMap<String, String> para = new HashMap<String, String>();
                    para.put("Token", URLDecoder.decode(LoginResponse.getLastest(getContext()).getToken()));
                    para.put("Password", pwd.getText().toString());
                    para.put("NewPassword", newpwd.getText().toString());
                    String resp = OkHttpUtil.execute(OkHttpUtil.postRequest(getString(R.string.base_url) + "Password", para)).body().string();
                    Log.v("QQQ", resp);
                    Gson gson = new Gson();
                    BaseResponse<Boolean> cc = gson.fromJson(resp, new TypeToken<BaseResponse<Boolean>>() {}.getType());
                    if(cc.getMessage().equals("Token 失效！")){
                        RxObject rxObject = new RxObject();
                        rxObject.setTag("Token失效");
                        rxBus.post(rxObject);
                    }else {
                        subscriber.onNext(cc);
                    }
                } catch (IOException e) {
                    subscriber.onError(e);
                }
                subscriber.onCompleted();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.back:
                RxObject rxObject = new RxObject();
                rxObject.setTag("back");
                rxBus.post(rxObject);
                break;
            case R.id.sure:
                if(pwd.getText().length()>0 && confirm_newpwd.getText().length()>0 && newpwd.getText().length()>0) {
                    if(confirm_newpwd.getText().toString().equals( newpwd.getText().toString())) {
                        Common.showLoading(getChildFragmentManager(), islive);
                        change().subscribeOn(Schedulers.newThread())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(next, error, new Action0() {
                                    @Override
                                    public void call() {
                                        Common.hideLoading(getChildFragmentManager(), islive);
                                    }
                                });
                    }else{
                        Toast.makeText(mContext, getString(R.string.newPwdNotSame), Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(mContext, getString(R.string.pwdNoEmpty), Toast.LENGTH_SHORT).show();
                }
                break;

        }
    }
}
