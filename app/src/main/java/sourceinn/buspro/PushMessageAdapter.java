package sourceinn.buspro;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import sourceinn.buspro.reponse.LoginResponse;
import sourceinn.buspro.reponse.PushMessageResponse;
import sourceinn.buspro.util.DateTimeUtil;


public class PushMessageAdapter extends RecyclerView.Adapter {
    RxBus rxBus = RxBus.getDefault();
    protected List<PushMessageResponse> mDatas;
    protected Context mContext;
    OnItemClickListener listener;
    public interface  OnItemClickListener{
        void onDelete(PushMessageResponse data);
    }


    public PushMessageAdapter(OnItemClickListener listener, List<PushMessageResponse> mDatas) {
        mContext = ((Fragment) listener).getContext();
        this.listener = listener;
        this.mDatas = mDatas;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.pushmessage_item, parent, false);
        return new MyViewHolder(view);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        ((MyViewHolder) holder).Message.setText(mDatas.get(position).getMessage());
        long utcTimeMillis = Long.parseLong(mDatas.get(position).getCreateTime().split("\\(" )[1].split("\\)")[0]);
        ((MyViewHolder) holder).CreateTime.setText(
                //DateTimeUtil.localToUTC( DateTimeUtil.getDateTime(utcTimeMillis), "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm:ss")
                DateTimeUtil.getDateTime(utcTimeMillis)
                );

        ((MyViewHolder)holder).delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onDelete(mDatas.get(position));
            }
        });

        if(position==0){
            ((MyViewHolder)holder).line.setVisibility(View.GONE);
        }else{
            ((MyViewHolder)holder).line.setVisibility(View.VISIBLE);
        }
    }




    @Override
    public int getItemCount()
    {
        return mDatas==null?0:mDatas.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView CreateTime, Message;
        Button delete;
        View line;


        public MyViewHolder(View view)
        {
            super(view);
            CreateTime = (TextView)view.findViewById(R.id.CreateTime);
            Message = (TextView)view.findViewById(R.id.Message);
            delete = (Button)view.findViewById(R.id.delete);
            line = view.findViewById(R.id.line);
        }
    }

}
