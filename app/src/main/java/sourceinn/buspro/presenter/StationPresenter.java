package sourceinn.buspro.presenter;

import android.Manifest;
import android.app.ActivityManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.nfc.NfcAdapter;
import android.os.Build;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Single;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import sourceinn.buspro.Common;
import sourceinn.buspro.DBHelper;
import sourceinn.buspro.DiscloseureDia;
import sourceinn.buspro.MapKey;
import sourceinn.buspro.MsgDialogFragment;
import sourceinn.buspro.MyService;
import sourceinn.buspro.R;
import sourceinn.buspro.RxBus;
import sourceinn.buspro.StationFragment;
import sourceinn.buspro.TTS;
import sourceinn.buspro.model.DataManager;
import sourceinn.buspro.reponse.BaseResponse;
import sourceinn.buspro.reponse.DistanceRp;
import sourceinn.buspro.reponse.FlagData;
import sourceinn.buspro.reponse.GpsData;
import sourceinn.buspro.reponse.LoginResponse;
import sourceinn.buspro.reponse.RouteResponse;
import sourceinn.buspro.reponse.RxObject;
import sourceinn.buspro.reponse.StationResponse;
import sourceinn.buspro.reponse.StudentResponse;
import sourceinn.buspro.util.DateTimeUtil;
import sourceinn.buspro.util.GpsUtil;

import static android.content.Context.ACTIVITY_SERVICE;

public class StationPresenter {
    DataManager dataManager;
    int checkInteval = 10, stationRefreshInteval = 10;
    Subscription distanceSub,
            stationSub, stationTimerSub;
    CB viewRefer;
    boolean gpsable;
    StudentResponse stationInfo;
    List<StationResponse> staionList = new ArrayList<>();
    DistanceRp distanceRp ;
    RouteResponse routeResponse;//parent, admin use
    RxBus rxBus = RxBus.getDefault();
    NfcAdapter mNfcAdapter;
    MyService myService;
    String routeName = null, route_id=null;
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            myService = ((MyService.MyBinder)service).getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            myService = null;
        }
    };
    public interface CB{
        void dataRefreshDone();
        void showLoading();
        void hideLoading();
        void setNfcText(String text);
        void setDiscloseureVisable(boolean flag);
        void setRouteName(String routeName);
    }

    public StationPresenter(CB viewRefer) {
        dataManager = new DataManager();
        this.viewRefer = viewRefer;
        gpsable = LoginResponse.getLastest(((Fragment)viewRefer).getContext()).isConductor();
        routeResponse = (RouteResponse) ((StationFragment)viewRefer).getRouteRp();
        if(routeResponse!=null){
            routeName = routeResponse.getName();
            viewRefer.setRouteName(routeName);
        }
    }

    public List<StationResponse> getStations(){
        return staionList;
    }

    public void requestData(){
        Observable ob;
        if(routeResponse!=null){
            HashMap para = new HashMap();
            para.put("RouteID", routeResponse.getID());
            ob = dataManager.getStationListForAdminParent(para);
        }else{
            HashMap para = new HashMap();
            para.put("Token", LoginResponse.getLastest(((Fragment)viewRefer).getContext()).getToken());
            ob = dataManager.getStationListForStudentDriver(para);
        }
        if(stationSub!=null && !stationSub.isUnsubscribed()){
            stationSub.unsubscribe();
        }
        viewRefer.showLoading();
        stationSub = ob.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1() {
                    @Override
                    public void call(Object o) {
                        //B
                        staionList.clear();
                        if (o instanceof BaseResponse) {
                            BaseResponse baseResponse = (BaseResponse) o;
                            stationInfo = (StudentResponse) baseResponse.getData();
                            staionList.addAll(stationInfo.getList());
                        } else {
                            Gson gson = new Gson();
                            Type listType = new TypeToken<List<StationResponse>>() {
                            }.getType();
                            //Log.v("QQQ", String.valueOf(o));
                            //List<StationResponse> tmpl = gson.fromJson(String.valueOf(o), listType);
                            staionList.addAll((List<StationResponse>)o);
                        }
                        //離站時間顯示
                        if(distanceRp!=null && distanceRp.isDeparture()){
                            for (int i=0; i<staionList.size(); i++){
                                staionList.get(i).setShowOutTime(distanceRp.isDeparture());
                                //最後一暫不要顯示離站
                                if(i==staionList.size()-1){
                                    staionList.get(i).setShowOutTime(false);
                                }
                            }
                        }else{
                            for (int i=0; i<staionList.size(); i++){
                                staionList.get(i).setShowOutTime(false);
                            }
                        }
                        viewRefer.dataRefreshDone();
                        viewRefer.hideLoading();

                        if(routeName==null && stationInfo!=null){
                            getRouteName(stationInfo.getRouteID());
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                    }
                });
    }

    private void getRouteName(String id){
        HashMap<String, String> para = new HashMap<String, String>();
        para.put("Token", LoginResponse.getLastest(((Fragment)viewRefer).getContext()).getToken());
        dataManager.getRoute(para)
                .map(response->response.getData())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(list->{
                    for (RouteResponse route : list){
                        if(route.getID().equals(id)){
                            routeName = route.getName() ;
                            viewRefer.setRouteName(routeName);
                            break;
                        }
                    }
                });
    }

    public Single<String> getRouteId(Context context){
        if(routeResponse!=null ){
            return Single.just( routeResponse.getID());
        }else if(route_id!=null){
            return Single.just( route_id);
        }else{
            HashMap para = new HashMap();
            para.put("Token", LoginResponse.getLastest(context).getToken());
            return dataManager.getStationListForStudentDriver(para)
                    .toSingle()
                    .map(stationResponse->{
                        route_id = stationResponse.getData().getRouteID();
                return stationResponse.getData().getRouteID();
            });
        }
    }

    public Single<StationResponse> getLocation(Context context, String route_id){
        HashMap<String, String> para = new HashMap<String, String>();
        para.put("RouteID", route_id);
        para.put("Token", LoginResponse.getLastest(context).getToken());
        return dataManager.RouteLocation(para);
    }

    public void onCreate(){
        mNfcAdapter = NfcAdapter.getDefaultAdapter(((StationFragment)viewRefer).getContext());
        if(gpsable) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ContextCompat.checkSelfPermission(((Fragment)viewRefer).getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                        ContextCompat.checkSelfPermission(((Fragment)viewRefer).getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    DiscloseureDia.showMsgDialog(((Fragment)viewRefer).getChildFragmentManager(), new DiscloseureDia.CB() {
                        @Override
                        public void onPermissionGet() {
                            ((Fragment) viewRefer).getActivity().startService(new Intent(((Fragment) viewRefer).getActivity(), MyService.class));
                        }
                    });
                }else{
                    if(DBHelper.getMapByKey(((Fragment)viewRefer).getContext(), MapKey.HAS_MAIN_DISCOSURE)!=null) {
                        viewRefer.setDiscloseureVisable(true);
                        DBHelper.insertMap(((Fragment)viewRefer).getContext(), MapKey.HAS_MAIN_DISCOSURE, MapKey.HAS_MAIN_DISCOSURE);
                    }
                    ((Fragment) viewRefer).getActivity().startService(new Intent(((Fragment) viewRefer).getActivity(), MyService.class));
                }
            }else{
                if(DBHelper.getMapByKey(((Fragment)viewRefer).getContext(), MapKey.HAS_MAIN_DISCOSURE)!=null) {
                    viewRefer.setDiscloseureVisable(true);
                    DBHelper.insertMap(((Fragment)viewRefer).getContext(), MapKey.HAS_MAIN_DISCOSURE, MapKey.HAS_MAIN_DISCOSURE);
                }
                ((Fragment) viewRefer).getActivity().startService(new Intent(((Fragment) viewRefer).getActivity(), MyService.class));
            }

        }
    }


    public void onResume(){
        //Log.v("QQQ", isForeground(MainActivity.class.getCanonicalName())+"");



        ((StationFragment)viewRefer).getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        HashMap para = new HashMap();
        para.put("Token", LoginResponse.getLastest(((Fragment)viewRefer).getContext()).getToken());
        if(distanceSub!=null && !distanceSub.isUnsubscribed()){
            distanceSub.unsubscribe();
        }
        distanceSub = dataManager.getDistance(para)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<BaseResponse<DistanceRp>>() {
                    @Override
                    public void call(BaseResponse<DistanceRp> rp) {
                        distanceRp = rp.getData();
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                    }
                });

        //NFC TEXT
        if (mNfcAdapter == null) {
            viewRefer.setNfcText(((StationFragment)viewRefer).getString(R.string.noSupport));
        }else if (!mNfcAdapter.isEnabled()) {
            viewRefer.setNfcText(((StationFragment)viewRefer).getString(R.string.plsOpenNFC));
        }else {
            viewRefer.setNfcText(((StationFragment)viewRefer).getString(R.string.rollcall));
        }


        //刷新葉面
        if(stationTimerSub!=null && !stationTimerSub.isUnsubscribed()){
            stationTimerSub.unsubscribe();
        }
        stationTimerSub = Observable.interval(0, stationRefreshInteval, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Long>() {
                    @Override
                    public void call(Long aLong) {
                        requestData();
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {

                    }
                });
    }

    public void onPause(){
        if(distanceSub!=null && !distanceSub.isUnsubscribed()){
            distanceSub.unsubscribe();
        }
        if(stationTimerSub!=null && !stationTimerSub.isUnsubscribed()){
            stationTimerSub.unsubscribe();
        }
        if(stationSub!=null && !stationSub.isUnsubscribed()){
            stationSub.unsubscribe();
        }
    }

    public void onStart(){
        //距離參數
        if(gpsable) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ContextCompat.checkSelfPermission(((Fragment)viewRefer).getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                        ContextCompat.checkSelfPermission(((Fragment)viewRefer).getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    DiscloseureDia.showMsgDialog(((Fragment)viewRefer).getChildFragmentManager(), new DiscloseureDia.CB() {
                        @Override
                        public void onPermissionGet() {
                            viewRefer.setDiscloseureVisable(true);
                            ((Fragment) viewRefer).getActivity().startService(new Intent(((Fragment) viewRefer).getActivity(), MyService.class));
                            bindService();
                        }

                    });
                }else{
                    if(DBHelper.getMapByKey(((Fragment)viewRefer).getContext(), MapKey.HAS_MAIN_DISCOSURE)!=null) {
                        viewRefer.setDiscloseureVisable(true);
                        DBHelper.insertMap(((Fragment)viewRefer).getContext(), MapKey.HAS_MAIN_DISCOSURE, MapKey.HAS_MAIN_DISCOSURE);
                    }
                    bindService();
                }
            }else{
                if(DBHelper.getMapByKey(((Fragment)viewRefer).getContext(), MapKey.HAS_MAIN_DISCOSURE)!=null) {
                    viewRefer.setDiscloseureVisable(true);
                    DBHelper.insertMap(((Fragment)viewRefer).getContext(), MapKey.HAS_MAIN_DISCOSURE, MapKey.HAS_MAIN_DISCOSURE);
                }
                bindService();
            }
        }

    }

    private void bindService(){
        //
        Log.v("QQQ", "bindService");
        ((Fragment)viewRefer).getActivity()
                .bindService(new Intent(((Fragment)viewRefer).getActivity(), MyService.class),
                        serviceConnection,
                        Context.BIND_AUTO_CREATE);
    }

    public void onStop(){
        if(gpsable && myService!=null) {
            Log.v("QQQ", "unbindService");
            ((Fragment) viewRefer).getActivity()
                    .unbindService(serviceConnection);
        }
    }

    public void onLogout(){
        if(gpsable && myService!=null) {
            ((Fragment)viewRefer).getActivity().stopService(new Intent(((Fragment)viewRefer).getActivity(), MyService.class));
        }
        RxObject rxObject = new RxObject();
        rxObject.setTag("logout");
        rxBus.post(rxObject);

    }

    public void onDestroy(){
//        if(gpsable && myService!=null) {
//            ((Fragment)viewRefer).getActivity().stopService(new Intent(((Fragment)viewRefer).getActivity(), MyService.class));
//        }
    }
}
