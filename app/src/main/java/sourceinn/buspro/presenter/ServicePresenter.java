package sourceinn.buspro.presenter;

import android.Manifest;
import android.app.ActivityManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.nfc.NfcAdapter;
import android.os.Build;
import android.util.Log;
import android.view.WindowManager;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import sourceinn.buspro.Common;
import sourceinn.buspro.DBHelper;
import sourceinn.buspro.DiscloseureDia;
import sourceinn.buspro.MapKey;
import sourceinn.buspro.MyService;
import sourceinn.buspro.R;
import sourceinn.buspro.RxBus;
import sourceinn.buspro.StationFragment;
import sourceinn.buspro.model.DataManager;
import sourceinn.buspro.reponse.BaseResponse;
import sourceinn.buspro.reponse.DistanceRp;
import sourceinn.buspro.reponse.FlagData;
import sourceinn.buspro.reponse.GpsData;
import sourceinn.buspro.reponse.LoginResponse;
import sourceinn.buspro.reponse.RouteResponse;
import sourceinn.buspro.reponse.StationResponse;
import sourceinn.buspro.reponse.StudentResponse;
import sourceinn.buspro.util.DateTimeUtil;
import sourceinn.buspro.util.GpsGoogleUtil;
import sourceinn.buspro.util.GpsUtil;

import static android.content.Context.ACTIVITY_SERVICE;

public class ServicePresenter {
    DataManager dataManager;
    public GpsGoogleUtil gpsUtil;
    int checkInteval = 10, stationRefreshInteval = 10;
    Subscription distanceSub, inTimerSub, outSub, notifySub, inSub,
            stationSub, stationTimerSub, sendGpsSub;
    public boolean hasFinalStopTime;
    StudentResponse stationInfo;
    DistanceRp distanceRp ;
    MyService service;

    public ServicePresenter(MyService service) {
        dataManager = new DataManager();
        this.service = service;
        //gpsUtil = new GpsUtil(service);
        gpsUtil = new GpsGoogleUtil(service);
    }


    public void requestStationData(){
        HashMap para = new HashMap();
        para.put("Token", LoginResponse.getLastest(service).getToken());
        if(stationSub!=null && !stationSub.isUnsubscribed()){
            stationSub.unsubscribe();
        }
        stationSub = dataManager.getStationListForStudentDriver(para)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<BaseResponse<StudentResponse>>() {
                    @Override
                    public void call(BaseResponse<StudentResponse> o) {
                        stationInfo = (StudentResponse) o.getData();
                        if(stationInfo.getList().size()>0 && !stationInfo.getList().get(stationInfo.getList().size()-1).getStopTime().equals("--")){
                            hasFinalStopTime = true;
                        }else{
                            hasFinalStopTime = false;
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                    }
                });
    }

    public void onCreate(){
        HashMap para = new HashMap();
        para.put("Token", LoginResponse.getLastest(service).getToken());
        if(distanceSub!=null && !distanceSub.isUnsubscribed()){
            distanceSub.unsubscribe();
        }
        distanceSub = dataManager.getDistance(para)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<BaseResponse<DistanceRp>>() {
                    @Override
                    public void call(BaseResponse<DistanceRp> rp) {
                        distanceRp = rp.getData();
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                    }
                });

        gpsUtil.startLocationUpdates();

        //距離判斷
        if(inTimerSub!=null && !inTimerSub.isUnsubscribed()){
            inTimerSub.unsubscribe();
        }
        inTimerSub = Observable.interval(0, checkInteval, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Long>() {
                    @Override
                    public void call(Long aLong) {
                        inoutCheck();

                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        Log.v("QQQ", "inoutCheck error "+throwable.getLocalizedMessage());
                        throwable.printStackTrace();
                    }
                });

        //刷新葉面
        if(stationTimerSub!=null && !stationTimerSub.isUnsubscribed()){
            stationTimerSub.unsubscribe();
        }
        stationTimerSub = Observable.interval(0, stationRefreshInteval, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Long>() {
                    @Override
                    public void call(Long aLong) {
                        requestStationData();
                        //save gps
                        if (!hasFinalStopTime && gpsUtil.getCurrentLocation() != null
                                && betweenTime(Calendar.getInstance().getTime(), stationInfo)) {
                            GpsData gpsData = new GpsData();
                            gpsData.setLatitude(gpsUtil.getCurrentLocation().getLatitude());
                            gpsData.setLongitude(gpsUtil.getCurrentLocation().getLongitude());
                            Common.saveGPS(service, gpsData);
                        }
                        //send gps
                        sendGPS();

                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {

                    }
                });
    }

    public void onStartCommand(){

    }

    public void onDestroy(){
        gpsUtil.stopLocationUpdate();
        if(distanceSub!=null && !distanceSub.isUnsubscribed()){
            distanceSub.unsubscribe();
        }
        if(inTimerSub!=null && !inTimerSub.isUnsubscribed()){
            inTimerSub.unsubscribe();
        }
        if(stationTimerSub!=null && !stationTimerSub.isUnsubscribed()){
            stationTimerSub.unsubscribe();
        }
        if(stationSub!=null && !stationSub.isUnsubscribed()){
            stationSub.unsubscribe();
        }
        if (sendGpsSub != null && !sendGpsSub.isUnsubscribed()) {
            sendGpsSub.unsubscribe();
        }
    }

    public boolean isInWorkTime(){
        return stationInfo!=null &&
                stationInfo.getList().size()>0 &&
                betweenTime(Calendar.getInstance().getTime(), stationInfo)
                ;
    }



    private void inoutCheck(){
        if(gpsUtil.getCurrentLocation()!=null &&
                isInWorkTime() &&
                distanceRp!=null
        ){
            String keyString = "";
            String ampm = "";
            if(Integer.parseInt(DateTimeUtil.getTimeString(System.currentTimeMillis(), "HH")) >11){
                ampm = "PM";
            }else{
                ampm = "AM";
            }
            for (int i=0; i<stationInfo.getList().size(); i++){
                if(keyString.length()>0){
                    keyString+=",";
                }
                keyString += "'"+stationInfo.getList().get(i).getID()+"_"+ DateTimeUtil.getTimeString(System.currentTimeMillis(), "yyyy-MM-dd")
                +"_"+ampm+"'";
            }
            //進站偵測
            List<FlagData> hasNotifyList = Common.getFlags(service, keyString, FlagData.NOTIFY, stationInfo.getRouteID(), LoginResponse.getLastest(service).getMobile());
            List<FlagData> hasInList = Common.getFlags(service, keyString, FlagData.IN, stationInfo.getRouteID(), LoginResponse.getLastest(service).getMobile());
            List<FlagData> hasOutList = Common.getFlags(service, keyString, FlagData.OUT, stationInfo.getRouteID(), LoginResponse.getLastest(service).getMobile());
            List<StationResponse> inUnRecord = new ArrayList<>();
            List<StationResponse> outUnRecord = new ArrayList<>();
            List<StationResponse> notifyUnRecord = new ArrayList<>();
            StationResponse inClosestStation = null;
            StationResponse outClosestStation = null;
            StationResponse notifyClosestStation = null;
            for (int i=0; i<stationInfo.getList().size(); i++){
                String tmpPairKey = stationInfo.getList().get(i).getID()+"_"+ DateTimeUtil.getTimeString(System.currentTimeMillis(), "yyyy-MM-dd")+"_"+ampm;
                boolean inNeedCal = true;
                boolean outNeedCal = true;
                boolean notifyNeedCal = true;
                for (int j=0; j<hasInList.size(); j++){
                    if(hasInList.get(j).getStaionID_Date_AMPM().equals(tmpPairKey)){
                        inNeedCal = false;
                        break;
                    }
                }
                for (int j=0; j<hasOutList.size(); j++){
                    if(hasOutList.get(j).getStaionID_Date_AMPM().equals(tmpPairKey)){
                        outNeedCal = false;
                        break;
                    }
                }
                for (int j=0; j<hasNotifyList.size(); j++){
                    if(hasNotifyList.get(j).getStaionID_Date_AMPM().equals(tmpPairKey)){
                        notifyNeedCal = false;
                        break;
                    }
                }
                if(inNeedCal){
                    inUnRecord.add(stationInfo.getList().get(i));
                }
                if(outNeedCal){
                    if(!inNeedCal){//必須有進站過  才有出站意義
                        outUnRecord.add(stationInfo.getList().get(i));
                    }
                }
                if(notifyNeedCal){
                    notifyUnRecord.add(stationInfo.getList().get(i));
                }
            }
            //進站判定
            float distance = -1;
            for (int i=0; i<inUnRecord.size(); i++){
                float[] result = new float[1];
                Location.distanceBetween(gpsUtil.getCurrentLocation().getLatitude(), gpsUtil.getCurrentLocation().getLongitude(),
                        Double.parseDouble(inUnRecord.get(i).getLatitude()), Double.parseDouble(inUnRecord.get(i).getLongitude()),
                        result);
                if(i==0){
                    distance = result[0];
                    inClosestStation = inUnRecord.get(i);
                }else{
                    if(distance > result[0]){
                        distance = result[0];
                        inClosestStation = inUnRecord.get(i);
                    }
                }
            }
//Log.v("QQQ", inClosestStation.getName()+" distance = "+distance+" < "+distanceRp.getIn());
            if(distance!=-1 &&inClosestStation!=null && distance<distanceRp.getIn()){
                FlagData flagData = new FlagData();
                flagData.setType(FlagData.IN);
                flagData.setRouteID(stationInfo.getRouteID());
                flagData.setAccount(LoginResponse.getLastest(service).getMobile());
                flagData.setStaionID_Date_AMPM(inClosestStation.getID()+"_"+ DateTimeUtil.getTimeString(System.currentTimeMillis(), "yyyy-MM-dd")+"_"+ampm);
                Common.insertFlag(service, flagData);
                //cal in api
                HashMap para = new HashMap();
                para.put("Token", LoginResponse.getLastest(service).getToken());
                para.put("StationID", inClosestStation.getID());
                para.put("dateTime", DateTimeUtil.getTimeString(System.currentTimeMillis(), "yyyy-MM-dd HH:mm:ss"));
                //Log.v("QQQ", "call InStation"+para.toString()+" "+inClosestStation.getName()+" "+distance);
                if(inSub!=null && !inSub.isUnsubscribed()){
                    inSub.unsubscribe();
                }
                inSub = dataManager.InStation(para)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Action1<BaseResponse>() {
                            @Override
                            public void call(BaseResponse baseResponse) {

                            }
                        }, new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                Log.v("QQQ", "InStation error"+throwable.getLocalizedMessage());
                                throwable.printStackTrace();
                            }
                        });
            }

            //通知判定
            distance = -1;
            for (int i=0; i<notifyUnRecord.size(); i++){
                float[] result = new float[1];
                Location.distanceBetween(gpsUtil.getCurrentLocation().getLatitude(), gpsUtil.getCurrentLocation().getLongitude(),
                        Double.parseDouble(notifyUnRecord.get(i).getLatitude()), Double.parseDouble(notifyUnRecord.get(i).getLongitude()),
                        result);

                if(i==0){
                    distance = result[0];
                    notifyClosestStation = notifyUnRecord.get(i);
                }else{
                    if(distance > result[0]){
                        distance = result[0];
                        notifyClosestStation = notifyUnRecord.get(i);
                    }
                }
            }
            if(distance!=-1 && notifyClosestStation!=null && distance<distanceRp.getNotify()){
                FlagData flagData = new FlagData();
                flagData.setType(FlagData.NOTIFY);
                flagData.setRouteID(stationInfo.getRouteID());
                flagData.setAccount(LoginResponse.getLastest(service).getMobile());
                flagData.setStaionID_Date_AMPM(notifyClosestStation.getID()+"_"+ DateTimeUtil.getTimeString(System.currentTimeMillis(), "yyyy-MM-dd")+"_"+ampm);
                Common.insertFlag(service, flagData);
                //Log.v("QQQ", "insert "+flagData.getStaionID_Date_AMPM()+" type="+flagData.getType());
                //cal notify api
                if(notifySub!=null && !notifySub.isUnsubscribed()){
                    notifySub.unsubscribe();
                }
                HashMap<String, String> para = new HashMap<String, String>();
                para.put("Token", LoginResponse.getLastest(service).getToken());
                para.put("StationID", notifyClosestStation.getID());
                para.put("dateTime", DateTimeUtil.getTimeString(System.currentTimeMillis(), "yyyy-MM-dd HH:mm:ss"));
                //Log.v("QQQ", "InStationNotify "+para.toString());
                notifySub = dataManager.InStationNotify(para)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Action1<BaseResponse>() {
                            @Override
                            public void call(BaseResponse baseResponse) {

                            }
                        }, new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                Log.v("QQQ", "InStationNotify error"+throwable.getLocalizedMessage());
                                throwable.printStackTrace();
                            }
                        });
            }

            //出站判定
            distance = -1;
            for (int i=0; i<outUnRecord.size(); i++){
                float[] result = new float[1];
                Location.distanceBetween(gpsUtil.getCurrentLocation().getLatitude(), gpsUtil.getCurrentLocation().getLongitude(),
                        Double.parseDouble(outUnRecord.get(i).getLatitude()), Double.parseDouble(outUnRecord.get(i).getLongitude()),
                        result);

                if(i==0){
                    distance = result[0];
                    outClosestStation = outUnRecord.get(i);
                }else{
                    if(distance > result[0]){
                        distance = result[0];
                        outClosestStation = outUnRecord.get(i);
                    }
                }
            }
            if(distance!=-1 && outClosestStation!=null && distance>distanceRp.getOut()){
                FlagData flagData = new FlagData();
                flagData.setType(FlagData.OUT);
                flagData.setRouteID(stationInfo.getRouteID());
                flagData.setAccount(LoginResponse.getLastest(service).getMobile());
                flagData.setStaionID_Date_AMPM(outClosestStation.getID()+"_"+ DateTimeUtil.getTimeString(System.currentTimeMillis(), "yyyy-MM-dd")+"_"+ampm);
                Common.insertFlag(service, flagData);
                if(distanceRp.isDeparture()){//cal out api
                    if(outSub!=null && !outSub.isUnsubscribed()){
                        outSub.unsubscribe();
                    }
                    HashMap<String, String> para = new HashMap<String, String>();
                    para.put("Token", LoginResponse.getLastest(service).getToken());
                    para.put("StationID", outClosestStation.getID());
                    para.put("dateTime", DateTimeUtil.getTimeString(System.currentTimeMillis(), "yyyy-MM-dd HH:mm:ss"));
                    //Log.v("QQQ", "OutStation"+para.toString()+" "+outClosestStation.getName()+" "+distance);
                    outSub = dataManager.OutStation(para)
                            .subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Action1<BaseResponse>() {
                                @Override
                                public void call(BaseResponse baseResponse) {

                                }
                            }, new Action1<Throwable>() {
                                @Override
                                public void call(Throwable throwable) {
                                    Log.v("QQQ", "OutStation error"+throwable.getLocalizedMessage());
                                    throwable.printStackTrace();
                                }
                            });
                }
            }


        }
    }

    private void sendGPS() {
        if(stationInfo==null){
            return;
        }
        try {
            final SimpleDateFormat strTodate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
            List<GpsData> list = Common.getGPS(service);
            HashMap<String, String> para;
            for (int i = 0; i < list.size(); i++) {

                if (betweenTime(strTodate.parse(list.get(i).getCreatetime()), stationInfo)) {//排除昨天沒送完的
                    para = new HashMap<>();

                    para.put("Latitude", String.valueOf(list.get(i).getLatitude()));
                    para.put("Longitude", String.valueOf(list.get(i).getLongitude()));
                    para.put("CreateTime", list.get(i).getCreatetime());
                    //Log.v("QQQ", "SetPosition "+para.toString());
                    para.put("Token", URLDecoder.decode(LoginResponse.getLastest(service).getToken()));
                    dataManager.SetPosition(para)
                            .subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Action1<BaseResponse>() {
                                @Override
                                public void call(BaseResponse baseResponse) {

                                }
                            }, new Action1<Throwable>() {
                                @Override
                                public void call(Throwable throwable) {
                                    Log.v("QQQ", "SetPosition "+throwable.getMessage());
                                    throwable.printStackTrace();
                                }
                            });
                }
                Common.removeGPS(service, list.get(i).getId());
            }
        }catch (Exception e){
            Log.v("QQQ", "QQQQQ "+e.getLocalizedMessage());
            e.printStackTrace();
        }
    }

    private boolean betweenTime(Date date, StudentResponse stationInfo) {
        if(stationInfo==null){
            return false;
        }
        Date today = Calendar.getInstance().getTime();
        SimpleDateFormat date_fo = new SimpleDateFormat("yyyy-MM-dd");
        if (!date_fo.format(today).equals(date_fo.format(date))) {
            return false;
        }


        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
        String now = formatter.format(date);
        int nowHour = Integer.parseInt(now.substring(0, 2));
        int nowMin = Integer.parseInt(now.substring(3, 5));
        int targetStartHour;
        int targetStartMin;
        int targetStopHour;
        int targetStopMin;
        if (nowHour < 12) {//上午
            if (stationInfo.getStartLogTimeAM().equals("")) {
                stationInfo.setStartLogTimeAM("04:00:00");
            }
            if (stationInfo.getStopLogTimeAM().equals("")) {
                stationInfo.setStopLogTimeAM("11:59:00");
            }
            targetStartHour = Integer.parseInt(stationInfo.getStartLogTimeAM().substring(0, 2));
            targetStartMin = Integer.parseInt(stationInfo.getStartLogTimeAM().substring(3, 5));
            targetStopHour = Integer.parseInt(stationInfo.getStopLogTimeAM().substring(0, 2));
            targetStopMin = Integer.parseInt(stationInfo.getStopLogTimeAM().substring(3, 5));
        } else {//下午
            if (stationInfo.getStartLogTimePM().equals("")) {
                stationInfo.setStartLogTimePM("12:00:00");
            }
            if (stationInfo.getStopLogTimePM().equals("")) {
                stationInfo.setStopLogTimePM("21:00:00");
            }
            targetStartHour = Integer.parseInt(stationInfo.getStartLogTimePM().substring(0, 2));
            targetStartMin = Integer.parseInt(stationInfo.getStartLogTimePM().substring(3, 5));
            targetStopHour = Integer.parseInt(stationInfo.getStopLogTimePM().substring(0, 2));
            targetStopMin = Integer.parseInt(stationInfo.getStopLogTimePM().substring(3, 5));
        }
        if ((nowHour * 60 + nowMin) >= (targetStartHour * 60 + targetStartMin) && (nowHour * 60 + nowMin) <= (targetStopHour * 60 + targetStopMin)) {
            //Log.v("QQQ", "true");
            return true;
        } else {
            //Log.v("QQQ", "false");
            return false;
        }

    }
}
