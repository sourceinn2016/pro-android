package sourceinn.buspro;

import android.annotation.SuppressLint;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.wifi.WifiManager;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.os.PowerManager;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.subscriptions.CompositeSubscription;
import sourceinn.buspro.manager.NotifyManager;
import sourceinn.buspro.presenter.ServicePresenter;
import sourceinn.buspro.reponse.LoginResponse;
import sourceinn.buspro.service.entity.GpsLocationData;
import sourceinn.buspro.ui.login.view.LoginActivity;

public class MyService extends Service {
    public static final String EXTRA_CANCEL_LOCATION_TRACKING_FROM_NOTIFICATION = "EXTRA_CANCEL_LOCATION_TRACKING_FROM_NOTIFICATION";
    ServicePresenter presenter;
    MyBinder binder;

    MediaPlayer ring;
    PowerManager.WakeLock wakeLock;
    WifiManager.WifiLock mWifiLock;
    private WifiManager mWifiManager;
    CompositeSubscription compositeSubscription;
    NotifyManager notifyManager;

    GpsLocationData gpsLocationData;


    @SuppressLint("InvalidWakeLockTag")
    @Override
    public void onCreate() {
        super.onCreate();
        presenter = new ServicePresenter(this);
        presenter.onCreate();
        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MyWakelockTag");
        wakeLock.acquire();
        mWifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        mWifiLock = mWifiManager.createWifiLock(WifiManager.WIFI_MODE_FULL_HIGH_PERF, "WifiLocKManager");
        mWifiLock.acquire();
        ring = MediaPlayer.create(this, R.raw.blank);
        ring.start();
        ring.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                ring.start();
            }
        });
        notifyManager = new NotifyManager();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.v("QQQ", "onBind");
        stopForeground(true);
        binder= new MyBinder();
        return binder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        startForeground(NotifyManager.foregoundId,
                notifyManager.createForgroundMsg(getApplicationContext(),
                        "將在背景存取您的位置訊息，用於計算進出站演算").build());
        return true;
    }

    @Override
    public void onRebind(Intent intent) {
        stopForeground(true);
        super.onRebind(intent);
    }

    @Override
    public void onDestroy() {
        Log.v("QQQ", "service onDestroy");
        //stopForeground(true);
        presenter.onDestroy();
        wakeLock.release();
        ring.stop();
        ring.release();
        if (mWifiLock.isHeld()) {
            mWifiLock.release();
        }
        if(compositeSubscription!=null && !compositeSubscription.isUnsubscribed()){
            compositeSubscription.unsubscribe();
        }
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.v("QQQ", "onStartCommand");
        if(intent.getBooleanExtra(EXTRA_CANCEL_LOCATION_TRACKING_FROM_NOTIFICATION, false)){
            stopSelf();
        }


        presenter.onStartCommand();
        if(compositeSubscription!=null && !compositeSubscription.isUnsubscribed()){
            compositeSubscription.unsubscribe();
        }
        gpsLocationData = new GpsLocationData();
        compositeSubscription = new CompositeSubscription();
        compositeSubscription.add(
                //time count
                Observable.interval(1, TimeUnit.SECONDS)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(l->{
                            if(
                                    presenter.isInWorkTime() &&
                                    !presenter.hasFinalStopTime
                            ){
                                if(gpsLocationData.getLocation()==null){
                                    gpsLocationData.setLocation(presenter.gpsUtil.getCurrentLocation());
                                    gpsLocationData.setTimeInMillis(System.currentTimeMillis());
                                }else{
                                    if(gpsLocationData.getLocation().getLatitude()==presenter.gpsUtil.getCurrentLocation().getLatitude() &&
                                            gpsLocationData.getLocation().getLongitude()==presenter.gpsUtil.getCurrentLocation().getLongitude()){
                                        if((System.currentTimeMillis() - gpsLocationData.getTimeInMillis())/1000 >30){
                                            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                                            PendingIntent pendingIntent = null;
                                            Intent loginIntent = null;
                                            loginIntent = new Intent(this, LoginActivity.class);
                                            loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S){
                                                pendingIntent = PendingIntent.getActivity(this, NotifyManager.gpsReminddId /* Request code */, loginIntent,
                                                        PendingIntent.FLAG_IMMUTABLE | PendingIntent.FLAG_CANCEL_CURRENT);
                                            }else{
                                                pendingIntent = PendingIntent.getActivity(this, NotifyManager.gpsReminddId /* Request code */, loginIntent,
                                                        PendingIntent.FLAG_CANCEL_CURRENT);
                                            }
//                                            notificationManager.notify(
//                                                    NotifyManager.gpsReminddId,
//                                                    notifyManager.createNormalMsg(getApplicationContext(), "請立刻打開行車寶 app  超過30秒gps未變動")
//                                                            .setContentIntent(pendingIntent)
//                                                            .build()
//                                            );
                                            gpsLocationData.setTimeInMillis(System.currentTimeMillis());
                                        }
                                    }else{
                                        gpsLocationData.setLocation(presenter.gpsUtil.getCurrentLocation());
                                        gpsLocationData.setTimeInMillis(System.currentTimeMillis());
                                    }
                                }
                            }
                        })
        );
        return START_NOT_STICKY;
    }

    public class MyBinder extends Binder {
        public MyService getService() {
            return MyService.this;
        }
    }
}
