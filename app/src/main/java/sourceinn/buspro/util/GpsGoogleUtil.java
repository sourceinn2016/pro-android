package sourceinn.buspro.util;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;

import android.location.Location;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

public class GpsGoogleUtil {
    Context context;
    private GoogleApiClient googleApiClient;// Google API用戶端物件
    private LocationRequest locationRequest;// Location請求物件
    public Location currentLocation; // 記錄目前最新的位置
    LocationListener locationListener;

    public GpsGoogleUtil(final Context context) {
        this.context = context;
        locationRequest = new LocationRequest();
        locationRequest.setInterval(5000); // 設定讀取位置資訊的間隔時間為一秒（1000ms）
        locationRequest.setFastestInterval(5000);  // 設定讀取位置資訊最快的間隔時間為一秒（1000ms）
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);// 設定優先讀取高精確度的位置資訊（GPS）
        googleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(@Nullable Bundle bundle) {
                        //currentLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient); 不取上次的位置

                        if (ActivityCompat.checkSelfPermission(context,
                                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                                ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        LocationServices.FusedLocationApi.requestLocationUpdates(
                                googleApiClient, locationRequest, locationListener);


                    }

                    @Override
                    public void onConnectionSuspended(int i) {

                    }
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

                    }
                })
                .addApi(LocationServices.API)
                .build();
    }

    //gps
    public void startLocationUpdates() {
        Log.v("QQQ", "startLocationUpdates");
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                if (location != null) {
                    Log.v("GGG", location.toString());
                    currentLocation = location;
                }
            }
        };


        googleApiClient.connect();

    }

    public void stopLocationUpdate(){
        if (googleApiClient.isConnected()){
                        LocationServices.FusedLocationApi.removeLocationUpdates(
                                        googleApiClient, locationListener);
                        googleApiClient.disconnect();
                   }
    }

    public Location getCurrentLocation() {
        return currentLocation;
    }

}
