package sourceinn.buspro.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class DateTimeUtil {
    public static String localToUTC(String localTime, String inFormat, String outFormat) {
        String utcTime = null;
        SimpleDateFormat localSdf = new SimpleDateFormat(inFormat);
        localSdf.setTimeZone(TimeZone.getDefault());
        Date localDate= null;
        try {
            localDate = localSdf.parse(localTime);

            SimpleDateFormat utcSdf = new SimpleDateFormat(outFormat);
            utcSdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            utcTime = utcSdf.format(localDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return utcTime;
    }

    public static String utcToLocal(String utcTime, String inFormat, String outFormat){
        String localTime = null;
        SimpleDateFormat utcSdf = new SimpleDateFormat(inFormat);
        utcSdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date utcDate = null;
        try {
            utcDate = utcSdf.parse(utcTime);
            SimpleDateFormat localSdf = new SimpleDateFormat(outFormat);
            localSdf.setTimeZone(TimeZone.getDefault());
            localTime = localSdf.format(utcDate.getTime());

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return localTime;
    }

    public static String millis2WeekOfYear(long millis){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(millis);
        calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
        return String.format("%02d", calendar.get(Calendar.WEEK_OF_YEAR));
    }

    public static String getDateTime(long timeInMillis){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeInMillis);
        String yyyy  = String.valueOf(calendar.get(Calendar.YEAR));
        String mm = String.format("%02d", calendar.get(Calendar.MONTH) + 1);
        String dd = String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH));
        String hh = String.format("%02d", calendar.get(Calendar.HOUR_OF_DAY));
        String ii = String.format("%02d", calendar.get(Calendar.MINUTE));
        String ss = String.format("%02d", calendar.get(Calendar.SECOND));
        return yyyy+"-"+mm+"-"+dd+" "+hh+":"+ii+":"+ss;
    }

    public static String getTimeString(long timeInMillis, String format){
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        Date date = new Date();
        date.setTime(timeInMillis);
        return sdf.format(date);
    }

    public static long stringToMillis(String time, String format){
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        long t = 0;
        try {
            t = sdf.parse(time).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return t;
    }

    //-1 0 1
    public static int compareDate(String t1, String t2, String format){
        int rt = 0;
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        try {
            if(sdf.parse(t1).getTime() > sdf.parse(t2).getTime()){
                rt = -1;
            }else if(sdf.parse(t1).getTime() < sdf.parse(t2).getTime()){
                rt = 1;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return rt;
    }
}
