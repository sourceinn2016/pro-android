package sourceinn.buspro;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.List;

import sourceinn.buspro.reponse.GetMemberResponse;
import sourceinn.buspro.reponse.LoginResponse;
import sourceinn.buspro.reponse.RxObject;


public class SearchMemberAdapter extends RecyclerView.Adapter {
    protected List<GetMemberResponse> mDatas;
    RxBus rxBus = RxBus.getDefault();
    protected Context mContext;
    public SearchMemberAdapter(Context context, List<GetMemberResponse> mDatas){
        mContext = context;
        this.mDatas = mDatas;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.dispatch_listitem, parent, false);
        return new MyViewHolder(view);

    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        ((MyViewHolder)holder).name.setText(mDatas.get(position).getName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RxObject rxObject = new RxObject();
                rxObject.setTag("AddRoute");
                rxObject.setObject(mDatas.get(position).getID());
                rxBus.post(rxObject);
            }
        });
        if(position==0){
            ((MyViewHolder)holder).line.setVisibility(View.GONE);
        }else{
            ((MyViewHolder)holder).line.setVisibility(View.VISIBLE);
        }
        ((MyViewHolder)holder).FramIsConductor.setVisibility(View.GONE);
        ((MyViewHolder)holder).FramIsRollCall.setVisibility(View.GONE);
    }



    @Override
    public int getItemCount()
    {
        return mDatas==null?0:mDatas.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView name;
        View line;
        FrameLayout FramIsConductor;
        FrameLayout FramIsRollCall;

        public MyViewHolder(View view)
        {
            super(view);
            FramIsConductor = (FrameLayout) view.findViewById(R.id.FramIsConductor);
            FramIsRollCall = (FrameLayout) view.findViewById(R.id.FramIsRollCall);
            name = (TextView)view.findViewById(R.id.name);
            line = view.findViewById(R.id.line);
        }
    }

}
