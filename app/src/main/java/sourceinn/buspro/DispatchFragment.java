package sourceinn.buspro;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import sourceinn.buspro.reponse.BaseResponse;
import sourceinn.buspro.reponse.GetMemberResponse;
import sourceinn.buspro.reponse.LoginResponse;
import sourceinn.buspro.reponse.RxObject;

/**
 * Created by sourceinn on 2016/9/14.
 */
public class DispatchFragment extends BaseFragment implements View.OnClickListener {
    RxBus rxBus = RxBus.getDefault();
    RecyclerView recyclerView;
    protected SwipeRefreshLayout laySwipe;
    GetMemberAdapter adapter;
    List<GetMemberResponse> mData = new ArrayList<>();
    Subscription mSubscription;
    Subscription rxListener;
    String RouteID;
    public static DispatchFragment newInstance(String routeid){
        Bundle args = new Bundle();
        args.putString("RouteID", routeid);
        DispatchFragment fragment = new DispatchFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dispatch_main, null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ImageView back = (ImageView) view.findViewById(R.id.back);
        ImageView add = (ImageView) view.findViewById(R.id.add);
        back.setOnClickListener(this);
        add.setOnClickListener(this);
        int dp = (int) Common.getDensity(mContext);
        add.setImageBitmap(Common.readBitMap(mContext, R.drawable.add_user, 40*dp, 40*dp));
        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        adapter = new GetMemberAdapter(mContext, mData);
        recyclerView.setAdapter(adapter);
        laySwipe = (SwipeRefreshLayout)view.findViewById(R.id.laySwipe);

        SwipeRefreshLayout.OnRefreshListener onSwipeToRefresh = new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                laySwipe.setRefreshing(true);
                getData();
            }
        };
        laySwipe.setOnRefreshListener(onSwipeToRefresh);

        RouteID = getArguments().getString("RouteID");
        getData();



    }

    @Override
    public void onPause() {
        super.onPause();
        if(mSubscription!=null && !mSubscription.isUnsubscribed()){
            mSubscription.unsubscribe();
        }
        if(rxListener!=null && !rxListener.isUnsubscribed()){
            rxListener.unsubscribe();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(rxListener==null || rxListener.isUnsubscribed()){
            rxListener = rxBus.toObservable(RxObject.class)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Action1<RxObject>() {
                        @Override
                        public void call(final RxObject rxObject) {
                            if (rxObject.getTag().equals("AddRoute")) {
                                Common.showMsgDialog(getChildFragmentManager(), getString(R.string.addCheck), new MsgDialogFragment.MsgDialogLister() {
                                    @Override
                                    public void sureEvent() {
                                        Common.showLoading(getChildFragmentManager(), islive);
                                        AddRoute((String) rxObject.getObject()).subscribeOn(Schedulers.newThread())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(new Action1<String>() {
                                            @Override
                                            public void call(String s) {
                                                Common.hideLoading(getChildFragmentManager(), islive);
                                                if (s.equals("Success")) {
                                                    FragmentManager fm = getChildFragmentManager();
                                                    FragmentTransaction ft = fm.beginTransaction();
                                                    Fragment prev = fm.findFragmentByTag("DispatchDoalog");
                                                    if (prev != null) {
                                                        ft.remove(prev);
                                                    }
                                                    ft.commitAllowingStateLoss();
                                                    getData();
                                                    Toast.makeText(mContext, getString(R.string.addSuccess), Toast.LENGTH_SHORT).show();
                                                } else {
                                                    Toast.makeText(mContext, getString(R.string.addFail), Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        }, new Action1<Throwable>() {
                                            @Override
                                            public void call(Throwable throwable) {
                                                throwable.printStackTrace();
                                                Common.hideLoading(getChildFragmentManager(), islive);
                                            }
                                        }, new Action0() {
                                            @Override
                                            public void call() {
                                                Common.hideLoading(getChildFragmentManager(), islive);
                                            }
                                        });

                                    }
                                }, "");

                            }
                        }
                    });

        }
    }

    private Observable<String> AddRoute(final String MemberID){
        return Observable.create(new Observable.OnSubscribe<String>() {
            @Override
            public void call(Subscriber<? super String> subscriber) {
                try {
                    HashMap<String, String> para = new HashMap<String, String>();
                    para.put("Token", LoginResponse.getLastest(getContext()).getToken());
                    para.put("RouteID", RouteID);
                    para.put("MemberID", MemberID);
                    String resp = OkHttpUtil.execute(OkHttpUtil.postRequest(getString(R.string.base_url) + "AddRoute", para)).body().string();
                    Gson gson = new Gson();
                    BaseResponse<Boolean> cc = gson.fromJson(resp, new TypeToken<BaseResponse<Boolean>>() {}.getType());
                    if(cc.getMessage().equals("Token 失效！")){
                        RxObject rxObject = new RxObject();
                        rxObject.setTag("Token失效");
                        rxBus.post(rxObject);
                    }else {
                        subscriber.onNext(cc.getResult());
                    }
                } catch (IOException e) {
                    subscriber.onError(e);
                }
                subscriber.onCompleted();
            }
        });
    }

    protected void getData(){
        laySwipe.post(new Runnable() {
            @Override
            public void run() {
                laySwipe.setRefreshing(true);
            }
        });
        if(mSubscription!=null && !mSubscription.isUnsubscribed()){
            mSubscription.unsubscribe();
        }
        mSubscription = GetMemberList().subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        new Action1<List<GetMemberResponse>>() {
                            @Override
                            public void call(List<GetMemberResponse> routeResponses) {
                                adapter.notifyDataSetChanged();
                                laySwipe.setRefreshing(false);
                            }
                        },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable e) {
                                e.printStackTrace();
                                adapter.notifyDataSetChanged();
                                laySwipe.setRefreshing(false);
                            }
                        }
                );

    }

    private Observable<List<GetMemberResponse>> GetMemberList(){
        return Observable.create(new Observable.OnSubscribe<List<GetMemberResponse>>() {
            @Override
            public void call(Subscriber<? super List<GetMemberResponse>> subscriber) {
                try {
                    HashMap<String, String> para = new HashMap<String, String>();
                    para.put("Token", LoginResponse.getLastest(getContext()).getToken());
                    para.put("RouteID", RouteID);
                    String resp = OkHttpUtil.execute(OkHttpUtil.postRequest(getString(R.string.base_url) + "GetMemberList", para)).body().string();
                    Gson gson = new Gson();
                    BaseResponse<List<GetMemberResponse>> cc = gson.fromJson(resp, new TypeToken<BaseResponse<List<GetMemberResponse>>>() {}.getType());
                    if(cc.getMessage().equals("Token 失效！")){
                        RxObject rxObject = new RxObject();
                        rxObject.setTag("Token失效");
                        rxBus.post(rxObject);
                    }else {
                        mData.clear();
                        if (cc.getData() != null) {
                            mData.addAll(cc.getData());
                        }
                        subscriber.onNext(cc.getData());
                    }
                } catch (IOException e) {
                    subscriber.onError(e);
                }
                subscriber.onCompleted();
            }
        });
    }


    @Override
    public void onClick(View v) {
        RxObject rxObject = new RxObject();;
        switch (v.getId()){
            case R.id.back:
                rxObject.setTag("back");
                rxBus.post(rxObject);
                break;
            case R.id.add:
                FragmentManager fm = getChildFragmentManager();
                FragmentTransaction ft =fm.beginTransaction();
                Fragment prev = fm.findFragmentByTag("DispatchDoalog");
                if (prev != null) {
                    ft.remove(prev);
                }
                final DispatchDialogFragment newFragment = DispatchDialogFragment.newInstance(LoginResponse.getLastest(getContext()).getToken());
                ft.add(newFragment, "DispatchDoalog");
                ft.commitAllowingStateLoss();
                break;
        }
    }
}
