package sourceinn.buspro;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.schedulers.TimeInterval;
import sourceinn.buspro.reponse.BaseResponse;
import sourceinn.buspro.reponse.LoginResponse;
import sourceinn.buspro.reponse.RouteResponse;
import sourceinn.buspro.reponse.RxObject;
import sourceinn.buspro.ui.route.presenter.RoutePresenter;

/**
 * Created by sourceinn on 2016/9/14.
 */
public class RouteFragment extends BaseFragment implements View.OnClickListener {
    RxBus rxBus = RxBus.getDefault();
    RecyclerView recyclerView;
    protected SwipeRefreshLayout laySwipe;
    RouteAdapter adapter;
    List<RouteResponse> mData = new ArrayList<>();
    Subscription timeSub;
    Subscription mSubscription;
    RoutePresenter presenter;

    public static RouteFragment newInstance() {
        return new RouteFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.route, null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter = new RoutePresenter();
        ImageView bg = (ImageView) view.findViewById(R.id.bg);
        bg.setImageBitmap(Common.readBitMap(mContext, R.drawable.list_background, bg.getWidth(), bg.getHeight()));
        ImageView logout = (ImageView) view.findViewById(R.id.logout);
        ImageView setting = (ImageView) view.findViewById(R.id.setting);
        Button abnormal = (Button) view.findViewById(R.id.abnormal);
        Button push = (Button) view.findViewById(R.id.push);
        Button scanNFC = (Button) view.findViewById(R.id.scanNFC);
        Button message = view.findViewById(R.id.message);
        message.setOnClickListener(this);
        logout.setOnClickListener(this);
        logout.setOnClickListener(this);
        setting.setOnClickListener(this);
        abnormal.setOnClickListener(this);
        push.setOnClickListener(this);
        scanNFC.setOnClickListener(this);

        LinearLayout admin_area = (LinearLayout) view.findViewById(R.id.admin_area);
        if (LoginResponse.getLastest(getContext()).getResult().equals("admin")) {
            push.setVisibility(View.VISIBLE);
            abnormal.setVisibility(View.VISIBLE);
            scanNFC.setVisibility(View.VISIBLE);
        } else {//parent
            push.setVisibility(View.GONE);
            abnormal.setVisibility(View.GONE);
            scanNFC.setVisibility(View.GONE);
            view.findViewById(R.id.line1).setVisibility(View.GONE);
            view.findViewById(R.id.line2).setVisibility(View.GONE);
            view.findViewById(R.id.line3).setVisibility(View.GONE);

        }
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        adapter = new RouteAdapter(mContext, mData);
        recyclerView.setAdapter(adapter);
        laySwipe = (SwipeRefreshLayout) view.findViewById(R.id.laySwipe);

        SwipeRefreshLayout.OnRefreshListener onSwipeToRefresh = new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                laySwipe.setRefreshing(true);
                getData();
            }
        };
        laySwipe.setOnRefreshListener(onSwipeToRefresh);

        getData();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (timeSub != null && !timeSub.isUnsubscribed()) {
            timeSub.unsubscribe();
        }
        if (mSubscription != null && !mSubscription.isUnsubscribed()) {
            mSubscription.unsubscribe();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        timeSub = Observable.interval(10000L, TimeUnit.MILLISECONDS).timeInterval()
                .subscribe(new Action1<TimeInterval<Long>>() {
                    @Override
                    public void call(TimeInterval<Long> longTimeInterval) {
                        getData();
                    }
                });
    }

    protected void getData() {

        laySwipe.post(new Runnable() {
            @Override
            public void run() {
                laySwipe.setRefreshing(true);
            }
        });
        if (mSubscription != null && !mSubscription.isUnsubscribed()) {
            mSubscription.unsubscribe();
        }
        HashMap<String, String> para = new HashMap<String, String>();
        para.put("Token", LoginResponse.getLastest(getContext()).getToken());
        mSubscription = presenter.getRoute(para)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    if (((BaseResponse<List<RouteResponse>>) response).getMessage().equals("Token 失效！")) {
                        RxObject rxObject = new RxObject();
                        rxObject.setTag("Token失效");
                        rxBus.post(rxObject);
                    } else {
                        List<RouteResponse> list = ((BaseResponse<List<RouteResponse>>) response).getData();
                        mData.clear();
                        if (list != null) {
                            mData.addAll(list);
                        }
                        adapter.notifyDataSetChanged();
                        laySwipe.setRefreshing(false);
                    }

                }, error -> {
                    adapter.notifyDataSetChanged();
                    laySwipe.setRefreshing(false);
                });

//        mSubscription =route()
//                .subscribeOn(Schedulers.newThread())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(cc->{
//
//                    adapter.notifyDataSetChanged();
//                    laySwipe.setRefreshing(false);
//                }, error->{
//                    Log.v("QQQ", "eeeee");
//                    adapter.notifyDataSetChanged();
//                    laySwipe.setRefreshing(false);
//                });


    }

    private Observable<BaseResponse<List<RouteResponse>>> route() {
        return Observable.create(new Observable.OnSubscribe<BaseResponse<List<RouteResponse>>>() {
            @Override
            public void call(Subscriber<? super BaseResponse<List<RouteResponse>>> subscriber) {
                try {

                    String resp = OkHttpUtil.execute(OkHttpUtil.getRequest(getString(R.string.base_url) + "Route?Token=" +
                            LoginResponse.getLastest(getContext()).getToken())).body().string();
                    //Log.v("QQQ", resp);
                    Gson gson = new Gson();
                    BaseResponse<List<RouteResponse>> cc = gson.fromJson(resp, new TypeToken<BaseResponse<List<RouteResponse>>>() {
                    }.getType());
                    if (cc.getMessage().equals("Token 失效！")) {
                        RxObject rxObject = new RxObject();
                        rxObject.setTag("Token失效");
                        rxBus.post(rxObject);
                    }
                    mData.clear();
                    if (cc.getData() != null) {
                        mData.addAll(cc.getData());
                    }
                    subscriber.onNext(cc);
                } catch (IOException e) {
                    subscriber.onError(e);
                }
                subscriber.onCompleted();
            }
        });
    }


    @Override
    public void onClick(View v) {
        RxObject rxObject = new RxObject();
        ;
        switch (v.getId()) {
            case R.id.logout:
                rxObject.setTag("logout");
                rxBus.post(rxObject);
                break;
            case R.id.setting:
                rxObject.setTag("setting");
                rxBus.post(rxObject);
                break;
            case R.id.abnormal:
                rxObject.setTag("abnormal");
                rxBus.post(rxObject);
                break;
            case R.id.push:
                rxObject.setTag("push");
                rxBus.post(rxObject);
                break;
            case R.id.scanNFC:
                Intent intent = new Intent();
                intent.setClass(mContext, UploadNFCActivity.class);
                ((Activity) mContext).startActivity(intent);
                break;
            case R.id.message:
                PushMessageDialogFg.showPushMessageDialogFg(getChildFragmentManager());
                break;

        }
    }
}
