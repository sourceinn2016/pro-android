package sourceinn.buspro;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import sourceinn.buspro.reponse.LoginResponse;
import sourceinn.buspro.reponse.RollcallResponse;
import sourceinn.buspro.reponse.RxObject;


public class RollcallAdapter extends RecyclerView.Adapter {
    RxBus rxBus = RxBus.getDefault();
    protected List<RollcallResponse> mDatas;
    protected Context mContext;

    public RollcallAdapter(Context context, List<RollcallResponse> mDatas) {
        mContext = context;
        this.mDatas = mDatas;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.rollcall_item, parent, false);
        return new MyViewHolder(view);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        ((MyViewHolder) holder).name.setText(mDatas.get(position).getName());
        if (mDatas.get(position).isStatus()) {
            ((MyViewHolder) holder).rollcall.setVisibility(View.GONE);
            ((MyViewHolder) holder).cancel.setVisibility(View.VISIBLE);
        } else {
            ((MyViewHolder) holder).rollcall.setVisibility(View.VISIBLE);
            ((MyViewHolder) holder).cancel.setVisibility(View.GONE);
        }
        if (mDatas.get(position).getParentsPhone().size() > 0) {
            ((MyViewHolder) holder).tel1.setText(mDatas.get(position).getParentsPhone().get(0));
            ((MyViewHolder) holder).tel1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Log.v("QQQ", mDatas.get(position).getParentsPhone().get(0));
                    RxObject rxObject = new RxObject();
                    rxObject.setTag("call");
                    rxObject.setObject(mDatas.get(position).getParentsPhone().get(0));
                    rxBus.post(rxObject);
                }
            });
            if(mDatas.get(position).getParentsPhone().size()==2){
                ((MyViewHolder)holder).tel2.setText(mDatas.get(position).getParentsPhone().get(1));
                ((MyViewHolder) holder).tel2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        RxObject rxObject = new RxObject();
                        rxObject.setTag("call");
                        rxObject.setObject(mDatas.get(position).getParentsPhone().get(1));
                        rxBus.post(rxObject);
                    }
                });
            }else{
                ((MyViewHolder)holder).tel2.setText("");
            }
        }else{
            ((MyViewHolder)holder).tel1.setText("");
            ((MyViewHolder)holder).tel2.setText("");
        }
        ((MyViewHolder)holder).rollcall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RxObject rxObject = new RxObject();
                rxObject.setTag("rollcall");
                rxObject.setObject(mDatas.get(position).getID());
                rxBus.post(rxObject);
            }
        });
        ((MyViewHolder)holder).cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RxObject rxObject = new RxObject();
                rxObject.setTag("cancel");
                rxObject.setObject(mDatas.get(position).getID());
                rxBus.post(rxObject);
            }
        });
        ((MyViewHolder)holder).notify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RxObject rxObject = new RxObject();
                rxObject.setTag("notify");
                rxObject.setObject(mDatas.get(position).getID());
                rxBus.post(rxObject);
            }
        });
        ((MyViewHolder)holder).sign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!mDatas.get(position).isSign()) {
                    Intent intent = new Intent();
                    intent.setClass(mContext, SignActivity.class);
                    intent.putExtra("MemberID", mDatas.get(position).getID());
                    ((Activity) mContext).startActivity(intent);
                }
            }
        });
        ((MyViewHolder)holder).sign.setText(mDatas.get(position).isSign()?mContext.getString(R.string.alreadyAccept):mContext.getString(R.string.accept));
        ((MyViewHolder)holder).sign.setBackgroundResource(mDatas.get(position).isSign()?R.drawable.btngray:R.drawable.btnblue);

        if(position==0){
            ((MyViewHolder)holder).line.setVisibility(View.GONE);
        }else{
            ((MyViewHolder)holder).line.setVisibility(View.VISIBLE);
        }
    }




    @Override
    public int getItemCount()
    {
        return mDatas==null?0:mDatas.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView name;
        TextView tel1;
        TextView tel2;
        Button rollcall;
        Button cancel;
        Button notify;
        Button sign;
        View line;


        public MyViewHolder(View view)
        {
            super(view);
            name = (TextView)view.findViewById(R.id.name);
            tel1 = (TextView)view.findViewById(R.id.tel1);
            tel2 = (TextView)view.findViewById(R.id.tel2);
            rollcall = (Button)view.findViewById(R.id.rollcall);
            cancel = (Button)view.findViewById(R.id.cancel);
            notify = (Button)view.findViewById(R.id.notify);
            sign = (Button)view.findViewById(R.id.sign);
            line = view.findViewById(R.id.line);
        }
    }

}
