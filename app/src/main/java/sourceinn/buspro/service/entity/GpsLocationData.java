package sourceinn.buspro.service.entity;

import android.location.Location;

import java.io.Serializable;

public class GpsLocationData implements Serializable {
    Location location;
    Long timeInMillis = System.currentTimeMillis();

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Long getTimeInMillis() {
        return timeInMillis;
    }

    public void setTimeInMillis(Long timeInMillis) {
        this.timeInMillis = timeInMillis;
    }
}
