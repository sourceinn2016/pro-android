package sourceinn.buspro;

import android.Manifest;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.media.AudioManager;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Build;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.functions.Func2;
import rx.schedulers.Schedulers;
import rx.schedulers.TimeInterval;
import sourceinn.buspro.reponse.BaseResponse;
import sourceinn.buspro.reponse.LoginResponse;
import sourceinn.buspro.reponse.RollcallResponse;
import sourceinn.buspro.reponse.RouteResponse;
import sourceinn.buspro.reponse.RxObject;
import sourceinn.buspro.reponse.StationResponse;
import sourceinn.buspro.reponse.StudentResponse;
import sourceinn.buspro.ui.login.view.LoginActivity;

/**
 * Created by sourceinn on 2016/9/14.
 */
public class MainActivity extends BaseActivity {
    RxBus rxBus = RxBus.getDefault();
    protected Subscription fragmentSub;
    Subscription checkStatus;
    TextView error;

    private NfcAdapter mNfcAdapter;
    private PendingIntent mPendingIntent;
    private TextToSpeech mTts;
    boolean isScaning = false;
    Calendar calendar;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        calendar = Calendar.getInstance();

        error = (TextView) findViewById(R.id.error);


        onCreateCallback();
        //}else{
        //startGPS();
        //}

        if(getIntent().getBooleanExtra("openMessage", false)) {
            PushMessageDialogFg.showPushMessageDialogFg(getSupportFragmentManager());
        }

    }



    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        calendar.setTimeInMillis(System.currentTimeMillis());
        if(!isScaning) {
            final Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fg);
            if (fragment instanceof RollcallFragment) {
                //isScaning = true;
                Common.showLoading(getSupportFragmentManager(), true);
                final Tag tag = intent.getParcelableExtra(mNfcAdapter.EXTRA_TAG);
                //Log.v("QQQ", Common.bytesToHexString(tag.getId()));
                final String cardID = Common.bytesToHexString(tag.getId());

                Observable.zip(
                        Observable.create(new Observable.OnSubscribe<BaseResponse<StudentResponse>>() {
                            @Override
                            public void call(Subscriber<? super BaseResponse<StudentResponse>> subscriber) {
                                try {
                                    String resp = OkHttpUtil.execute(OkHttpUtil.getRequest(getString(R.string.base_url) + "Student?Token=" +
                                            LoginResponse.getLastest(MainActivity.this).getToken())).body().string();
                                    Gson gson = new Gson();
                                    BaseResponse<StudentResponse> cc = gson.fromJson(resp, new TypeToken<BaseResponse<StudentResponse>>() {
                                    }.getType());
                                    subscriber.onNext(cc);
                                } catch (IOException e) {
                                    subscriber.onError(e);
                                }
                                subscriber.onCompleted();
                            }


                        }),
                        Observable.create(new Observable.OnSubscribe<BaseResponse<List<RollcallResponse>>>() {
                            @Override
                            public void call(Subscriber<? super BaseResponse<List<RollcallResponse>>> subscriber) {
                                try {
                                    String resp = OkHttpUtil.execute(OkHttpUtil.getRequest(getString(R.string.base_url) + "Rollcall?Token=" +
                                            LoginResponse.getLastest(MainActivity.this).getToken())).body().string();
                                    Gson gson = new Gson();
                                    BaseResponse<List<RollcallResponse>> cc = gson.fromJson(resp, new TypeToken<BaseResponse<List<RollcallResponse>>>() {
                                    }.getType());
                                    subscriber.onNext(cc);
                                } catch (IOException e) {
                                    subscriber.onError(e);
                                }
                                subscriber.onCompleted();
                            }
                        }), new Func2<BaseResponse<StudentResponse>, BaseResponse<List<RollcallResponse>>, String>() {
                            @Override
                            public String call(BaseResponse<StudentResponse> studentResponseBaseResponse, BaseResponse<List<RollcallResponse>> listBaseResponse) {
                                //查最後的進站記錄
                                String staionID = "";
                                if (studentResponseBaseResponse.getMessage().equals("Token 失效！") || listBaseResponse.getMessage().equals("Token 失效！")) {

                                } else {
                                    List<StationResponse> list = studentResponseBaseResponse.getData().getList();
                                    for (int i = list.size() - 1; i >= 0; i--) {
                                        if (!list.get(i).getStopTime().equals("--")) {
                                            //Log.v("QQQ", "找到進站的STATIONID");
                                            staionID = list.get(i).getID();
                                            break;
                                        }
                                    }
                                    if (staionID.length() == 0) {//假如沒有進站記錄 去比對卡號對應的站點id
                                        if(list.size()>0) {//default StationID
                                            staionID = list.get(0).getID();
                                        }
                                        //Log.v("QQQ", "車沒進站要去rollcall找對硬卡到的STATIONID");
                                        List<RollcallResponse> rollList = listBaseResponse.getData();
                                        for (int i = 0; i < rollList.size(); i++) {
                                            if (rollList.get(i).getCardID()!=null && rollList.get(i).getCardID().equals(cardID) ) {
                                                staionID = rollList.get(i).getStationID();
                                                break;
                                            }
                                        }
                                    }
                                }
                                return staionID;
                            }
                        }).subscribeOn(Schedulers.newThread())
                        .flatMap(new Func1<String, Observable<BaseResponse>>() {
                            @Override
                            public Observable<BaseResponse> call(final String stationID) {
                                //Log.v("QQQ", "StationID="+stationID);
                                    return Observable.create(new Observable.OnSubscribe<BaseResponse>() {
                                        @Override
                                        public void call(Subscriber<? super BaseResponse> subscriber) {
                                            if(stationID.length()>0) {
                                                try {
                                                    HashMap para = new HashMap();
                                                    para.put("Token", LoginResponse.getLastest(MainActivity.this).getToken());
                                                    para.put("StationID", stationID);
                                                    para.put("CardID", cardID);
                                                    para.put("CreateTime", sdf.format(calendar.getTime()));
                                                    //Log.v("QQQ", para.toString());
                                                    String resp = OkHttpUtil.execute(OkHttpUtil.postRequest(getString(R.string.base_url) + "RollcallByNFC", para)).body().string();
                                                    //Log.v("QQQ", resp);
                                                    Gson gson = new Gson();
                                                    BaseResponse<List<RollcallResponse>> cc = gson.fromJson(resp, new TypeToken<BaseResponse<List<RollcallResponse>>>() {
                                                    }.getType());
                                                    subscriber.onNext(cc);
                                                } catch (IOException e) {
                                                    subscriber.onError(e);
                                                }
                                            }else{//只要有站點就不會發生  因為有設首站default
                                                BaseResponse response = new BaseResponse();
                                                response.setResult("Error");
                                                response.setMessage(getString(R.string.cardFail));
                                                subscriber.onNext(response);
                                            }
                                            subscriber.onCompleted();
                                        }
                                    });
                                }

                        })
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Action1<BaseResponse>() {
                            @Override
                            public void call(BaseResponse o) {
                                //String msg = o.getResult().equals("Success")? "刷卡成功" : "卡號不存在";
                                String msg = o.getMessage();
                                String showMsg = null;
                                String speakMsg = null;
                                if(msg.contains(":")){//站點名稱+名子
                                    speakMsg = getString(R.string.cardSuccess);
                                    showMsg = msg + "\n" + speakMsg;
                                }else if(msg.equals("查無此學生") || msg.equals("卡號不存在")){//多國語言?
                                    speakMsg = getString(R.string.noThisCard);
                                    showMsg = getString(R.string.noThisCard)+cardID;
                                }else{//念名子
                                    speakMsg = msg;
                                    showMsg = msg;
                                }
                                HashMap hashAudio = new HashMap<String, String>();
                                hashAudio.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, String.valueOf(AudioManager.STREAM_MUSIC));
                                mTts.speak(speakMsg, TextToSpeech.QUEUE_FLUSH, hashAudio);
                                Common.showBigMsgDialog(getSupportFragmentManager(), showMsg, null, "noCancel");
                            }
                        }, new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                throwable.printStackTrace();
                                Common.hideLoading(getSupportFragmentManager(), true);
                                isScaning = false;

                                HashMap hashAudio = new HashMap<String, String>();
                                hashAudio.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, String.valueOf(AudioManager.STREAM_MUSIC));
                                mTts.speak(getString(R.string.cardFail), TextToSpeech.QUEUE_FLUSH, hashAudio);
                                Common.showBigMsgDialog(getSupportFragmentManager(), getString(R.string.cardFail)+throwable.getMessage(), null, "noCancel");
                            }
                        }, new Action0() {
                            @Override
                            public void call() {
                                Common.hideLoading(getSupportFragmentManager(), true);
                                isScaning = false;
                            }
                        });


            }
        }
    }

    private void onCreateCallback() {
        if(LoginResponse.getLastest(this).isRollCall()){
            mNfcAdapter = mNfcAdapter.getDefaultAdapter(this);
            //if (mNfcAdapter != null && mNfcAdapter.isEnabled()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                mPendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()), 0|PendingIntent.FLAG_IMMUTABLE);
            }else{
                mPendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()), 0);
            }
            mTts = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
                @Override
                public void onInit(int status) {
                    if (status == TextToSpeech.SUCCESS) {
                        int result = mTts.setLanguage(Locale.CHINESE);
                        if (result != TextToSpeech.LANG_MISSING_DATA && result != TextToSpeech.LANG_NOT_SUPPORTED) {
                            mTts.setOnUtteranceCompletedListener(new TextToSpeech.OnUtteranceCompletedListener() {
                                @Override
                                public void onUtteranceCompleted(String s) {

                                }
                            });
                        }
                    }
                }
            });
            //}
        }
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        if (LoginResponse.getLastest(this).getResult().equals("student") || LoginResponse.getLastest(this).getResult().equals("driver")) {
            //ft.replace(R.id.fg, StationFragment.newInstance());
            ft.replace(R.id.fg, ViewPagerFragment.newInstance(null, null));
        } else if (LoginResponse.getLastest(this).getResult().equals("admin")) {// admin
            ft.replace(R.id.fg, RouteFragment.newInstance());
        } else {//parents
            Observable.create(new Observable.OnSubscribe<List<RouteResponse>>() {
                @Override
                public void call(Subscriber<? super List<RouteResponse>> subscriber) {
                    try {

                        String resp = OkHttpUtil.execute(OkHttpUtil.getRequest(getString(R.string.base_url) + "Route?Token=" +
                                LoginResponse.getLastest(MainActivity.this).getToken())).body().string();
                        Gson gson = new Gson();
                        BaseResponse<List<RouteResponse>> cc = gson.fromJson(resp, new TypeToken<BaseResponse<List<RouteResponse>>>() {
                        }.getType());
                        //Log.v("QQQ", resp);
                        subscriber.onNext(cc.getData());
                    } catch (IOException e) {
                        subscriber.onError(e);
                    }
                    subscriber.onCompleted();
                }
            }).subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Action1<List<RouteResponse>>() {
                        @Override
                        public void call(List<RouteResponse> routeResponses) {
                            FragmentManager fm = getSupportFragmentManager();
                            FragmentTransaction ft = fm.beginTransaction();

                            if (routeResponses == null) {//當天沒有路線
                                ft.replace(R.id.fg, RouteFragment.newInstance());
                            } else if (routeResponses.size() == 1) {
                                //ft.replace(R.id.fg, StationFragment.newInstance(routeResponses.get(0), "onlyOne"));
                                ft.replace(R.id.fg, ViewPagerFragment.newInstance(routeResponses.get(0), "onlyOne"));
                            } else {
                                ft.replace(R.id.fg, RouteFragment.newInstance());
                            }
                            ft.commitAllowingStateLoss();
                        }
                    }, new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            throwable.printStackTrace();
                        }
                    });
        }
        ft.commitAllowingStateLoss();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        //Log.v("QQQ", "onRequestPermissionsResult");
        if (requestCode == 123) {
            if (LoginResponse.getLastest(this).isConductor()) {//車長一定要可以發GPS
                if (Common.checkGrantResult(grantResults)) {
                    startGPS();
                    onCreateCallback();
                } else {
                    Common.showMsgDialog(getSupportFragmentManager(), getString(R.string.IDNeedOpenPosition), new MsgDialogFragment.MsgDialogLister() {
                        @Override
                        public void sureEvent() {
                            Common.logout(MainActivity.this);
                            finish();
                        }
                    }, "noCancel");
                }
            } else {
                onCreateCallback();
            }


        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mTts != null) {
            mTts.stop();
            mTts.shutdown();
        }
    }

    private void startGPS() {
        Observable.create(new Observable.OnSubscribe<BaseResponse<StudentResponse>>() {
            @Override
            public void call(Subscriber<? super BaseResponse<StudentResponse>> subscriber) {
                try {
                    String resp;

                    resp = OkHttpUtil.execute(OkHttpUtil.getRequest(getString(R.string.base_url) + "Student?Token=" +
                            LoginResponse.getLastest(MainActivity.this).getToken())).body().string();
                    Gson gson = new Gson();
                    BaseResponse<StudentResponse> cc = gson.fromJson(resp, new TypeToken<BaseResponse<StudentResponse>>() {
                    }.getType());
                    subscriber.onNext(cc);
                } catch (IOException e) {
                    subscriber.onError(e);
                }
                subscriber.onCompleted();
            }
        })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<BaseResponse<StudentResponse>>() {
                    @Override
                    public void call(BaseResponse<StudentResponse> studentResponseBaseResponse) {
                        if (studentResponseBaseResponse.getData() == null) {
                            Common.cleanGPS(MainActivity.this);
                            Common.showMsgDialog(getSupportFragmentManager(), getString(R.string.noSetStation), new MsgDialogFragment.MsgDialogLister() {
                                @Override
                                public void sureEvent() {
                                    logoutFunction();
                                }
                            }, "noCancel");
                        } else {

                        }
                    }
                });

    }


    @Override
    protected void onPause() {
        super.onPause();
        if (fragmentSub != null && !fragmentSub.isUnsubscribed()) {
            fragmentSub.unsubscribe();
        }
        if (checkStatus != null && !checkStatus.isUnsubscribed()) {
            checkStatus.unsubscribe();
        }
        if (mNfcAdapter != null) {
            mNfcAdapter.disableForegroundDispatch(this);
        }
    }

    private Observable<BaseResponse> logout() {
        return Observable.create(new Observable.OnSubscribe<BaseResponse>() {
            @Override
            public void call(Subscriber<? super BaseResponse> subscriber) {
                try {
                    String resp = OkHttpUtil.execute(OkHttpUtil.getRequest(getString(R.string.base_url) + "Logout?Token=" +
                            LoginResponse.getLastest(MainActivity.this).getToken())).body().string();
                    Gson gson = new Gson();
                    BaseResponse cc = gson.fromJson(resp, new TypeToken<BaseResponse>() {
                    }.getType());
                    subscriber.onNext(cc);
                } catch (Exception e) {

                }
                subscriber.onCompleted();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (fragmentSub == null || fragmentSub.isUnsubscribed()) {
            fragmentSub = rxBus.toObservable(RxObject.class)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Action1<RxObject>() {
                        @Override
                        public void call(RxObject rxObject) {
                            if (rxObject.getTag().equals("RouteToStation")) {
                                FragmentManager fm = getSupportFragmentManager();
                                FragmentTransaction ft = fm.beginTransaction();
                                //ft.replace(R.id.fg, StationFragment.newInstance((RouteResponse) rxObject.getObject(), "moreThanOne"));
                                ft.replace(R.id.fg, ViewPagerFragment.newInstance((RouteResponse) rxObject.getObject(), "moreThanOne"));
                                ft.addToBackStack(null);
                                ft.commit();
                            } else if (rxObject.getTag().equals("setting")) {
                                FragmentManager fm = getSupportFragmentManager();
                                FragmentTransaction ft = fm.beginTransaction();
                                ft.replace(R.id.fg, SettingFragment.newInstance());
                                ft.addToBackStack(null);
                                ft.commit();
                            } else if (rxObject.getTag().equals("dispatch")) {
                                FragmentManager fm = getSupportFragmentManager();
                                FragmentTransaction ft = fm.beginTransaction();
                                ft.replace(R.id.fg, DispatchFragment.newInstance((String) rxObject.getObject()));
                                ft.addToBackStack(null);
                                ft.commit();
                            } else if (rxObject.getTag().equals("abnormal")) {
                                FragmentManager fm = getSupportFragmentManager();
                                FragmentTransaction ft = fm.beginTransaction();
                                ft.replace(R.id.fg, AbnormalFragment.newInstance());
                                ft.addToBackStack(null);
                                ft.commit();
                            } else if (rxObject.getTag().equals("back")) {
                                onBackPressed();
                            } else if (rxObject.getTag().equals("StationToRollcall")) {
                                FragmentManager fm = getSupportFragmentManager();
                                FragmentTransaction ft = fm.beginTransaction();
                                //ft.setCustomAnimations(R.anim.fragment_in, R.anim.fragment_out);
                                ft.replace(R.id.fg, RollcallFragment.newInstance((String) rxObject.getObject()));
                                ft.addToBackStack(null);
                                ft.commit();
                            } else if (rxObject.getTag().equals("logout")) {
                                Common.showLoading(getSupportFragmentManager(), islive);
                                logoutFunction();
                            } else if (rxObject.getTag().equals("error")) {
                                error.setText((String) rxObject.getObject());
                                error.setVisibility(View.VISIBLE);
                            } else if (rxObject.getTag().equals("right")) {
                                error.setVisibility(View.GONE);
                            } else if (rxObject.getTag().equals("push")) {
                                FragmentManager fm = getSupportFragmentManager();
                                FragmentTransaction ft = fm.beginTransaction();
                                ft.replace(R.id.fg, PushFragment.newInstance());
                                ft.addToBackStack(null);
                                ft.commit();
                            } else if (rxObject.getTag().equals("showloading")) {
                                Common.showLoading(getSupportFragmentManager(), islive);
                            } else if (rxObject.getTag().equals("hideloading")) {
                                Common.hideLoading(getSupportFragmentManager(), islive);
                            } else if (rxObject.getTag().equals("Token失效")) {
                                Common.showMsgDialog(getSupportFragmentManager(), getString(R.string.otherLogin), new MsgDialogFragment.MsgDialogLister() {
                                    @Override
                                    public void sureEvent() {
                                        logoutFunction();
                                    }
                                }, "noCancel");

                            } else if (rxObject.getTag().equals("ChangeRoute")) {
                                //Common.updateToken(MainActivity.this, "123123123");
                                recreate();

                            }
                        }
                    });

        }

        checkStatus = Observable.interval(0, 2000L, TimeUnit.MILLISECONDS).timeInterval()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<TimeInterval<Long>>() {
                    @Override
                    public void call(TimeInterval<Long> longTimeInterval) {
                        if (LoginResponse.getLastest(MainActivity.this).isConductor()) {
                            LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                                error.setText(getString(R.string.plsOpenGPS));
                                error.setVisibility(View.VISIBLE);
                            } else if (!Common.checkIntenet(getApplicationContext())) {
                                error.setText(getString(R.string.plsOpenInternet));
                                error.setVisibility(View.VISIBLE);
                            } else {
                                error.setVisibility(View.GONE);
                            }
                        } else {
                            if (!Common.checkIntenet(getApplicationContext())) {
                                error.setText(getString(R.string.plsOpenInternet));
                                error.setVisibility(View.VISIBLE);
                            } else {
                                error.setVisibility(View.GONE);
                            }
                        }

                    }
                });

        if (mNfcAdapter != null) {
            //Log.v("QQQ", "mNfcAdapter.enableForegroundDispatch(this, mPendingIntent, null, null);");
            mNfcAdapter.enableForegroundDispatch(this, mPendingIntent, null, null);
        }else{
            //Log.v("QQQ", "mNfcAdapter == null");
        }
    }

    public void logoutFunction() {
        logout().subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<BaseResponse>() {
                               @Override
                               public void call(BaseResponse baseResponse) {
                                   Common.deleteSign(MainActivity.this);
                                   Common.logout(MainActivity.this);
                                   Common.hideLoading(getSupportFragmentManager(), islive);
                                   Intent intent = new Intent();
                                   intent.setClass(MainActivity.this, LoginActivity.class);
                                   startActivity(intent);
                                   finish();
                               }
                           },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                Common.hideLoading(getSupportFragmentManager(), islive);
                            }
                        });
    }

    public boolean checkPermission() {
        return ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fg);
        if (fragment instanceof ViewPagerFragment) {
            if (((ViewPagerFragment) fragment).viewpager.getCurrentItem() != 0) {
                ((ViewPagerFragment) fragment).viewpager.setCurrentItem(0);
            } else {
                super.onBackPressed();
            }
        } else {
            super.onBackPressed();
        }
    }


}
