package sourceinn.buspro;

import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import sourceinn.buspro.reponse.LoginResponse;
import sourceinn.buspro.reponse.RouteResponse;
import sourceinn.buspro.ui.map.view.MapFragment;

/**
 * Created by sourceinn on 2016/11/30.
 */

public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    RouteResponse routeResponse;
    String type;
    Context context;
    public ViewPagerAdapter(FragmentManager fm, Context context, RouteResponse routeResponse, String type) {
        super(fm);
        this.routeResponse = routeResponse;
        this.type = type;
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        if(position==0) {
            if(LoginResponse.getLastest(context).getResult().equals("student") || LoginResponse.getLastest(context).getResult().equals("driver")){
                return StationFragment.newInstance();
            }else{
                return StationFragment.newInstance(routeResponse, type);
            }
        }else{
            /*SupportMapFragment mapFragment = new SupportMapFragment();
            mapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    //mMap = googleMap;
                    // Add a marker in Sydney and move the camera
                    googleMap.setMyLocationEnabled(true); // 右上角的定位功能；這行會出現紅色底線，不過仍可正常編譯執行
                    googleMap.getUiSettings().setZoomControlsEnabled(true);  // 右下角的放大縮小功能
                    googleMap.getUiSettings().setCompassEnabled(true);       // 左上角的指南針，要兩指旋轉才會出現
                    googleMap.getUiSettings().setMapToolbarEnabled(true);    // 右下角的導覽及開啟 Google Map功能
                    LatLng sydney = new LatLng(22.631392, 120.301803);
                    googleMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
                    sydney = new LatLng(22.633392, 120.303803);
                    googleMap.addMarker(new MarkerOptions().position(sydney).title("QQQ"));
                    googleMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
                    googleMap.animateCamera(CameraUpdateFactory.zoomTo(12));     // 放大地圖到 16 倍大
                }
            });
            return mapFragment;*/
            if(LoginResponse.getLastest(context).getResult().equals("student") || LoginResponse.getLastest(context).getResult().equals("driver")){
                return MapFragment.newInstance();
            }else{
                return MapFragment.newInstance(routeResponse);
            }
            //return MapFragment.newInstance();
        }
    }

    @Override
    public int getCount() {
        return 2;
    }


}
