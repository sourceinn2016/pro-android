package sourceinn.buspro;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.util.Base64;
import android.util.DisplayMetrics;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import sourceinn.buspro.reponse.FlagData;
import sourceinn.buspro.reponse.GpsData;
import sourceinn.buspro.reponse.LoginResponse;


public class Common {
    public static boolean checkIntenet(Context context){
        ConnectivityManager CM = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = CM.getActiveNetworkInfo();
        return info==null? false:true;

    }
    //從SharedPreferences取出token
    public static String get_token_fromsp(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString("gcmtoken", "");
    }

    public static void showLoading(FragmentManager fm, boolean islive) {
        if(islive) {
            FragmentTransaction ft = fm.beginTransaction();
            Fragment prev = fm.findFragmentByTag("loading");
            if (prev != null) {
                ft.remove(prev);
            }
            LoadingDialogFragment newFragment = new LoadingDialogFragment();
            ft.add(newFragment, "loading");
            ft.commitAllowingStateLoss();
            fm.executePendingTransactions();
        }
    }

    public static void hideLoading(FragmentManager fm, boolean islive) {
        if(islive) {
            FragmentTransaction ft = fm.beginTransaction();
            LoadingDialogFragment prev = (LoadingDialogFragment) fm.findFragmentByTag("loading");
            if (prev != null) {
                ft.remove(prev);
                ft.commitAllowingStateLoss();
                fm.executePendingTransactions();
            }
        }
    }

    public static Bitmap readBitMap(Context context, int resId, int reqWidth, int reqHeight) {
        BitmapFactory.Options option = new BitmapFactory.Options();
        option.inJustDecodeBounds = true;
        InputStream is = context.getResources().openRawResource(resId);
        BitmapFactory.decodeStream(is, null, option);
        //BitmapFactory.decodeResource(context.getResources(), resId, option);
        int scale = 1;
        if(reqWidth>0 && reqHeight>0) {
            scale = calculateInSampleSize(option, reqWidth, reqHeight);
        }
        //Log.v("QQQ", String.valueOf(scale));
        option = new BitmapFactory.Options();
        option.inSampleSize=scale;
        is = context.getResources().openRawResource(resId);
        return BitmapFactory.decodeStream(is, null, option);
    }

    public static Bitmap getCroppedBitmap(Bitmap source) {
        /*Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        // canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
        canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2,
                bitmap.getWidth() / 2, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        //Bitmap _bmp = Bitmap.createScaledBitmap(output, 60, 60, false);
        //return _bmp;
        return output;*/
        int size = Math.min(source.getWidth(), source.getHeight());

        int x = (source.getWidth() - size) / 2;
        int y = (source.getHeight() - size) / 2;

        Bitmap squaredBitmap = Bitmap.createBitmap(source, x, y, size, size);
        if (squaredBitmap != source) {
            source.recycle();
        }

        Bitmap bitmap = Bitmap.createBitmap(size, size, source.getConfig());

        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();
        BitmapShader shader = new BitmapShader(squaredBitmap, BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
        paint.setShader(shader);
        paint.setAntiAlias(true);

        float r = size/2f;
        canvas.drawCircle(r, r, r, paint);

        squaredBitmap.recycle();
        return bitmap;
    }

    public static Bitmap getBitmap(String imagePath, int reqWidth, int reqHeight) {
        //Only decode image size. Not whole image

        BitmapFactory.Options option = new BitmapFactory.Options();
        option.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(imagePath, option);
        //The new size to decode to

        final int NEW_SIZE=100;
        //Now we have image width and height. We should find the correct scale value. (power of 2)
        int width=option.outWidth;
        int height=option.outHeight;
        // Log.v("QQQ", String.valueOf(width) + " " + String.valueOf(height));
       /* int scale=1;
        while(true){
            if(width/2<NEW_SIZE || height/2<NEW_SIZE)
                break;
            width/=2;
            height/=2;
            scale++;
        }
        Log.v("QQQ", String.valueOf(scale));
        Log.v("QQQ", String.valueOf(calculateInSampleSize(option, reqWidth, reqHeight)));*/

        int scale = calculateInSampleSize(option, reqWidth, reqHeight);
        //Decode again with inSampleSize

        option = new BitmapFactory.Options();

        option.inSampleSize=scale;
        return BitmapFactory.decodeFile(imagePath, option);
    }

    private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height
                    / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? widthRatio : heightRatio;
        }

        return inSampleSize;
    }

    public static float getDensity(Context context){
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return metrics.density;
    }

    public static LoginResponse getLoginInfo(Context context) {
        LoginResponse myac = null;
        //從Sqlite取得SessionID
        DBHelper myDB = new DBHelper(context, context.getResources().getString(R.string.DB_name), null, context.getResources().getInteger(R.integer.DB_version));
        SQLiteDatabase db = myDB.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM login", null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            myac = new LoginResponse();
            myac.setToken(cursor.getString(cursor.getColumnIndex("Token")));
            myac.setResult(cursor.getString(cursor.getColumnIndex("Result")));
            myac.setIsConductor(cursor.getInt(cursor.getColumnIndex("IsConductor")) == 1 ? true : false);
            myac.setRollCall(cursor.getInt(cursor.getColumnIndex("IsRollCall")) == 1 ? true : false);
            myac.setMobile(cursor.getString(cursor.getColumnIndex("Mobile")));
            myac.setPassword(cursor.getString(cursor.getColumnIndex("Password")));
        }
        cursor.close();
        db.close();

        return myac;
    }

    public static void setLoginInfo(Context context, LoginResponse rp) {
        //存入Sqlite登入資訊
        DBHelper myDB = new DBHelper(context, context.getResources().getString(R.string.DB_name), null, context.getResources().getInteger(R.integer.DB_version));
        myDB.truncate("login");
        SQLiteDatabase db = myDB.getWritableDatabase();
        db.execSQL("INSERT INTO login ( Token, Result, IsConductor, IsRollCall, Mobile, Password ) VALUES ('" + rp.getToken() + "','" + rp.getResult() + "'," + (rp.isConductor() == true ? 1 : 0) +","+ (rp.isRollCall() == true ? 1 : 0) + ",'"
                + rp.getMobile() + "','" + rp.getPassword() + "')");
        db.close();
    }

    public static void updateToken(Context context, String newToken){
        DBHelper myDB = new DBHelper(context, context.getResources().getString(R.string.DB_name), null, context.getResources().getInteger(R.integer.DB_version));
        SQLiteDatabase db = myDB.getWritableDatabase();
        db.execSQL("UPDATE login SET Token = '"+newToken+"' ");
        db.close();
    }

    public static void addSignature(Context context, String MemberID) {
        DBHelper myDB = new DBHelper(context, context.getResources().getString(R.string.DB_name), null, context.getResources().getInteger(R.integer.DB_version));
        SQLiteDatabase db = myDB.getWritableDatabase();
        db.execSQL("INSERT INTO signature (MemberID)VALUES ('"+MemberID+"')");
        db.close();
    }
    public static List<String> isSignatured(Context context, boolean isAm) {
        DBHelper myDB = new DBHelper(context, context.getResources().getString(R.string.DB_name), null, context.getResources().getInteger(R.integer.DB_version));
        SQLiteDatabase db = myDB.getWritableDatabase();
        String conti = isAm? "<" : ">";
        Cursor cursor = db.rawQuery("SELECT * FROM signature WHERE datetime(CreateTime) "+conti+" dateTime(date('now')||' 12:00:00') AND date(CreateTime)=date('now') ", null);
        List<String> data = new ArrayList<>();
        while(cursor.moveToNext()){
            data.add(cursor.getString(cursor.getColumnIndex("MemberID")));
        }
        cursor.close();
        db.close();
        return data;
    }

    public static void deleteSignBeforeToday(Context context){
        DBHelper myDB = new DBHelper(context, context.getResources().getString(R.string.DB_name), null, context.getResources().getInteger(R.integer.DB_version));
        SQLiteDatabase db = myDB.getWritableDatabase();
        db.execSQL("DELETE FROM signature WHERE date(CreateTime) < date('now')");
        db.close();
    }

    public static void deleteSign(Context context){
        DBHelper myDB = new DBHelper(context, context.getResources().getString(R.string.DB_name), null, context.getResources().getInteger(R.integer.DB_version));
        SQLiteDatabase db = myDB.getWritableDatabase();
        db.execSQL("DELETE FROM signature WHERE 1=1");
        db.close();
    }


    public static void saveGPS(Context context, GpsData gpsData) {
        DBHelper myDB = new DBHelper(context, context.getResources().getString(R.string.DB_name), null, context.getResources().getInteger(R.integer.DB_version));
        SQLiteDatabase db = myDB.getWritableDatabase();
        //Log.v("QQQ", "INSERT INTO GPS ( Latitude, Longitude, RouteID)VALUES ('"+gpsData.getLatitude()+"','"+gpsData.getLongitude()+"','"+gpsData.getRouteID()+"')");
        db.execSQL("INSERT INTO GPS ( Latitude, Longitude)VALUES ('"+gpsData.getLatitude()+"','"+gpsData.getLongitude()+"')");
        db.close();
    }

    public static List<GpsData> getGPS(Context context) {
        List<GpsData> gpsDataList = new ArrayList<>();
        DBHelper myDB = new DBHelper(context, context.getResources().getString(R.string.DB_name), null, context.getResources().getInteger(R.integer.DB_version));
        SQLiteDatabase db = myDB.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM gps", null);
        while(cursor.moveToNext()){
            GpsData tmp = new GpsData();
            tmp.setId(cursor.getInt(cursor.getColumnIndex("id")));
            tmp.setLatitude(cursor.getFloat(cursor.getColumnIndex("Latitude")));
            tmp.setLongitude(cursor.getFloat(cursor.getColumnIndex("Longitude")));
            tmp.setCreatetime(cursor.getString(cursor.getColumnIndex("CreateTime")));
            gpsDataList.add(tmp);
        }
        cursor.close();
        db.close();
        return gpsDataList;
    }

    public static void removeGPS(Context context, int id) {
        DBHelper myDB = new DBHelper(context, context.getResources().getString(R.string.DB_name), null, context.getResources().getInteger(R.integer.DB_version));
        SQLiteDatabase db = myDB.getWritableDatabase();
        db.execSQL("DELETE FROM gps WHERE id=" + id);
        db.close();
    }


    public static void insertFlag(Context context, FlagData data) {
        DBHelper myDB = new DBHelper(context, context.getResources().getString(R.string.DB_name), null, context.getResources().getInteger(R.integer.DB_version));
        SQLiteDatabase db = myDB.getWritableDatabase();
        //Log.v("QQQ", "INSERT INTO GPS ( Latitude, Longitude, RouteID)VALUES ('"+gpsData.getLatitude()+"','"+gpsData.getLongitude()+"','"+gpsData.getRouteID()+"')");
        db.execSQL("INSERT INTO FLAG ( StaionID_Date_AMPM, type, RouteID, Account)VALUES" +
                " ('"+data.getStaionID_Date_AMPM()+"','"+data.getType()+"','"+data.getRouteID()+"','"+data.getAccount()+"')");
        db.close();
    }

    public static boolean hasFlag(Context context, FlagData data ) {
        boolean finded = false;
        DBHelper myDB = new DBHelper(context, context.getResources().getString(R.string.DB_name), null, context.getResources().getInteger(R.integer.DB_version));
        SQLiteDatabase db = myDB.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM FLAG WHERE StaionID_Date_AMPM='"+data.getStaionID_Date_AMPM()+"' " +
                " AND type="+data.getType()+" AND RouteID='"+data.getRouteID()+"'", null);
        while(cursor.moveToNext()){
            finded = true;
            break;
        }
        cursor.close();
        db.close();
        return finded;
    }

    public static List<FlagData> getFlags(Context context, String rsdasString, int type, String routeId, String account) {
        List<FlagData> list = new ArrayList<>();
        DBHelper myDB = new DBHelper(context, context.getResources().getString(R.string.DB_name), null, context.getResources().getInteger(R.integer.DB_version));
        SQLiteDatabase db = myDB.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM FLAG WHERE StaionID_Date_AMPM IN("+rsdasString+") " +
                " AND type="+type+" AND RouteID='"+routeId+"' AND Account='"+account+"'", null);
        while(cursor.moveToNext()){
            FlagData data = new FlagData();
            data.setStaionID_Date_AMPM(cursor.getString(cursor.getColumnIndex("StaionID_Date_AMPM")));
            list.add(data);
        }
        cursor.close();
        db.close();
        return list;
    }

    public static void removeFlagBeforeToday(Context context) {
        DBHelper myDB = new DBHelper(context, context.getResources().getString(R.string.DB_name), null, context.getResources().getInteger(R.integer.DB_version));
        SQLiteDatabase db = myDB.getWritableDatabase();
        db.execSQL("DELETE FROM FLAG WHERE date(CreateTime) < date('now')");
        db.close();
    }

    //改為清空密碼而已
    public static void logout(Context context){
        /*DBHelper myDB = new DBHelper(context, context.getResources().getString(R.string.DB_name), null, context.getResources().getInteger(R.integer.DB_version));
        myDB.truncate("login");*/
        DBHelper myDB = new DBHelper(context, context.getResources().getString(R.string.DB_name), null, context.getResources().getInteger(R.integer.DB_version));
        SQLiteDatabase db = myDB.getWritableDatabase();
        db.execSQL("UPDATE login SET Password = '' ");
        db.close();
    }

    public static String BitmapToBase64(String filepath){
        //轉成base64
        Bitmap bm = BitmapFactory.decodeFile(filepath);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        if(filepath.endsWith(".png")) {
            bm.compress(Bitmap.CompressFormat.PNG, 100, baos); //bm is the bitmap object
        }else{
            bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        }
        byte[] b = baos.toByteArray();

        String header = "";
        if(filepath.endsWith(".png")) {
            header = "data:image/png;base64,";
        }else{
            header = "data:image/jpeg;base64,";
        }
        return header+ Base64.encodeToString(b, Base64.DEFAULT);
    }

    public static String getFileHeader(String filepath){
        String rtstring = "";
        if(filepath.endsWith(".png")){
            rtstring = "data:image/png;base64,";
        }else  if(filepath.endsWith(".jpeg")){
            rtstring = "data:image/jpeg;base64,";
        }else  if(filepath.endsWith(".jpg")){
            rtstring = "data:image/jpeg;base64,";
        }else  if(filepath.endsWith(".png")){
            rtstring = "data:image/png;base64,";
        }else {
            rtstring = "data:image/png;base64,";
        }
        return rtstring;
    }

    public static boolean checkTakePermission(Context context){
        boolean Pass = true;
        int permission = ActivityCompat.checkSelfPermission(context, Manifest.permission.CAMERA);
        if (permission != PackageManager.PERMISSION_GRANTED) {
            Pass = false;
        }
        permission = ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permission != PackageManager.PERMISSION_GRANTED) {
            Pass = false;
        }
        return Pass;
    }

    public static boolean checkPicPermission(Context context){
        return ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE)==PackageManager.PERMISSION_GRANTED;
    }

    //查看是否同意
    public static boolean checkGrantResult(int[] grantResults){
        boolean pass = true;
        for(int i=0; i<grantResults.length; i++){
            if(grantResults[i]!=PackageManager.PERMISSION_GRANTED){
                pass = false;
                break;
            }
        }
        return pass;
    }

    //9 -> 09
    public static String get02dDay(int d){
        return String.format("%02d", d);
    }

    public static void showMsgDialog(FragmentManager fm, String msg, MsgDialogFragment.MsgDialogLister l, String type){
        FragmentTransaction ft =fm.beginTransaction();
        Fragment prev = fm.findFragmentByTag("showmsg");
        if (prev != null) {
            ft.remove(prev);
        }
        final MsgDialogFragment newFragment = MsgDialogFragment.newInstance(msg, type);
        newFragment.setMsgDialogLister(l);
        ft.add(newFragment, "showmsg");
        ft.commitAllowingStateLoss();
    }

    public static void showBigMsgDialog(FragmentManager fm, String msg, BigMsgDialogFragment.MsgDialogLister l, String type){
        FragmentTransaction ft =fm.beginTransaction();
        Fragment prev = fm.findFragmentByTag("showbigmsg");
        if (prev != null) {
            ft.remove(prev);
        }
        final BigMsgDialogFragment newFragment = BigMsgDialogFragment.newInstance(msg, type);
        newFragment.setMsgDialogLister(l);
        ft.add(newFragment, "showbigmsg");
        ft.commitAllowingStateLoss();
    }

    public static void cleanGPS(Context context){
        DBHelper myDB = new DBHelper(context, context.getResources().getString(R.string.DB_name), null, context.getResources().getInteger(R.integer.DB_version));
        SQLiteDatabase db = myDB.getWritableDatabase();
        db.execSQL("DELETE FROM gps WHERE 1=1");
        db.close();
    }

    public static String bytesToHexString(byte[] src) {
        StringBuilder stringBuilder = new StringBuilder("");
        if (src == null || src.length <= 0) {
            return null;
        }
        char[] buffer = new char[2];
        for (int i = 0; i < src.length; i++) {
            buffer[0] = Character.forDigit((src[i] >>> 4) & 0x0F, 16);
            buffer[1] = Character.forDigit(src[i] & 0x0F, 16);
            //System.out.println(buffer);
            stringBuilder.append(buffer);
        }
        return stringBuilder.toString();
    }
}

