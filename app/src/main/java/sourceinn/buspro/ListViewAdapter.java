package sourceinn.buspro;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import sourceinn.buspro.reponse.RouteResponse;

/**
 * Created by sourceinn on 2017/4/12.
 */

public class ListViewAdapter extends BaseAdapter {
    List<RouteResponse> data;
    Context context;
    public ListViewAdapter(Context context, List<RouteResponse> src){
        data = src;
        this.context = context;
    }
    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = LayoutInflater.from(context).inflate(R.layout.listview_item, null);
        TextView text = (TextView) v.findViewById(R.id.text);
        text.setText(data.get(position).getName());
        return v;
    }
}
