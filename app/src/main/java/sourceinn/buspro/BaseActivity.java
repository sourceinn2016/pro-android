package sourceinn.buspro;

import androidx.appcompat.app.AppCompatActivity;

import rx.Subscription;

/**
 * Created by sourceinn on 2016/8/9.
 */
public class BaseActivity extends AppCompatActivity {
    public boolean islive = true;
    //protected List<Subscription> subscriptionList = new ArrayList<>();
    protected Subscription mSubcription;

    
    @Override
    protected void onPause() {
        super.onPause();
        islive = false;
        /*for (int i=0; i<subscriptionList.size(); i++){
            if(subscriptionList.get(i)!=null && !subscriptionList.get(i).isUnsubscribed()){
                subscriptionList.get(i).unsubscribe();
            }
        }*/

    }

    @Override
    protected void onResume() {
        super.onResume();
        islive = true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mSubcription!=null && !mSubcription.isUnsubscribed()){
            mSubcription.unsubscribe();
        }

    }
}
