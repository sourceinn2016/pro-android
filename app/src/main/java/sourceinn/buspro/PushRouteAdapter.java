package sourceinn.buspro;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;

import java.util.HashMap;
import java.util.List;

import sourceinn.buspro.reponse.RouteResponse;


public class PushRouteAdapter extends RecyclerView.Adapter {
    protected List<RouteResponse> mDatas;
    protected Context mContext;
    HashMap<Integer, Boolean> isSelected;
    public PushRouteAdapter(Context context, List<RouteResponse> mDatas){
        mContext = context;
        this.mDatas = mDatas;
        isSelected = new HashMap<>();
        for (int i=0;i<mDatas.size();i++){
            isSelected.put(i, false);
        }
        //Log.v("QQQ", String.valueOf(isSelected.size()));
    }

    public HashMap<Integer, Boolean> getSelectList(){
        return isSelected;
    }

    public void notifyChange( List<RouteResponse> mDatas){
        isSelected = new HashMap<>();
        for (int i=0;i<mDatas.size();i++){
            isSelected.put(i, false);
        }
        notifyDataSetChanged();
    }

    public void selectAll(boolean status){
        for (int i=0;i<mDatas.size();i++){
            isSelected.put(i, status);
        }
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.push_item, parent, false);
        return new MyViewHolder(view);

    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        ((MyViewHolder)holder).checkedTextView.setText(mDatas.get(position).getName());
        ((MyViewHolder)holder).checkedTextView.setChecked(isSelected.get(position));
        ((MyViewHolder)holder).itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MyViewHolder)holder).checkedTextView.setChecked(!isSelected.get(position));
                if (isSelected.get(position)) {
                    isSelected.put(position, false);
                } else {
                    isSelected.put(position, true);
                }
            }
        });


    }


    @Override
    public int getItemCount()
    {
        return mDatas.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        CheckedTextView checkedTextView;


        public MyViewHolder(View view)
        {
            super(view);
            checkedTextView = (CheckedTextView)view.findViewById(R.id.checkedTextView);
        }
    }

}
