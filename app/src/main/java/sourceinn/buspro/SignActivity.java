package sourceinn.buspro;

import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.HashMap;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import sourceinn.buspro.reponse.BaseResponse;
import sourceinn.buspro.reponse.LoginResponse;
import sourceinn.buspro.ui.login.view.LoginActivity;

/**
 * Created by sourceinn on 2017/3/27.
 */

public class SignActivity extends BaseActivity implements View.OnClickListener {
    SignatureView signView;
    private NfcAdapter mNfcAdapter;
    private PendingIntent mPendingIntent;

    String MemberID;

    boolean isLoading = false;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign);
        MemberID = getIntent().getStringExtra("MemberID");
        Button sure = (Button) findViewById(R.id.sure);
        Button cancel = (Button) findViewById(R.id.cancel);
        sure.setOnClickListener(this);
        cancel.setOnClickListener(this);
        ImageView refresh = (ImageView) findViewById(R.id.refresh);
        refresh.setOnClickListener(this);
        signView = (SignatureView) findViewById(R.id.signView);
        TextView title = (TextView) findViewById(R.id.title);
        mNfcAdapter = mNfcAdapter.getDefaultAdapter(this);
        if (mNfcAdapter == null) {
            title.setText(getString(R.string.plsSign));
        }else if (!mNfcAdapter.isEnabled()) {
            Common.showMsgDialog(getSupportFragmentManager(), getString(R.string.plsOpenNFCInSystem), new MsgDialogFragment.MsgDialogLister() {
                @Override
                public void sureEvent() {
                    finish();

                }
            }, "noCancel");
            return;
        }//else {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S){
            mPendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()), PendingIntent.FLAG_IMMUTABLE|0);
        }else{
            mPendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()), 0);
        }

       // }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        if(!isLoading){
            Common.showLoading(getSupportFragmentManager(), islive);
            isLoading = true;
            Tag tag = intent.getParcelableExtra(mNfcAdapter.EXTRA_TAG);
            Log.v("QQQ", Common.bytesToHexString(tag.getId()));
            ClaimChild(Common.bytesToHexString(tag.getId())).subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Action1<BaseResponse>() {
                        @Override
                        public void call(BaseResponse baseResponse) {
                            if(baseResponse.getMessage().equals("Token 失效！")){
                                Common.showMsgDialog(getSupportFragmentManager(), getString(R.string.tokenFailReLogin), new MsgDialogFragment.MsgDialogLister() {
                                    @Override
                                    public void sureEvent() {
                                        Common.logout(SignActivity.this);
                                        Common.hideLoading(getSupportFragmentManager(), islive);
                                        Intent intent = new Intent();
                                        intent.setClass(SignActivity.this, LoginActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                }, "noCancel");
                            }else if(baseResponse.getResult().equals("Success")){
                                Common.addSignature(SignActivity.this, MemberID);
                                Common.showMsgDialog(getSupportFragmentManager(), getString(R.string.acceptSuccess), new MsgDialogFragment.MsgDialogLister() {
                                    @Override
                                    public void sureEvent() {
                                        finish();
                                    }
                                }, "noCancel");
                            }else{
                                Common.showMsgDialog(getSupportFragmentManager(), baseResponse.getMessage(), new MsgDialogFragment.MsgDialogLister() {
                                    @Override
                                    public void sureEvent() {
                                        finish();
                                    }
                                }, "noCancel");
                            }
                        }
                    }, new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            throwable.printStackTrace();
                        }
                    }, new Action0() {
                        @Override
                        public void call() {
                            Common.hideLoading(getSupportFragmentManager(), islive);
                        }
                    });
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        if (mNfcAdapter != null)
            mNfcAdapter.enableForegroundDispatch(this, mPendingIntent, null, null);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mNfcAdapter != null)
            mNfcAdapter.disableForegroundDispatch(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.cancel:
                finish();
                break;
            case R.id.refresh:
                signView.clear();
                break;
            case R.id.sure:
                Common.showLoading(getSupportFragmentManager(), islive);
                upLoadSign().subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Action1<BaseResponse>() {
                            @Override
                            public void call(BaseResponse baseResponse) {
                                if(baseResponse.getResult().equals("Success")){
                                    Common.addSignature(SignActivity.this, MemberID);
                                    Common.showMsgDialog(getSupportFragmentManager(), getString(R.string.acceptSuccess), new MsgDialogFragment.MsgDialogLister() {
                                        @Override
                                        public void sureEvent() {
                                            finish();
                                        }
                                    }, "noCancel");
                                }
                            }
                        }, new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {

                            }
                        }, new Action0() {
                            @Override
                            public void call() {
                                Common.hideLoading(getSupportFragmentManager(), islive);
                            }
                        });
                break;
        }
    }

    private Observable<BaseResponse> ClaimChild(final String CardID){
        return Observable.create(new Observable.OnSubscribe<BaseResponse>() {
            @Override
            public void call(Subscriber<? super BaseResponse> subscriber) {
                try {
                    HashMap<String, String> para = new HashMap<String, String>();
                    para.put("Token", URLDecoder.decode(LoginResponse.getLastest(SignActivity.this).getToken()));
                    para.put("MemberID", MemberID);
                    para.put("CardID", CardID);
                    String resp = OkHttpUtil.execute(OkHttpUtil.postRequest(SignActivity.this.getString(R.string.base_url) + "ClaimChild", para)).body().string();
                    Log.v("QQQ", resp);
                    Gson gson = new Gson();
                    BaseResponse cc = gson.fromJson(resp, new TypeToken<BaseResponse>() {}.getType());
                    subscriber.onNext(cc);
                } catch (IOException e) {
                    subscriber.onError(e);
                }
                subscriber.onCompleted();
            }
        });
    }

    private Observable<BaseResponse> upLoadSign(){
        return Observable.create(new Observable.OnSubscribe<BaseResponse>() {
            @Override
            public void call(Subscriber<? super BaseResponse> subscriber) {
                ByteArrayInputStream inputStream;
                try {
                    // create bitmap screen capture
                    View v1 = getWindow().getDecorView().getRootView();
                    v1.setDrawingCacheEnabled(true);
                    Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
                    v1.setDrawingCacheEnabled(false);

                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);

                    byte[] byteArray = stream.toByteArray();
                    inputStream = new ByteArrayInputStream(byteArray);

                    MultipartBuilder multipartBuilder = new MultipartBuilder().type(MultipartBuilder.FORM);
                    multipartBuilder.addFormDataPart("Token", LoginResponse.getLastest(SignActivity.this).getToken());
                    multipartBuilder.addFormDataPart("MemberID", MemberID);
                    MediaType MEDIA_TYPE = null;
                    //MEDIA_TYPE = imagepath.endsWith("png") ? MediaType.parse("image/png") : MediaType.parse("image/jpeg");
                    multipartBuilder.addFormDataPart("image", "image_name.png", RequestBodyUtil.create(MediaType.parse("image/png"), inputStream));

                    Request request = new Request.Builder()
                            .url(getString(R.string.base_url) + "ClaimChild")
                            .post(multipartBuilder.build())
                            .build();

                    Response  response = OkHttpUtil.execute(request);
                    Gson gson = new Gson();
                    String res = response.body().string();
                    BaseResponse cc = gson.fromJson(res, new TypeToken<BaseResponse>() {}.getType());
                    subscriber.onNext(cc);

                } catch (Throwable e) {
                    // Several error may come out with file handling or OOM
                    e.printStackTrace();
                }
                subscriber.onCompleted();
            }
        });
    }


}
