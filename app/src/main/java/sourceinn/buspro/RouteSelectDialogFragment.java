package sourceinn.buspro;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;

import java.io.Serializable;
import java.util.List;

import sourceinn.buspro.reponse.RouteResponse;


/**
 * Created by sourceinn on 2016/8/11.
 */
public class RouteSelectDialogFragment extends DialogFragment {
    Context context;
    ListView listView;
    List<RouteResponse> options;
    public interface RouteSelectDialogLister{
        void selected(RouteResponse s);
    }
    RouteSelectDialogLister routeSelectDialogLister;
    public void setMsgDialogLister(RouteSelectDialogLister l){
        routeSelectDialogLister = l;
    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.context = activity;
    }

    public static RouteSelectDialogFragment newInstance(List<RouteResponse> options){
        //Log.v("QQQ", "newInstance");
        Bundle args = new Bundle();
        args.putSerializable("options", (Serializable) options);
        RouteSelectDialogFragment fragment = new RouteSelectDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        options = (List<RouteResponse>) getArguments().getSerializable("options");
        View v = inflater.inflate(R.layout.route_select, null);
        listView = (ListView)v.findViewById(R.id.listView);
        ListViewAdapter adapter = new ListViewAdapter(getContext(), options);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                                                dismiss();
                                                routeSelectDialogLister.selected(options.get(position));

                                            }
                                        }
        );

        return v;
    }


    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {

            dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            //dialog.setCancelable(false);
            //dialog.setCanceledOnTouchOutside(false);
        }
    }
}
