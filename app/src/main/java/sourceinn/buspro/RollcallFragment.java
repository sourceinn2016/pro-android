package sourceinn.buspro;

import android.content.Intent;
import android.net.Uri;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.schedulers.TimeInterval;
import sourceinn.buspro.reponse.BaseResponse;
import sourceinn.buspro.reponse.LoginResponse;
import sourceinn.buspro.reponse.RollcallResponse;
import sourceinn.buspro.reponse.RxObject;

/**
 * Created by sourceinn on 2016/9/14.
 */
public class RollcallFragment extends BaseFragment implements View.OnClickListener {
    RxBus rxBus = RxBus.getDefault();
    RecyclerView recyclerView;
    protected SwipeRefreshLayout laySwipe;
    RollcallAdapter adapter;
    List<RollcallResponse> mData = new ArrayList<>();
    protected Subscription rxBusSub;
    protected Subscription timeSub;
    Subscription mSubscription;
    Calendar calendar = Calendar.getInstance();
    SimpleDateFormat sdf=new SimpleDateFormat("HH");
    String phone;
    TextView nfcMsg;
    NfcAdapter mNfcAdapter;
    public static RollcallFragment newInstance(String title){
        Bundle args = new Bundle();
        args.putString("title", title);
        RollcallFragment fragment = new RollcallFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.rollcall, null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mNfcAdapter = NfcAdapter.getDefaultAdapter(mContext);
        ImageView bg = (ImageView)view.findViewById(R.id.bg);
        bg.setImageBitmap(Common.readBitMap(mContext, R.drawable.list_background, bg.getWidth(), bg.getHeight()));
        nfcMsg = (TextView) view.findViewById(R.id.nfcMsg);

        /*
        if(mNfcAdapter!=null && mNfcAdapter.isEnabled()){
            float dp =  Common.getDensity(mContext);
            bg.getLayoutParams().width=(int) (60*dp);
            bg.getLayoutParams().height=(int) (60*dp);
            bg.setImageResource( R.mipmap.nfcbtn);

            //bg.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        }else{
            bg.setImageBitmap(Common.readBitMap(mContext, R.drawable.list_background, bg.getWidth(), bg.getHeight()));
        }*/



        TextView title = (TextView)view.findViewById(R.id.title);
        title.setText(getArguments().getString("title"));
        ImageView back =(ImageView)view.findViewById(R.id.back);
        back.setOnClickListener(this);

        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        adapter = new RollcallAdapter(mContext, mData);

        recyclerView.setAdapter(adapter);
        laySwipe = (SwipeRefreshLayout)view.findViewById(R.id.laySwipe);

        SwipeRefreshLayout.OnRefreshListener onSwipeToRefresh = new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                laySwipe.setRefreshing(true);
                getData();
            }
        };
        laySwipe.setOnRefreshListener(onSwipeToRefresh);
        Common.deleteSignBeforeToday(mContext);
        //getData();
    }

    @Override
    public void onPause() {
        super.onPause();
        if(rxBusSub!=null && !rxBusSub.isUnsubscribed()){
            rxBusSub.unsubscribe();
        }
        if(timeSub!=null && !timeSub.isUnsubscribed()){
            timeSub.unsubscribe();
        }
        if(mSubscription!=null && !mSubscription.isUnsubscribed()){
            mSubscription.unsubscribe();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        rxBusSub =  rxBus.toObservable(RxObject.class)
                .subscribe(new Action1<RxObject>() {
                    @Override
                    public void call(RxObject rxObject) {
                        if (rxObject.getTag().equals("rollcall")) {
                            Common.showLoading(getChildFragmentManager(), islive);
                            rollcall((String) rxObject.getObject()).subscribeOn(Schedulers.newThread())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(new Action1<BaseResponse>() {
                                        @Override
                                        public void call(BaseResponse baseResponse) {
                                            getData();
                                            Common.hideLoading(getChildFragmentManager(), islive);
                                        }
                                    }, new Action1<Throwable>() {
                                        @Override
                                        public void call(Throwable throwable) {
                                            Common.hideLoading(getChildFragmentManager(), islive);
                                        }
                                    });
                        } else if (rxObject.getTag().equals("cancel")) {
                            Common.showLoading(getChildFragmentManager(), islive);
                            cancel((String) rxObject.getObject()).subscribeOn(Schedulers.newThread())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(new Action1<BaseResponse>() {
                                        @Override
                                        public void call(BaseResponse baseResponse) {
                                            getData();
                                            Common.hideLoading(getChildFragmentManager(), islive);
                                        }
                                    }, new Action1<Throwable>() {
                                        @Override
                                        public void call(Throwable throwable) {
                                            Common.hideLoading(getChildFragmentManager(), islive);
                                        }
                                    });
                        } else if (rxObject.getTag().equals("notify")) {
                            FragmentTransaction ft =getChildFragmentManager().beginTransaction();
                            Fragment prev = getChildFragmentManager().findFragmentByTag("notify");
                            if (prev != null) {
                                ft.remove(prev);
                            }
                            NotifyDialogFragment newFragment = NotifyDialogFragment.newInstance((String) rxObject.getObject(), "您的小孩並未上車");
                            ft.add(newFragment, "notify");
                            ft.commit();
                        }else if(rxObject.getTag().equals("call")){
                            phone = (String) rxObject.getObject();
                            phoneCall();
                            /*if(ActivityCompat.checkSelfPermission(mContext, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED){
                                phoneCall();
                            }else{
                                requestPermissions(new String[]{
                                                Manifest.permission.CALL_PHONE
                                        },
                                        321);

                            }*/
                        }
                    }
                }) ;
        //getData();

        timeSub = Observable.interval(0, 10000L, TimeUnit.MILLISECONDS).timeInterval()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<TimeInterval<Long>>() {
                    @Override
                    public void call(TimeInterval<Long> longTimeInterval) {
                        getData();
                        if (mNfcAdapter == null) {
                            nfcMsg.setText(getString(R.string.noSupportNFC));
                        }else if (!mNfcAdapter.isEnabled()) {
                            nfcMsg.setText(getString(R.string.plsOpenNFC));
                        }else {
                            nfcMsg.setText(getString(R.string.supportNFC));
                        }
                    }
                });
    }

    private void checkSignatrue(){
        List<String> mems = Common.isSignatured(mContext, Integer.parseInt(sdf.format(calendar.getTime()))<12);
        for (int i=0;i<mData.size();i++){
            mData.get(i).setSign(mems.contains(mData.get(i).getID())?true:false);
        }
        adapter.notifyDataSetChanged();
    }

    protected void phoneCall(){
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + phone));
        startActivity(intent);
    }

    protected void getData(){
        laySwipe.post(new Runnable() {
            @Override
            public void run() {
                laySwipe.setRefreshing(true);
            }
        });
        if(mSubscription!=null && !mSubscription.isUnsubscribed()){
            mSubscription.unsubscribe();
        }
        mSubscription =  getList().subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        new Action1<BaseResponse>() {
                            @Override
                            public void call(BaseResponse baseResponse) {
                                checkSignatrue();
                                laySwipe.setRefreshing(false);
                            }
                        },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable e) {
                                e.printStackTrace();
                                checkSignatrue();
                                laySwipe.setRefreshing(false);
                            }
                        }
                );

    }

    private Observable<BaseResponse> getList(){
        return Observable.create(new Observable.OnSubscribe<BaseResponse>() {
            @Override
            public void call(Subscriber<? super BaseResponse> subscriber) {
                try {
                    String resp = OkHttpUtil.execute(OkHttpUtil.getRequest(getString(R.string.base_url) + "Rollcall?Token=" +
                            LoginResponse.getLastest(getContext()).getToken())).body().string();

                    Gson gson = new Gson();
                    BaseResponse<List<RollcallResponse>> cc = gson.fromJson(resp, new TypeToken<BaseResponse<List<RollcallResponse>>>() {}.getType());
                    if(cc.getMessage().equals("Token 失效！")){
                        RxObject rxObject = new RxObject();
                        rxObject.setTag("Token失效");
                        rxBus.post(rxObject);
                    }
                    mData.clear();
                    if(cc.getData()!=null) {
                        mData.addAll(cc.getData());
                    }
                    subscriber.onNext(cc);
                } catch (IOException e) {
                    subscriber.onError(e);
                }
                subscriber.onCompleted();
            }
        });
    }

    private Observable<BaseResponse> rollcall(final String Member_id){
        return Observable.create(new Observable.OnSubscribe<BaseResponse>() {
            @Override
            public void call(Subscriber<? super BaseResponse> subscriber) {
                try {
                    HashMap<String, String> para = new HashMap<String, String>();
                    para.put("Token", URLDecoder.decode(LoginResponse.getLastest(getContext()).getToken()));
                    para.put("MemberID", Member_id);
                    String resp = OkHttpUtil.execute(OkHttpUtil.postRequest(mContext.getString(R.string.base_url) + "Rollcall", para)).body().string();
                    Gson gson = new Gson();
                    BaseResponse cc = gson.fromJson(resp, new TypeToken<BaseResponse>() {}.getType());
                    if(cc.getMessage().equals("Token 失效！")){
                        RxObject rxObject = new RxObject();
                        rxObject.setTag("Token失效");
                        rxBus.post(rxObject);
                    }
                    subscriber.onNext(cc);
                } catch (IOException e) {
                    subscriber.onError(e);
                }
            }
        });
    }

    private Observable<BaseResponse> cancel(final String Member_id){
        return Observable.create(new Observable.OnSubscribe<BaseResponse>() {
            @Override
            public void call(Subscriber<? super BaseResponse> subscriber) {
                try {
                    HashMap<String, String> para = new HashMap<String, String>();
                    para.put("Token", URLDecoder.decode(LoginResponse.getLastest(getContext()).getToken()));
                    para.put("MemberID", Member_id);
                    String resp = OkHttpUtil.execute(OkHttpUtil.postRequest(mContext.getString(R.string.base_url) + "Cancel", para)).body().string();
                    Gson gson = new Gson();
                    BaseResponse cc = gson.fromJson(resp, new TypeToken<BaseResponse>() {}.getType());
                    if(cc.getMessage().equals("Token 失效！")){
                        RxObject rxObject = new RxObject();
                        rxObject.setTag("Token失效");
                        rxBus.post(rxObject);
                    }
                    subscriber.onNext(cc);
                } catch (IOException e) {
                    subscriber.onError(e);
                }
                subscriber.onCompleted();
            }
        });
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.back:
                RxObject rxObject = new RxObject();
                rxObject.setTag("back");
                rxBus.post(rxObject);
                
        }
    }
}
