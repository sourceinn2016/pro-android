package sourceinn.buspro;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by sourceinn on 2016/8/23.
 */
public class PopAdapter extends BaseAdapter {
    List<String> data;
    Context mContext;
    public PopAdapter(Context context, List<String> data) {
        this.data = data;
        mContext = context;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.mypop_item, null);
        TextView txt = (TextView)v.findViewById(R.id.txt);
        txt.setText(data.get(position));
        return v;
    }

}
