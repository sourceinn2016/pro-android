package sourceinn.buspro;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.List;

import sourceinn.buspro.reponse.RouteResponse;
import sourceinn.buspro.reponse.RxObject;


public class RouteAdapter extends RecyclerView.Adapter {
    protected List<RouteResponse> mDatas;
    RxBus rxBus = RxBus.getDefault();
    protected Context mContext;
    public RouteAdapter(Context context, List<RouteResponse> mDatas){
        mContext = context;
        this.mDatas = mDatas;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.route_item, parent, false);
        return new MyViewHolder(view);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        ((MyViewHolder)holder).name.setText(mDatas.get(position).getName());
        if(mDatas.get(position).getStation().length()==0){
            ((MyViewHolder)holder).name.append("("+mContext.getString(R.string.unGoCar)+")");
        }else{
            ((MyViewHolder)holder).name.append("("+mDatas.get(position).getStation()+")");
        }
        if(mDatas.get(position).getStatus()==null || mDatas.get(position).getStatus().equals("normal")){
            ((MyViewHolder)holder).name.setTextColor(mContext.getResources().getColor(R.color.black));
        }else if(mDatas.get(position).getStatus().equals("problem")){
            ((MyViewHolder)holder).name.setTextColor(mContext.getResources().getColor(R.color.red));
        }

        if(position==0){
            ((MyViewHolder)holder).line.setVisibility(View.GONE);
        }else{
            ((MyViewHolder)holder).line.setVisibility(View.VISIBLE);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RxObject rxObject = new RxObject();
                rxObject.setTag("RouteToStation");
                rxObject.setObject(mDatas.get(position));
                rxBus.post(rxObject);
            }
        });
    }


    @Override
    public int getItemCount()
    {
        return mDatas==null?0:mDatas.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        ImageView bus_status;
        TextView name;
        View line;


        public MyViewHolder(View view)
        {
            super(view);
            bus_status = (ImageView)view.findViewById(R.id.bus_status);
            name = (TextView)view.findViewById(R.id.name);
            line = view.findViewById(R.id.line);
        }
    }

}
