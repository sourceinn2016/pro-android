package sourceinn.buspro;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import sourceinn.buspro.reponse.BaseResponse;
import sourceinn.buspro.reponse.GetMemberResponse;
import sourceinn.buspro.reponse.RxObject;

/**
 * Created by sourceinn on 2016/4/15.
 */
public class DispatchDialogFragment extends DialogFragment {
    RxBus rxBus = RxBus.getDefault();
    Context context;
    RecyclerView recyclerView;
    EditText keyword;
    SearchMemberAdapter adapter;
    List<GetMemberResponse> mData = new ArrayList<>();

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.context = activity;
    }

    public static DispatchDialogFragment newInstance(String token){
        //Log.v("QQQ", "newInstance");
        Bundle args = new Bundle();
        args.putString("Token", token);
        DispatchDialogFragment fragment = new DispatchDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        View v = inflater.inflate(R.layout.dispatch_dialog, null);
        keyword = (EditText) v.findViewById(R.id.keyword);
        Button cancel = (Button)v.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        recyclerView = (RecyclerView)v.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        final ImageView search = (ImageView) v.findViewById(R.id.search);
        int dp = (int) Common.getDensity(context);
        search.setImageBitmap(Common.readBitMap(context, R.drawable.search_user, 35*dp, 35*dp));

        //final Button search = (Button) v.findViewById(R.id.search);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SearchMember().subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Action1<List<GetMemberResponse>>() {
                            @Override
                            public void call(List<GetMemberResponse> getMemberResponses) {
                                adapter = new SearchMemberAdapter(context, mData);
                                recyclerView.setAdapter(adapter);
                                InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(search.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                            }
                        });
            }
        });
        return v;
    }



    public Observable<List<GetMemberResponse>> SearchMember(){
        return Observable.create(new Observable.OnSubscribe<List<GetMemberResponse>>() {
            @Override
            public void call(Subscriber<? super List<GetMemberResponse>> subscriber) {
                try {
                    HashMap<String, String> para = new HashMap<String, String>();
                    para.put("Token", getArguments().getString("Token"));
                    para.put("Keyword", keyword.getText().toString());
                    String resp = OkHttpUtil.execute(OkHttpUtil.postRequest(getString(R.string.base_url) + "SearchMember", para)).body().string();
                    Log.v("QQQ", resp);
                    Gson gson = new Gson();
                    BaseResponse<List<GetMemberResponse>> cc = gson.fromJson(resp, new TypeToken<BaseResponse<List<GetMemberResponse>>>() {}.getType());
                    if(cc.getMessage().equals("Token 失效！")){//注意多國?
                        RxObject rxObject = new RxObject();
                        rxObject.setTag("Token失效");
                        rxBus.post(rxObject);
                    }else {
                        mData.clear();
                        mData.addAll(cc.getData());
                    }
                    subscriber.onNext(cc.getData());
                } catch (IOException e) {
                    subscriber.onError(e);
                }
                subscriber.onCompleted();
            }
        });
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(context, getTheme()) {
            /*@Override
            public void onBackPressed() {
                //do your stuff
                //mCallback.onbackClick( getArguments().getString("type") );
            }*/
        };

        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {

            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            dialog.setCancelable(false);
            //dialog.setCanceledOnTouchOutside(false);
        }
    }


}