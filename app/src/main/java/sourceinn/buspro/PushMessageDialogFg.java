package sourceinn.buspro;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import sourceinn.buspro.reponse.BaseResponse;
import sourceinn.buspro.reponse.LoginResponse;
import sourceinn.buspro.reponse.PushMessageResponse;
import sourceinn.buspro.reponse.RxObject;

/**
 * Created by sourceinn on 2016/9/14.
 */
public class PushMessageDialogFg extends DialogFragment implements View.OnClickListener, PushMessageAdapter.OnItemClickListener {
    RxBus rxBus = RxBus.getDefault();
    RecyclerView recyclerView;
    protected SwipeRefreshLayout laySwipe;
    PushMessageAdapter adapter;
    List<PushMessageResponse> mData = new ArrayList<>();
    Subscription getDataSub, deleteSub;
    NfcAdapter mNfcAdapter;
    public static void showPushMessageDialogFg(FragmentManager fm){
        FragmentTransaction ft =fm.beginTransaction();
        Fragment prev = fm.findFragmentByTag("showPushMessageDialogFg");
        if (prev != null) {
            ft.remove(prev);
        }
        final PushMessageDialogFg newFragment = PushMessageDialogFg.newInstance();
        ft.add(newFragment, "showPushMessageDialogFg");
        ft.commitAllowingStateLoss();
    }
    public static PushMessageDialogFg newInstance(){
        Bundle args = new Bundle();
        PushMessageDialogFg fragment = new PushMessageDialogFg();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.pushmessage, null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mNfcAdapter = NfcAdapter.getDefaultAdapter(getContext());
        ImageView bg = (ImageView)view.findViewById(R.id.bg);
        bg.setImageBitmap(Common.readBitMap(getContext(), R.drawable.list_background, bg.getWidth(), bg.getHeight()));

        TextView title = (TextView)view.findViewById(R.id.title);
        title.setText(getString(R.string.messageCenter));
        ImageView back =(ImageView)view.findViewById(R.id.back);
        back.setOnClickListener(this);


        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new PushMessageAdapter(this, mData);
        //recyclerView.addItemDecoration(null);
        recyclerView.setAdapter(adapter);
        laySwipe = (SwipeRefreshLayout)view.findViewById(R.id.laySwipe);

        SwipeRefreshLayout.OnRefreshListener onSwipeToRefresh = new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                laySwipe.setRefreshing(true);
                getData();
            }
        };
        laySwipe.setOnRefreshListener(onSwipeToRefresh);
        getData();
    }

    @Override
    public void onStart() {
        super.onStart();

        //填滿並去背
        getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        //遮罩
        Window window = getDialog().getWindow();
        WindowManager.LayoutParams windowParams = window.getAttributes();
        windowParams.dimAmount = 0.0f;
        window.setAttributes(windowParams);
        getDialog().setCancelable(true);
        //getDialog().getWindow().setWindowAnimations(R.style.dialog_animation_fade);

    }

    @Override
    public void onPause() {
        super.onPause();
        if(getDataSub!=null && !getDataSub.isUnsubscribed()){
            getDataSub.unsubscribe();
        }
        if(deleteSub!=null && !deleteSub.isUnsubscribed()){
            deleteSub.unsubscribe();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

    }


    protected void getData(){

        if(getDataSub!=null && !getDataSub.isUnsubscribed()){
            getDataSub.unsubscribe();
        }
        HashMap<String , String> map = new HashMap<>();
        map.put("Token", LoginResponse.getLastest(getContext()).getToken());
        getDataSub =  getList(map).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        new Action1<BaseResponse<List<PushMessageResponse>>>() {
                            @Override
                            public void call(BaseResponse<List<PushMessageResponse>> baseResponse) {
                                mData.clear();
                                mData.addAll(baseResponse.getData());
                                adapter.notifyDataSetChanged();
                                laySwipe.setRefreshing(false);
                            }
                        },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable e) {
                                e.printStackTrace();
                                laySwipe.setRefreshing(false);
                            }
                        }
                );

    }

    private Observable<BaseResponse<List<PushMessageResponse>>> getList(final HashMap map){
        return Observable.create(new Observable.OnSubscribe<BaseResponse<List<PushMessageResponse>>>() {
            @Override
            public void call(Subscriber<? super BaseResponse<List<PushMessageResponse>>> subscriber) {
                try {
                    String resp = OkHttpUtil.execute(OkHttpUtil.postRequest(getString(R.string.base_url) +
                            "GetPushMessage?Token", map)).body().string();

                    Gson gson = new Gson();
                    BaseResponse<List<PushMessageResponse>> cc = gson.fromJson(resp, new TypeToken<BaseResponse<List<PushMessageResponse>>>() {}.getType());
                    if(cc.getMessage().equals("Token 失效！")){
                        RxObject rxObject = new RxObject();
                        rxObject.setTag("Token失效");
                        rxBus.post(rxObject);
                    }
                    mData.clear();
                    if(cc.getData()!=null) {
                        mData.addAll(cc.getData());
                    }
                    subscriber.onNext(cc);
                } catch (IOException e) {
                    subscriber.onError(e);
                }
                subscriber.onCompleted();
            }
        });
    }

    private Observable<BaseResponse> delete(final String id){
        return Observable.create(new Observable.OnSubscribe<BaseResponse>() {
            @Override
            public void call(Subscriber<? super BaseResponse> subscriber) {
                try {
                    HashMap<String, String> para = new HashMap<String, String>();
                    para.put("Token", URLDecoder.decode(LoginResponse.getLastest(getContext()).getToken()));
                    para.put("ID", id);
                    String resp = OkHttpUtil.execute(OkHttpUtil.postRequest(getContext().getString(R.string.base_url) + "DeletePushMessage", para)).body().string();
                    Gson gson = new Gson();
                    Log.v("QQQ", resp);
                    BaseResponse cc = gson.fromJson(resp, new TypeToken<BaseResponse>() {}.getType());
                    if(cc.getMessage().equals("Token 失效！")){
                        RxObject rxObject = new RxObject();
                        rxObject.setTag("Token失效");
                        rxBus.post(rxObject);
                    }
                    subscriber.onNext(cc);
                } catch (IOException e) {
                    subscriber.onError(e);
                }
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.back:
                dismiss();
                
        }
    }

    @Override
    public void onDelete(final PushMessageResponse data) {

        Common.showMsgDialog(getChildFragmentManager(), getString(R.string.confirmDelete), new MsgDialogFragment.MsgDialogLister() {
            @Override
            public void sureEvent() {
                if(deleteSub!=null && !deleteSub.isUnsubscribed()){
                    deleteSub.unsubscribe();
                }
                deleteSub = delete(String.valueOf(data.getID()))
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Action1<BaseResponse>() {
                            @Override
                            public void call(BaseResponse baseResponse) {
                                if(baseResponse.getResult().equals("Success")){
                                    int index = -1;
                                    for (int i=0; i<mData.size(); i++){
                                        if(mData.get(i).getID()==data.getID()){
                                            index = i;
                                            break;
                                        }
                                    }
                                    if(index!=-1){
                                        mData.remove(index);
                                        adapter.notifyItemRemoved(index);
                                        adapter.notifyItemRangeChanged(index, mData.size());
                                    }
                                }else{
                                    Common.showMsgDialog(getChildFragmentManager(), baseResponse.getMessage(), null, "noCancel");
                                }
                            }
                        }, new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                throwable.printStackTrace();
                            }
                        });
            }
        }, "");

    }
}
