package sourceinn.buspro;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.concurrent.TimeUnit;


/**
 * Created by sourceinn on 2016/5/13.
 */
public class OkHttpUtil {
    private static OkHttpClient mOkHttpClient;

    public static OkHttpClient getInstance(){
        if( mOkHttpClient==null ){
            synchronized(OkHttpUtil.class){
                if( mOkHttpClient==null ){
                    mOkHttpClient = new OkHttpClient();
                    mOkHttpClient.setConnectTimeout(10, TimeUnit.SECONDS);
                }
            }
        }
        return mOkHttpClient;
    }



    /**
     * 该不会开启异步线程。
     * @param request
     * @return
     * @throws IOException
     */
    public static Response execute(Request request) throws IOException {

        return getInstance().newCall(request).execute();
    }

    public static String getStringFromServer(String url) throws IOException{
        Request request = new Request.Builder().url(url).build();
        Response response = execute(request);
        if (response.isSuccessful()) {
            String responseUrl = response.body().string();
            return responseUrl;
        } else {
            throw new IOException("Unexpected code " + response);
        }
    }

    public static Request getRequest(String url){
        String lang = (Locale.getDefault().getLanguage()+"-"+Locale.getDefault().getCountry()).toLowerCase();
        if(!lang.equals("zh-tw")){
            lang = "en-us";
        }
        url += "&lang="+lang;
        return new Request.Builder()
                .url(url)
                .build();
    }

    public static Request postRequest(String url, HashMap<String, String> para){
        String lang = (Locale.getDefault().getLanguage()+"-"+Locale.getDefault().getCountry()).toLowerCase();
        if(!lang.equals("zh-tw")){
            lang = "en-us";
        }
        para.put("lang",lang);
        FormEncodingBuilder formBody = new FormEncodingBuilder();

        for (String key : para.keySet()) {
            formBody.add(key, para.get(key));
        }

        RequestBody requestBody = formBody.build();
        Request request = new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();
        return request;
    }

    private static final String CHARSET_NAME = "UTF-8";


}
