package sourceinn.buspro;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import sourceinn.buspro.reponse.RouteResponse;

/**
 * Created by sourceinn on 2016/9/30.
 */
public class SpinnerBaseAdapter extends BaseAdapter {
    List<RouteResponse> list;
    Context context;
    public SpinnerBaseAdapter(Context context, List<RouteResponse> list){
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = LayoutInflater.from(context).inflate(R.layout.route_spinner_item, null);
        TextView name = (TextView)v.findViewById(R.id.name);
        name.setText(list.get(position).getName());
        return v;
    }
}
