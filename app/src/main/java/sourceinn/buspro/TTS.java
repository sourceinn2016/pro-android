package sourceinn.buspro;

import android.app.Service;
import android.content.Intent;
import android.media.AudioManager;
import android.os.IBinder;
import android.speech.tts.TextToSpeech;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import sourceinn.buspro.reponse.BaseResponse;
import sourceinn.buspro.reponse.LoginResponse;
import sourceinn.buspro.reponse.StationResponse;
import sourceinn.buspro.reponse.StudentResponse;

/**
 * Created by sourceinn on 2017/6/9.
 */

public class TTS extends Service implements TextToSpeech.OnInitListener, TextToSpeech.OnUtteranceCompletedListener {
    private TextToSpeech mTts;
    @Override
    public void onCreate() {
        mTts = new TextToSpeech(this, this);
        // This is a good place to set spokenText
    }
    //String msg;
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            int result = mTts.setLanguage(Locale.CHINESE);

            if (result != TextToSpeech.LANG_MISSING_DATA && result != TextToSpeech.LANG_NOT_SUPPORTED) {

                mTts.setOnUtteranceCompletedListener(this);

                /*HashMap hashAudio = new HashMap<String, String>();
                hashAudio.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID,String.valueOf(AudioManager.STREAM_MUSIC));
                mTts.speak(msg, TextToSpeech.QUEUE_FLUSH, hashAudio);*/

                Observable.create(new Observable.OnSubscribe<BaseResponse<StudentResponse>>() {
                    @Override
                    public void call(Subscriber<? super BaseResponse<StudentResponse>> subscriber) {
                        try {
                            String resp;
                            resp = OkHttpUtil.execute(OkHttpUtil.getRequest(getString(R.string.base_url) + "Student?Token=" + LoginResponse.getLastest(TTS.this).getToken())).body().string();
                            //Log.v("QQQ", resp);
                            Gson gson = new Gson();
                            BaseResponse<StudentResponse> cc = gson.fromJson(resp, new TypeToken<BaseResponse<StudentResponse>>() {}.getType());
                            subscriber.onNext(cc);
                        } catch (IOException e) {
                            subscriber.onError(e);
                        }
                        subscriber.onCompleted();
                    }


                }).subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Action1<BaseResponse<StudentResponse>>() {
                            @Override
                            public void call(BaseResponse<StudentResponse> studentResponseBaseResponse) {
                                if(studentResponseBaseResponse.getMessage().equals("Token 失效！")){

                                }else {
                                    String msg = "";
                                    List<StationResponse> list = studentResponseBaseResponse.getData().getList();
                                    for (int i=list.size()-1; i>=0; i--){
                                        if(!list.get(i).getStopTime().equals("--")){
                                            //if(i<list.size()-1){
                                                msg = getString(R.string.carNear)+list.get(i).getName()+"，"+getString(R.string.prepareGoCar)+"。";
                                            //}
                                            break;
                                        }
                                    }
                                    if(msg.length()>0){
                                        HashMap hashAudio = new HashMap<String, String>();
                                        hashAudio.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID,String.valueOf(AudioManager.STREAM_MUSIC));
                                        mTts.speak(msg, TextToSpeech.QUEUE_FLUSH, hashAudio);
                                    }else{
                                        stopSelf();
                                    }
                                }
                            }
                        }, new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {

                            }
                        });

            }
        }
    }

    @Override
    public void onUtteranceCompleted(String uttId) {
        Log.v("QQQ", "onUtteranceCompleted");
        stopSelf();
    }

    @Override
    public void onDestroy() {
        Log.v("QQQ", "TTS onDestroy");
        if (mTts != null) {
            mTts.stop();
            mTts.shutdown();
        }
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }
}
